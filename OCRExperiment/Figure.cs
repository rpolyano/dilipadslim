﻿using AForge.Imaging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCRExperiment
{
    public class Figure
    {
        public PointF[] RelativePoints
        {
            get
            {
                if (OriginalPoints.Length == 0)
                    return new PointF[0];
                float minX = 0;//OriginalPoints.Min(vert => vert.X);
                float maxX = originalSize.Width;//OriginalPoints.Max(vert => vert.X);
                float minY = 0;//OriginalPoints.Min(vert => vert.Y);
                float maxY = originalSize.Height;//OriginalPoints.Max(vert => vert.Y);
                return OriginalPoints.Select(vert => new PointF((vert.X - minX) / (maxX - minX), (vert.Y - minY) / (maxY - minY))).ToArray();
            }
        }
        public PointF[] OriginalPoints { get; set; }

        Size originalSize;

        public void AForgeGenerate(Bitmap bitmap)
        {
            SusanCornersDetector scd = new SusanCornersDetector();
            //Bitmap inverted = bitmap.Invert();
            var corners = scd.ProcessImage(bitmap);
            //inverted.Dispose();
            OriginalPoints = corners.Select(crn => new PointF(crn.X, crn.Y)).ToArray();
            originalSize = bitmap.Size;
            //if (OriginalPoints.Length == 0)
            //{
            //    Program.Preview(bitmap);
            //    Debugger.Break();
            //}

        }

        public float Similarity(Figure fig)
        {
            PointF[] largerPoints;
            PointF[] smallerPoints;
            if (RelativePoints.Length > fig.RelativePoints.Length)
            {
                largerPoints = RelativePoints;
                smallerPoints = fig.RelativePoints;
            }
            else
            {
                largerPoints = fig.RelativePoints;
                smallerPoints = RelativePoints;
            }
            if (largerPoints.Length == 0 || smallerPoints.Length == 0)
            {
                return 0f;
            }
            float score = 0f;
            foreach (var myPoint in largerPoints)
            {
                var closestDistance = smallerPoints.Min(vert => vert.DistanceTo(myPoint));
                if (closestDistance < 0.05f)
                {
                    score += (1f / largerPoints.Length);
                }
            }
            return score;
        }


        public void Generate(Bitmap bitmap)
        {
            List<PointF> shapeOutline = new List<PointF>();
            bool[,] matrix = bitmap.GetMatrix(Point.Empty, bitmap.Size);
            for (int x = 0; x < bitmap.Width; x++)
            {
                bool inShape = false;
                float avg = 0;
                for (int y = 0; y < bitmap.Height; y++)
                {
                    bool me = matrix[x, y];
                    if (me && !inShape)
                    {
                        inShape = true;
                        avg = y;
                    }
                    else if ((!me || y == bitmap.Height - 1) && inShape)
                    {
                        inShape = false;
                        shapeOutline.Add(new PointF(x, (int)avg));
                    }
                    else if (me && inShape)
                    {
                        avg += 0.5f;
                    }
                    
                }
            }
            for (int y = 0; y < bitmap.Height; y++)
            {
                bool inShape = false;
                float avg = 0;
                for (int x = 0; x < bitmap.Width; x++)
                {
                    bool me = matrix[x, y];
                    if (me && !inShape)
                    {
                        inShape = true;
                        avg = x;
                    }
                    else if ((!me || x == bitmap.Width - 1) && inShape)
                    {
                        inShape = false;
                        shapeOutline.Add(new PointF((int)avg, y));
                    }
                    else if (me && inShape)
                    {
                        avg += 0.5f;
                    }

                }
            }

            List<PointF> vertices = new List<PointF>();
            for (int x = 1; x < bitmap.Width - 1; x++)
            {
                for (int y = 1; y < bitmap.Height - 1; y++)
                {
                    bool me = matrix[x, y];

                    bool corner;
                    if (me)
                    {
                        corner = (!matrix[x - 1, y - 1] && !matrix[x - 1, y] && !matrix[x, y - 1]) ||
                        (!matrix[x - 1, y + 1] && !matrix[x - 1, y] && !matrix[x, y + 1]) ||
                        (!matrix[x + 1, y - 1] && !matrix[x + 1, y] && !matrix[x, y - 1]) ||
                        (!matrix[x + 1, y + 1] && !matrix[x + 1, y] && !matrix[x, y + 1]);
                    }
                    else
                    {
                        corner = (matrix[x - 1, y - 1] && matrix[x - 1, y] && matrix[x, y - 1]) ||
                            (matrix[x - 1, y + 1] && matrix[x - 1, y] && matrix[x, y + 1]) ||
                            (matrix[x + 1, y - 1] && matrix[x + 1, y] && matrix[x, y - 1]) ||
                            (matrix[x + 1, y + 1] && matrix[x + 1, y] && matrix[x, y + 1]);

                    }
                    
                    if (corner)
                        vertices.Add(new PointF(x, y));
                }
            }
            shapeOutline = vertices;
            //foreach (var vert in vertices)
            //{

            //}

            //List<PointF> vertices = new List<PointF>();
            //bool[,] matrix = bitmap.GetMatrix(Point.Empty, bitmap.Size);
            //for (int x = 1; x < bitmap.Width - 1; x++)
            //{
            //    for (int y = 1; y < bitmap.Height - 1; y++)
            //    {
            //        bool me = matrix[x, y];
            //        if (!me)
            //            continue;

            //        bool corner = (!matrix[x - 1, y - 1] && !matrix[x - 1, y] && !matrix[x, y - 1]) ||
            //            (!matrix[x - 1, y + 1] && !matrix[x - 1, y] && !matrix[x, y + 1]) ||
            //            (!matrix[x + 1, y - 1] && !matrix[x + 1, y] && !matrix[x, y - 1]) ||
            //            (!matrix[x + 1, y + 1] && !matrix[x + 1, y] && !matrix[x, y + 1]);
            //        if (corner)
            //            vertices.Add(new PointF(x, y));
            //    }
            //}
            float minX = shapeOutline.Min(vert => vert.X);
            float maxX = shapeOutline.Max(vert => vert.X);
            float minY = shapeOutline.Min(vert => vert.Y);
            float maxY = shapeOutline.Max(vert => vert.Y);
            OriginalPoints = shapeOutline.ToArray();
            originalSize = bitmap.Size;
            //RelativePoints = shapeOutline.Select(vert => new PointF((vert.X - minX) / (maxX - minX), (vert.Y - minY) / (maxY - minY))).ToArray();

        }

        public Bitmap Draw(Size size, Bitmap original)
        {
            float xScale = (float)size.Width / originalSize.Width;
            float yScale = (float)size.Height / originalSize.Height;

            Bitmap bmp = new Bitmap(size.Width, size.Height);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.DrawImage(original, new Rectangle(Point.Empty, size));
                Pen dotPen = new Pen(Color.Red, 2);
                foreach (var point in OriginalPoints)
                {
                    g.DrawEllipse(dotPen, point.X * xScale, point.Y * yScale, Math.Min(1, size.Width * 0.1f), Math.Min(1, size.Height * 0.1f));
                }
            }
            return bmp;
        }
    }
}
