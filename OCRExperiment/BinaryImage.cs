﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCRExperiment
{
    public class BinaryImage
    {
        public bool[,] image;

        public int xStartPadding = 0;
        public int yStartPadding = 0;

        public int PaddedWidth
        {
            get
            {
                return xStartPadding + image.GetLength(0);
            }
        }

        public int PaddedHeight
        {
            get
            {
                return yStartPadding + image.GetLength(1);
            }
        }

        public int Width
        {
            get
            {
                return image.GetLength(0);
            }
        }

        public int Height
        {
            get
            {
                return image.GetLength(1);
            }
        }

        public BinaryImage(Bitmap bmp)
        {
            image = bmp.GetMatrix(Point.Empty, bmp.Size);
        }

        public bool this[int x, int y]
        {
            get
            {
                if (x < xStartPadding || 
                    y < yStartPadding || 
                    x >= xStartPadding + Width || 
                    y >= yStartPadding + Height)
                {
                    return false;
                }
                return image[x - xStartPadding, y - yStartPadding];
            }
        }

        public float OverlayCompare(BinaryImage b, int offsetX, int offsetY)
        {
            int maxWidth = Math.Max(PaddedWidth, b.Width + offsetX);
            int maxHeight = Math.Max(PaddedHeight, b.Height + offsetY);

            float num = 0f, denom = 0f;
            for (int x = 0; x < maxWidth; x++)
            {
                for (int y = 0; y < maxHeight; y++)
                {
                    num += this[x, y] && b[x - offsetX, y - offsetY] ? 1 : 0;
                    denom += this[x, y] || b[x - offsetX, y - offsetY] ? 1 : 0;
                }
            }
            return num / denom;

            //float num = 0f;
            //for (int x = 0; x < maxWidth; x++)
            //{
            //    for (int y = 0; y < maxHeight; y++)
            //    {
            //        num += this[x, y] ^ b[x - offsetX, y - offsetY] ? 1 : 0;
            //    }
            //}
            //return 1f - num / (maxWidth * maxHeight);
        }

    }
}
