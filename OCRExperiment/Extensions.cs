﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCRExperiment
{
    public static class Extensions
    {
        public static bool Evaluate(this bool[,] matrix, float threshold)
        {
            float total = matrix.GetLength(1) * matrix.GetLength(0);
            float numTrue = 0;
            foreach (var x in matrix)
            {
                if (x)
                    numTrue++;
            }
            return numTrue / total > threshold;

        }

       
        public static float OverlayCompare(this Bitmap a, Bitmap b)
        {
            
            //1) Find top left pixel in a and b to get offset -->
            // Better way to find optimal alignment? Try all possible alignments?
            //2) return sum(x,y=>a[x,y].colored and b[x,y].colored) / sum(x,y=>a[x,y].colored or b[x,y].colored)

            int widthDiff = Math.Abs(a.Width - b.Width);
            int heightDiff = Math.Abs(a.Height - b.Height);
            BinaryImage aBin = new BinaryImage(a);
            BinaryImage bBin = new BinaryImage(b);

            aBin.xStartPadding = bBin.Width;
            aBin.yStartPadding = bBin.Height;
            
            float max = 0f;

            Parallel.For(0, bBin.Width + aBin.Width + 1, (x) =>// (int x = 0; x <= bBin.Width + aBin.Width; x++)
            {
                for (int y = 0; y <= bBin.Height + aBin.Height; y++)
                {
                    //bBin.xStartPadding = x;
                    //bBin.yStartPadding = y;
                    max = Math.Max(aBin.OverlayCompare(bBin, x, y), max);
                }
            });

            return max;
        }

        public static Bitmap Pad(this Bitmap bmp, int paddingX, int paddingY)
        {
            if (paddingX == 0 && paddingY == 0)
            {
                return (Bitmap)bmp.Clone();
            }
            Bitmap output = new Bitmap(bmp.Width + paddingX * 2, bmp.Height + paddingY * 2, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            for (int x = 0; x < output.Width; x++)
            {
                for (int y = 0; y < output.Height; y++)
                {
                    output.SetPixel(x, y, Color.Black);
                }
            }
            using (Graphics g = Graphics.FromImage(output))
            {
                
                g.DrawImage(bmp, new Point(paddingX, paddingY));
            }
            return output;
        }

        public static int GetBaselineY(this Bitmap bmp)
        {
            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = bmp.Height - 1; y >= 0; y--)
                {
                    if (bmp.GetPixel(x, y).R == 255 && bmp.GetPixel(x, y).G == 0 && bmp.GetPixel(x, y).A == 255)
                    {
                        return y;
                    }
                }
            }
            return -1;
        }

        public static bool[,] GetMatrix(this Bitmap bmp, Point offset, Size size, bool alphaOnly = false)
        {
            bool[,] ret = new bool[size.Width, size.Height];
            for (int x = offset.X; x < Math.Min(offset.X + size.Width, bmp.Width); x++)
            {
                for (int y = offset.Y; y < Math.Min(offset.Y + size.Height, bmp.Height); y++)
                {
                    Color col = bmp.GetPixel(x, y);
                    ret[x - offset.X, y - offset.Y] = ((alphaOnly) || (col.R < 10 && col.G < 10 && col.B < 10)) && col.A > 100;
                }
            }
            return ret;
        }

        public static Bitmap DownSample(this Bitmap original, int windowSize, float threshold)
        {

            Bitmap output = new Bitmap(original.Width / windowSize, original.Height / windowSize);
            for (int x = 0; x < original.Width; x += windowSize)
            {
                for (int y = 0; y < original.Height; y += windowSize)
                {

                    bool[,] matrix = original.GetMatrix(new Point(x, y), new Size(windowSize, windowSize));
                    if (x / windowSize < output.Width && y / windowSize < output.Height)
                        output.SetPixel(x / windowSize, y / windowSize, matrix.Evaluate(threshold) ? Color.Black : Color.Transparent);
                }
            }
            return output;
        }

        public static double DistanceTo(this PointF p1, PointF p2)
        {
            return Math.Sqrt(Math.Pow(p2.X - p1.X, 2) + Math.Pow(p2.Y - p1.Y, 2));
        }

        public static Bitmap Binarize(this Bitmap original, int padding = 5, bool alphaOnly = false)
        {
            Bitmap output = new Bitmap(original.Width + padding * 2, original.Height + padding * 2, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            for (int x = 0; x < output.Width; x++)
            {
                for (int y = 0; y < output.Height; y++)
                {
                    output.SetPixel(x, y, Color.Black);
                }
            }


            bool[,] originalMatrix = original.GetMatrix(Point.Empty, original.Size, alphaOnly);
            for (int x = 0; x < original.Width; x++)
            {
                for (int y = 0; y < original.Height; y++)
                {
                    if (originalMatrix[x, y])
                        output.SetPixel(x + padding, y + padding, Color.White);
                    else
                        output.SetPixel(x + padding, y + padding, Color.Black);
                }
            }
            return output;
        }
    }
}
