﻿using AForge.Imaging;
using AForge.Imaging.ColorReduction;
using NLPHelpers;
using OCRExperiment.OCRCorrection;
using OCRExperiment.OCRCorrection.NGram;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace OCRExperiment
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Levenshtein _test = new Levenshtein("tlie", "tilde");
            _test.ComputeMatrix();
            _test.ComputeOperations(false);
            Console.WriteLine("OCR Correction experiment menu");
            Console.WriteLine("0) Extract letters");
            Console.WriteLine("1) Create symbol combinations");
            Console.WriteLine("2) Match symbol combinations (Exhaustive Template Method)");
            Console.WriteLine("3) Match symbol combinations (Relative Vertex Method)");
            Console.WriteLine("4) Open Tester");
            Console.WriteLine("5) Batch test");
            Console.Write("Enter option #: ");
            int option = Int32.Parse(Console.ReadLine());
            string letterIndex = File.ReadAllText("letters/string.txt").Replace(" ", "").Trim();
            switch (option)
            {
                case 0:
                    Console.Clear();
                    Bitmap bmp = new Bitmap("letters.png");
                    Bitmap bmpBaselined = new Bitmap("letters-baselined.png");
                    Bitmap inverted = bmp.Binarize(0, true);
                    BlobCounter bc = new BlobCounter();
                    //bc.FilterBlobs = true;
                    //bc.MinHeight = 5;
                    //bc.MaxHeight = 5;
                    bc.ObjectsOrder = ObjectsOrder.None;
                    bc.ProcessImage(inverted);

                    //Preview(inverted);
                    Blob[] blobs = bc.GetObjectsInformation();
                    List<Blob> orderedBlobs = new List<Blob>();
                    for (int row = 1; row <= 6; row++)
                    {
                        int rowHeight = bmp.Height / 6;
                        orderedBlobs.AddRange(blobs.Where(blob => blob.Rectangle.Y > rowHeight * (row - 1) &&
                            blob.Rectangle.Y < rowHeight * row).OrderBy(b => b.Rectangle.X));
                    }
                    blobs = orderedBlobs.ToArray();
                    for (int i = 0; i < blobs.Length; i++)
                    {
                        Bitmap blobBmp = new Bitmap(blobs[i].Rectangle.Width, blobs[i].Rectangle.Height);
                        for (int x = 0; x < blobBmp.Width; x++)
                        {
                            for (int y = 0; y < blobBmp.Height; y++)
                            {
                                blobBmp.SetPixel(x, y, bmpBaselined.GetPixel(blobs[i].Rectangle.X + x, blobs[i].Rectangle.Y + y));
                            }
                        }
                        blobBmp.Save("letters/" + (int)letterIndex[i] + ".png", ImageFormat.Png);
                    }
                    break;
                case 1:
                    Console.Clear();
                    Console.WriteLine("Enter spacing: ");
                    int spacing = Int32.Parse(Console.ReadLine());
                    string[] inputFiles = Directory.GetFiles("letters", "*.png").OrderBy(str => str).ToArray();
                    int total = inputFiles.Length * inputFiles.Length;
                    for (int i = 0; i < inputFiles.Length; i++)
                    {
                        string one = inputFiles[i];
                        File.Copy(one, "combinations/" + Path.GetFileNameWithoutExtension(one) + ".png", true);
                        for (int j = 0; j < inputFiles.Length; j++)
                        {
                            string two = inputFiles[j];
                            Bitmap merged = Merge(one, two, spacing);

                            merged.Save("combinations/" + Path.GetFileNameWithoutExtension(one) + "." + Path.GetFileNameWithoutExtension(two) + ".png", ImageFormat.Png);
                            Console.Write("\r" + (100 * (i * inputFiles.Length + j)) / total + "%");
                        }
                    }

                    break;

                case 2:
                    DoComparisons("output-etm.csv", ETM);
                    break;
                case 3:
                    DoComparisons("output-rtm.csv", RTM, 5);
                    break;
                case 4:
                    Tester tester = new Tester();
                    tester.ShowDialog();
                    Console.ReadLine();
                    break;
                case 5:
                    {
                        Console.Clear();
                        int NUM_SAMPLES = 5000;
                        HashSet<string> wordList = new HashSet<string>();
                        string[] wordlistWords = File.ReadAllLines("wordlist.txt");
                        foreach (var wlw in wordlistWords)
                        {
                            wordList.Add(wlw);
                        }
                        Console.WriteLine("Loaded {0} words from wordlist.txt", wordlistWords.Length);
                        Console.WriteLine("Enter input folder:");
                        string inputFolder = Console.ReadLine();
                        string[] xmls = Directory.GetFiles(inputFolder, "*.xml");
                        List<string> allWords = new List<string>();
                        Dictionary<string, int> wrongWordOccurences = new Dictionary<string, int>();
                        Dictionary<string, float> realWordOccurences = new Dictionary<string, float>();
                        Dictionary<string, List<string>> wordContexts = new Dictionary<string, List<string>>();
                        int numFile = 0;
                        foreach (var xml in xmls)
                        {
                            numFile++;
                            XDocument xdoc = XDocument.Load(xml);
                            string[] pvalues = xdoc.Descendants().Where(x => x.Name.LocalName == "speech").Descendants().Where(x => x.Name.LocalName == "p").Select(xel => xel.Value).ToArray();
                            foreach (var p in pvalues)
                            {
                                var pWords = Regex.Split(p, @"\b", RegexOptions.Compiled).Where(w => w != " ").ToArray();
                                for (int i = 0; i < pWords.Length; i++)
                                {
                                    string word = pWords[i];
                                    string prefix = "";
                                    string affix = "";




                                    if (wordList.Contains(word)
                                        || (i < pWords.Length - 1 && pWords[i + 1] == "-")
                                        || (i > 0 && pWords[i - 1] == "-")
                                        || !Regex.IsMatch(word, "^[a-z]{4,}$", RegexOptions.Compiled))
                                    {
                                        if (wordList.Contains(word)
                                            && (i == pWords.Length - 1 || pWords[i + 1] != "-")
                                            && (i == 0 || pWords[i - 1] != "-")
                                            && Regex.IsMatch(word, "^[a-z]{3,}$", RegexOptions.Compiled))
                                        {
                                            //if (word == "toi")
                                            //{
                                            //    Debugger.Break();
                                            //}
                                            if (!realWordOccurences.ContainsKey(word))
                                            {
                                                realWordOccurences[word] = 1;
                                            }
                                            else
                                            {
                                                realWordOccurences[word] = realWordOccurences[word] + 1;
                                            }
                                        }
                                        continue;
                                    }



                                    if (i > 0)
                                    {
                                        prefix = Enumerable.Range(1, Math.Min(i, 5)).Select(x => pWords[i - x] + " ").Reverse().Aggregate((a, b) => a + " " + b) + " ";
                                    }
                                    if (i < pWords.Length - 1)
                                    {
                                        affix = Enumerable.Range(1, Math.Min(5, pWords.Length - i - 1)).Select(x => pWords[i + x] + " ").Aggregate((a, b) => a + " " + b);
                                    }


                                    if (!wrongWordOccurences.ContainsKey(word))
                                    {
                                        wordContexts[word] = new List<string>();
                                        wrongWordOccurences[word] = 1;
                                    }
                                    else
                                    {
                                        wrongWordOccurences[word] = wrongWordOccurences[word] + 1;
                                    }
                                    wordContexts[word].Add(prefix + word + " " + affix);

                                    Console.Write("\r" + numFile + " - " + wordContexts.Count + " words collected");
                                }
                                if (wordContexts.Count > NUM_SAMPLES)
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("Enough samples collected");
                                    break;
                                }
                            }

                            if (wordContexts.Count > NUM_SAMPLES)
                            {
                                break;
                            }

                        }
                        float maxRWO = realWordOccurences.Max(kvp => kvp.Value);
                        realWordOccurences = realWordOccurences.ToDictionary(kvp => kvp.Key, kvp => kvp.Value / maxRWO);
                        var orderedPairs = wrongWordOccurences.OrderByDescending(kvp => kvp.Value);
                        string output = "Errored word,Occurences,Correction(s),Contexts,Corrected contexts\r\n";
                        OCRCorrector corr = new OCRCorrector(realWordOccurences.Select(kvp => kvp.Key).ToArray(), null,
                            new VisualFactor(), new PopularityFactor(realWordOccurences), new LevenshteinDistanceFactor(), new OperationClassFactor());
                        int count = 0;
                        Parallel.ForEach(orderedPairs, new ParallelOptions() { MaxDegreeOfParallelism = 4 }, kvp =>
                        {
                            count++;
                            string correction = corr.Correct(kvp.Key, true);
                            string wrongContexts = wordContexts[kvp.Key].Take(Math.Min(2, wordContexts[kvp.Key].Count)).Aggregate((one, two) => one + "; " + two).Replace("\"", "''").Replace("\n", " ");
                            lock (corr)
                            {
                                output += kvp.Key + "," + kvp.Value + ",\"" + correction + "\",\"" +
                                wrongContexts + "\",\"" +
                                wrongContexts.Replace(kvp.Key, correction.Contains(",") ? correction.Substring(0, correction.IndexOf(',')) : correction) + "\"\r\n";
                            }

                            Console.Write("\r" + count + " words corrected");
                        });
                        File.WriteAllText("corrections.csv", output);
                    }
                    break;
                case 6:
                    {
                        Console.Clear();
                        int NUM_SAMPLES = Int32.MaxValue;//1000000;//5000;
                        HashSet<string> wordList = new HashSet<string>();
                        string[] wordlistWords = File.ReadAllLines("wordlist.txt");
                        foreach (var wlw in wordlistWords)
                        {
                            wordList.Add(wlw.Replace("-","").Replace("\'", "").Trim());
                        }
                        Console.WriteLine("Loaded {0} words from wordlist.txt", wordlistWords.Length);
                        Console.WriteLine("Enter input folder:");
                        string inputFolder = Console.ReadLine();
                        string[] xmls = Directory.GetFiles(inputFolder, "*.xml");
                        TriGramDictionary tgd = new TriGramDictionary();
                        Dictionary<string, float> wordPopularity = new Dictionary<string, float>();
                        int numFile = 0;
                        bool save = true;
                        if (File.Exists("trigrams.bin"))
                        {
                            byte[] bytes = File.ReadAllBytes("trigrams.bin");
                            tgd = TriGramDictionary.Deserialize(bytes);
                            Console.WriteLine("Loaded {0} trigrams from save file", tgd.Count);
                            save = false;
                        }

                        foreach (var xml in xmls)
                        {
                            numFile++;
                            XDocument xdoc = XDocument.Load(xml);
                            string[] pvalues = xdoc.Descendants().Where(x => x.Name.LocalName == "speech").Descendants().Where(x => x.Name.LocalName == "p").Select(xel => xel.Value).ToArray();
                            if (pvalues.Length == 0)
                                continue;
                            string masterP = pvalues.Aggregate((a, b) => a + " " + b);
                            string[] sentences = Regex.Split(masterP, @"(?<=[.?!;])\s+(?=\p{Lu})", RegexOptions.Compiled);
                            List<string> wordCache = new List<string>();
                            foreach (var sentence in sentences)
                            {
                               
                                string[] words = Regex.Matches(sentence, @"\w(?<!\d)[\w'-]*", RegexOptions.Compiled)
                                    .Cast<Match>().Select(m => m.Value.Replace("-","").Replace("\'","").Trim()).ToArray();
                                foreach (var word in words)
                                {
                                    if (!wordPopularity.ContainsKey(word))
                                    {
                                        wordPopularity[word] = 1;
                                        wordCache.Add(word);
                                    }
                                    else
                                    {
                                        wordPopularity[word] = wordPopularity[word] + 1;
                                        wordCache.Remove(word);
                                    }
                                }
                                
                                if (save)
                                    tgd.Generate(words, w => wordList.Contains(w), (w, v) => Char.IsLower(w[0]) || v);

                            }
                            foreach (var word in wordCache)
                            {
                                wordPopularity.Remove(word);
                            }
                            if (save)
                            {
                                Console.Write("\r{0} TriGrams in {1} files {2}%", tgd.Count, numFile, numFile * 100 / xmls.Length);
                                var ordered = tgd.OrderByDescending(kvp => kvp.Value);
                                if (tgd.Count > NUM_SAMPLES)
                                {
                                    break;
                                }
                            }
                            else
                            {
                                Console.Write("\r{0} words logged, {1} files", wordPopularity.Count, numFile);
                                if (wordPopularity.Count > 100000)
                                    break;
                            }
                           
                        }
                        if (save)
                        {
                            Console.WriteLine("\nSaving trigrams");
                            using (FileStream ms = new FileStream("trigrams.bin", FileMode.Create))
                            {
                                var bytes = tgd.Serialize();
                                ms.Write(bytes, 0, bytes.Length);
                            }
                            tgd.GenerateOccurenceMax();
                        }
                        List<TriGram> invalid = tgd.GetNotValid().OrderByDescending(tg=>tgd.GetRelativePopularity(tg)).ToList();
                        List<TriGram> valid = tgd.GetValid();

                        Console.WriteLine();
                        Console.WriteLine("{0} trigrams with errors", invalid.Count);
                        float maxRWO = wordPopularity.Max(kvp => kvp.Value);
                        wordPopularity = wordPopularity.ToDictionary(kvp => kvp.Key, kvp => kvp.Value / maxRWO);
                        string output = "3-gram with wrong Y\tCorr Y\tCorr 3-gram\r\n";
                        int count = 0;
                        foreach (var inv in invalid)
                        {
                            var xzmatches = valid.Where(tg => tg.XZMatch(inv)).ToArray();
                            if (xzmatches.Length == 0)
                            {
                                output += inv + "\tNO MATCH\tNO MATCH\r\n";
                                continue;
                            }
                            OCRCorrector ocr = new OCRCorrector(xzmatches.Select(tg=>tg.Y).ToArray(), null, new VisualFactor(), new PopularityFactor(wordPopularity));
                            string result = ocr.Correct(inv.Y, false, false);
                            output += inv + "\t" + result + "\t" +
                                (result + ",").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                .Select(res => xzmatches.First(xz => xz.CleanY.ToLower() == res.Trim().ToLower())).Select(xz => xz.ToString()).Aggregate((a, b) => a + ", " + b)
                                + "\r\n";
                            count++;
                            Console.Write("\r{0} corrected", count);
                        }
                        File.WriteAllText("trigramcorrections.tsv", output);
                        
                    }
                    break;
            }

        }



        static void DoComparisons(string outfile, Func<Bitmap, Bitmap, float> comparator, int padding = 0)
        {
            string[] combinations = Directory.GetFiles("combinations", "*.png").OrderBy(str => str).ToArray();
            Dictionary<string, Bitmap> bmpCache = new Dictionary<string, Bitmap>();
            Console.Clear();
            Console.WriteLine("Caching bitmaps");
            //for (int i = 0; i < combinations.Length; i++)
            //{
            //    Bitmap bmp = new Bitmap(combinations[i]);
            //    bmpCache[combinations[i]] = bmp.Binarize(padding);
            //    bmp.Dispose();
            //    if (i % 100 == 0)
            //        Console.Write("\r{0}/{1} - {2}%", i, combinations.Length, 100 * i / combinations.Length);
            //}
            Parallel.For(0, combinations.Length, i =>
                {
                    Bitmap bmp = new Bitmap(combinations[i]);
                    bmpCache[combinations[i]] = bmp.Binarize(padding);
                    bmp.Dispose();
                    //if (i % 100 == 0)
                    //    Console.Write("\r{0}/{1} - {2}%", i, combinations.Length, 100 * i / combinations.Length);
                });
            Console.WriteLine("Done");
            string output = "";
            int records = 0;
            ColorImageQuantizer ciq = new ColorImageQuantizer(new MedianCutQuantizer());
            long ttl = combinations.Length * combinations.Length;
            DateTime start = DateTime.Now;
            long ticksRemain = 100;
            float max = 0f;
            Console.WriteLine("[{0}] Starting operations", start.ToShortTimeString());
            HashSet<string> blacklist = new HashSet<string>();
            for (int i = 0; i < combinations.Length; i++)
            {
                string onep = combinations[i];

                Bitmap one = bmpCache[onep];
                for (int j = 0; j < combinations.Length; j++)//Parallel.For(0, combinations.Length, j =>
                {
                    TimeSpan ts = new TimeSpan(ticksRemain);

                    //if ((i * combinations.Length + j + 1) % 1000 == 0)
                    //{
                    //    long ticksPassed = (DateTime.Now - start).Ticks;
                    //    double ticksPerComp = (double)ticksPassed / (i * combinations.Length + j);
                    //    ticksRemain = (long)(ticksPerComp * (combinations.Length * combinations.Length - (i * combinations.Length + j)));
                    //    Console.Write("\r{0} - {1}% {2} h, {3} m, {4} s remain, {5} records ({6}% rate)", i * combinations.Length + j, 100 * (i * combinations.Length + j) / ttl, ts.Hours, ts.Minutes, ts.Seconds, records, 100 * records / (i * combinations.Length + j));
                    //}
                    string twop = combinations[j];

                    if (onep == twop)
                        continue;

                    string oneLetters = Path.GetFileName(onep).Split('.').Take(2).Where(s => s != "png").Select(s => ((char)Int32.Parse(s)).ToString()).Aggregate((a, b) => a + b);
                    string twoLetters = Path.GetFileName(twop).Split('.').Take(2).Where(s => s != "png").Select(s => ((char)Int32.Parse(s)).ToString()).Aggregate((a, b) => a + b);

                    int oneHash = oneLetters.GetHashCode();
                    int twoHash = twoLetters.GetHashCode();

                    string totalHash = Math.Max(oneHash, twoHash) + "." + Math.Min(twoHash, oneHash);

                    if (blacklist.Contains(totalHash))
                    {
                        continue;
                    }
                    blacklist.Add(totalHash);
                    //if (oneLetters.Intersect(twoLetters).Count() > 0 || twoLetters.Length == 1)
                    //    continue;

                    Bitmap two = bmpCache[twop];

                    float similarity = comparator(one, two);
                    max = Math.Max(similarity, max);
                    if (similarity < 0.8f || Double.IsNaN(similarity))
                        continue;

                    lock (bmpCache)
                    {
                        output += oneLetters + "," + twoLetters + "," + similarity + "\r\n";
                    }

                    records++;

                }//);
                Console.WriteLine("[{3}] {0} - {1}% - {2} records", (i + 1) * combinations.Length, (i + 1) * 100 / combinations.Length, records, DateTime.Now.ToShortTimeString());

            }
            File.WriteAllText(outfile, output);
        }

        static float ETM(Bitmap one, Bitmap two)
        {
            lock (one)
            {
                lock (two)
                {

                    ExhaustiveTemplateMatching tm = new ExhaustiveTemplateMatching(0);
                    // compare two images
                    Bitmap padded = one.Pad(Math.Max(0, two.Width - one.Width), Math.Max(0, two.Height - one.Height));
                    var matchings = tm.ProcessImage(padded, two).OrderByDescending(match => match.Similarity);
                    padded.Dispose();
                    // check similarity level
                    float onetwo = matchings.First().Similarity;

                    padded = two.Pad(Math.Max(0, one.Width - two.Width), Math.Max(0, one.Height - two.Height));

                    matchings = tm.ProcessImage(padded, one).OrderByDescending(match => match.Similarity);
                    padded.Dispose();

                    float twoone = matchings.First().Similarity;

                    return twoone * onetwo; // (twoone + onetwo) * 0.5f;
                }
            }

        }

        static float RTM(Bitmap one, Bitmap two)
        {
            Figure fig1 = new Figure();
            fig1.AForgeGenerate(one);
            Figure fig2 = new Figure();
            fig2.AForgeGenerate(two);
            return fig1.Similarity(fig2);
        }

        public static void Preview(Bitmap image, string title = "Preview")
        {
            ImagePreview pv = new ImagePreview(image, title);
            pv.ShowDialog();
        }

        static Bitmap Merge(string image1path, string image2path, int space)
        {
            using (Bitmap one = new Bitmap(image1path))
            {
                using (Bitmap two = new Bitmap(image2path))
                {
                    int oneBaseline = one.GetBaselineY();
                    int twoBaseline = two.GetBaselineY();

                    int twoBaselineOffset = twoBaseline - oneBaseline;

                    Bitmap larger;
                    Bitmap smaller;

                    if (one.Height > two.Height)
                    {
                        larger = one;
                        smaller = two;
                    }
                    else
                    {
                        larger = two;
                        smaller = one;
                    }
                    int height = Math.Max(oneBaseline, twoBaseline) + Math.Max(one.Height - oneBaseline, two.Height - twoBaseline);

                    int oneOffset = 0;
                    int twoOffset = 0;// = Math.Min(one.Height - oneBaseline, two.Height - twoBaseline);
                    if (oneBaseline > twoBaseline)
                    {
                        twoOffset = oneBaseline - twoBaseline;
                    }
                    else
                    {
                        oneOffset = twoBaseline - oneBaseline;
                    }
                    int width = one.Width + space + two.Width;
                    Bitmap output = new Bitmap(width, height);
                    for (int x = 0; x < one.Width; x++)
                    {
                        for (int y = 0; y < one.Height; y++)
                        {
                            output.SetPixel(x, y + oneOffset, one.GetPixel(x, y));
                            //if (one == smaller)
                            //{
                            //    output.SetPixel(x, y + yOffset, one.GetPixel(x, y));
                            //}
                            //else
                            //{
                            //    output.SetPixel(x, y, one.GetPixel(x, y));
                            //}

                        }
                    }
                    for (int x = 0; x < two.Width; x++)
                    {
                        for (int y = 0; y < two.Height; y++)
                        {
                            output.SetPixel(x + one.Width + space, y + twoOffset, two.GetPixel(x, y));
                            //if (two == smaller)
                            //{
                            //    output.SetPixel(x + one.Width + space, y + yOffset, two.GetPixel(x, y));
                            //}
                            //else
                            //{
                            //    output.SetPixel(x + one.Width + space, y, two.GetPixel(x, y));
                            //}

                        }
                    }
                    //Preview(output);
                    return output;
                }
            }

        }
    }
}
