﻿using AForge.Imaging;
using NLPHelpers;
using OCRExperiment.OCRCorrection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OCRExperiment
{
    public partial class Tester : Form
    {
        Bitmap originalSymbol;
        Bitmap localSymbol;
        Bitmap localSymbol2;
        Bitmap originalSymbol2;
        Figure fig;
        Figure fig2;
        string[] wordList;

        
        public Tester()
        {
            InitializeComponent();
            wordList = File.ReadAllLines("wordlist.txt");
            Levenshtein test = new Levenshtein("ietter", "reutter");
            test.ComputeMatrix();
            test.ComputeOperations();
        }

        private void btnLoadPic_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofg = new OpenFileDialog();
            ofg.Filter = "PNG|*.png";
            ofg.InitialDirectory = Environment.CurrentDirectory;
            ofg.FileName = "";
            if (ofg.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
            {
                Bitmap bmp = new Bitmap(ofg.FileName);
                SetSymbol1(bmp);
            }
        }

        public void SetSymbol1(Bitmap bmp)
        {
            originalSymbol = bmp;
            localSymbol = originalSymbol.Binarize();
            fig = new Figure();
            fig.AForgeGenerate(localSymbol);
            pbxFigure.BackgroundImage = fig.Draw(localSymbol.Size, localSymbol);
            pbxImage.BackgroundImage = originalSymbol;
        }

        public void SetSymbol2(Bitmap bmp)
        {
            originalSymbol2 = bmp;
            localSymbol2 = originalSymbol2.Binarize();
            fig2 = new Figure();
            fig2.AForgeGenerate(localSymbol2);
            pbxFigure2.BackgroundImage = fig2.Draw(localSymbol2.Size, localSymbol2);
            pbxSymbol2.BackgroundImage = originalSymbol2;
        }

        private void btnDownSample_Click(object sender, EventArgs e)
        {
            localSymbol = originalSymbol.DownSample((int)numDownsampleWindowSize.Value, 0.5f).Binarize(0);
            fig = new Figure();
            fig.AForgeGenerate(localSymbol);
            pbxFigure.BackgroundImage = fig.Draw(localSymbol.Size, localSymbol);

            localSymbol2 = originalSymbol2.DownSample((int)numDownsampleWindowSize.Value, 0.5f).Binarize(0);
            fig2 = new Figure();
            fig2.AForgeGenerate(localSymbol2);
            pbxFigure2.BackgroundImage = fig2.Draw(localSymbol2.Size, localSymbol2);

            float sim;
            ExhaustiveTemplateMatching tm = new ExhaustiveTemplateMatching(0);
            // compare two images

            var matchings = tm.ProcessImage(localSymbol, localSymbol2).OrderByDescending(match => match.Similarity);
            // check similarity level
            sim = matchings.First().Similarity;
            lblSimilarity.Text = "Similarity: " + localSymbol.OverlayCompare(localSymbol2) + "\n" +
                sim;
        }

        private void pbxFigure_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void btnMakeFigure_Click(object sender, EventArgs e)
        {
            fig = new Figure();
            fig.AForgeGenerate(localSymbol);
            pbxFigure.BackgroundImage = fig.Draw(localSymbol.Size, localSymbol);
        }

        private void btnRestore_Click(object sender, EventArgs e)
        {
            localSymbol = originalSymbol.Binarize();
            pbxFigure.BackgroundImage = localSymbol;
        }

        private void btnLoadSymbol2_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofg = new OpenFileDialog();
            ofg.Filter = "PNG|*.png";
            ofg.InitialDirectory = Environment.CurrentDirectory;
            ofg.FileName = "";
            if (ofg.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
            {
                Bitmap bmp = new Bitmap(ofg.FileName);
                SetSymbol2(bmp);
                lblSimilarity.Text = "Similarity: " + fig2.Similarity(fig);
            }
        }

        private void btnLoadCombo_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtCombo1.Text))
            {
                Bitmap bmp = LoadCombo(txtCombo1.Text);
                if (bmp != null)
                {
                    SetSymbol1(bmp);
                }
            }
            if (!String.IsNullOrEmpty(txtCombo2.Text))
            {
                Bitmap bmp = LoadCombo(txtCombo2.Text);
                if (bmp != null)
                {
                    SetSymbol2(bmp);
                }
            }
            if (fig != null && fig2 != null)
            {
                float sim;
                ExhaustiveTemplateMatching tm = new ExhaustiveTemplateMatching(0);
                // compare two images

                var matchings = tm.ProcessImage(localSymbol.Pad(Math.Max(localSymbol2.Width - localSymbol.Width, 0), Math.Max(localSymbol2.Height - localSymbol.Height, 0)), localSymbol2)
                    .Union(tm.ProcessImage(localSymbol2.Pad(Math.Max(localSymbol.Width - localSymbol2.Width, 0), Math.Max(localSymbol.Height - localSymbol2.Height, 0)), localSymbol)).OrderByDescending(match => match.Similarity);
                // check similarity level
                sim = matchings.First().Similarity;
                lblSimilarity.Text = "Similarity: " + localSymbol.OverlayCompare(localSymbol2) + "\n" +
                    sim;
            }
        }

        public Bitmap LoadCombo(string chars)
        {
            string path = "combinations/" + chars.Select(ch => ((int)ch).ToString()).Aggregate((a, b) => a + "." + b) + ".png";
            if (!File.Exists(path))
            {
                MessageBox.Show("File not found: " + path);
                return null;
            }
            return new Bitmap(path);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OCRCorrector corrector = new OCRCorrector(textBox2.Text.Split(','));
            MessageBox.Show(corrector.Correct(textBox1.Text));
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        
    }
}
