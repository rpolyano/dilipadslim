﻿namespace OCRExperiment
{
    partial class Tester
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbxImage = new System.Windows.Forms.PictureBox();
            this.btnLoadPic = new System.Windows.Forms.Button();
            this.btnDownSample = new System.Windows.Forms.Button();
            this.numDownsampleWindowSize = new System.Windows.Forms.NumericUpDown();
            this.btnRestore = new System.Windows.Forms.Button();
            this.btnMakeFigure = new System.Windows.Forms.Button();
            this.btnLoadSymbol2 = new System.Windows.Forms.Button();
            this.pbxSymbol2 = new System.Windows.Forms.PictureBox();
            this.lblSimilarity = new System.Windows.Forms.Label();
            this.btnLoadCombo = new System.Windows.Forms.Button();
            this.txtCombo1 = new System.Windows.Forms.TextBox();
            this.txtCombo2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.pbxFigure2 = new OCRExperiment.NearestNeighbourPBX();
            this.pbxFigure = new OCRExperiment.NearestNeighbourPBX();
            this.textBox2 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDownsampleWindowSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSymbol2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxFigure2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxFigure)).BeginInit();
            this.SuspendLayout();
            // 
            // pbxImage
            // 
            this.pbxImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbxImage.Location = new System.Drawing.Point(113, 110);
            this.pbxImage.Margin = new System.Windows.Forms.Padding(2);
            this.pbxImage.Name = "pbxImage";
            this.pbxImage.Size = new System.Drawing.Size(75, 81);
            this.pbxImage.TabIndex = 0;
            this.pbxImage.TabStop = false;
            // 
            // btnLoadPic
            // 
            this.btnLoadPic.Location = new System.Drawing.Point(9, 10);
            this.btnLoadPic.Margin = new System.Windows.Forms.Padding(2);
            this.btnLoadPic.Name = "btnLoadPic";
            this.btnLoadPic.Size = new System.Drawing.Size(80, 28);
            this.btnLoadPic.TabIndex = 2;
            this.btnLoadPic.Text = "Load Symbol";
            this.btnLoadPic.UseVisualStyleBackColor = true;
            this.btnLoadPic.Click += new System.EventHandler(this.btnLoadPic_Click);
            // 
            // btnDownSample
            // 
            this.btnDownSample.Location = new System.Drawing.Point(9, 43);
            this.btnDownSample.Margin = new System.Windows.Forms.Padding(2);
            this.btnDownSample.Name = "btnDownSample";
            this.btnDownSample.Size = new System.Drawing.Size(127, 28);
            this.btnDownSample.TabIndex = 3;
            this.btnDownSample.Text = "Downsample symbol";
            this.btnDownSample.UseVisualStyleBackColor = true;
            this.btnDownSample.Click += new System.EventHandler(this.btnDownSample_Click);
            // 
            // numDownsampleWindowSize
            // 
            this.numDownsampleWindowSize.Location = new System.Drawing.Point(140, 49);
            this.numDownsampleWindowSize.Margin = new System.Windows.Forms.Padding(2);
            this.numDownsampleWindowSize.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numDownsampleWindowSize.Name = "numDownsampleWindowSize";
            this.numDownsampleWindowSize.Size = new System.Drawing.Size(49, 20);
            this.numDownsampleWindowSize.TabIndex = 4;
            this.numDownsampleWindowSize.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // btnRestore
            // 
            this.btnRestore.Location = new System.Drawing.Point(93, 10);
            this.btnRestore.Margin = new System.Windows.Forms.Padding(2);
            this.btnRestore.Name = "btnRestore";
            this.btnRestore.Size = new System.Drawing.Size(95, 28);
            this.btnRestore.TabIndex = 5;
            this.btnRestore.Text = "Restore Symbol";
            this.btnRestore.UseVisualStyleBackColor = true;
            this.btnRestore.Click += new System.EventHandler(this.btnRestore_Click);
            // 
            // btnMakeFigure
            // 
            this.btnMakeFigure.Location = new System.Drawing.Point(9, 76);
            this.btnMakeFigure.Margin = new System.Windows.Forms.Padding(2);
            this.btnMakeFigure.Name = "btnMakeFigure";
            this.btnMakeFigure.Size = new System.Drawing.Size(179, 28);
            this.btnMakeFigure.TabIndex = 6;
            this.btnMakeFigure.Text = "Generate Figure";
            this.btnMakeFigure.UseVisualStyleBackColor = true;
            this.btnMakeFigure.Click += new System.EventHandler(this.btnMakeFigure_Click);
            // 
            // btnLoadSymbol2
            // 
            this.btnLoadSymbol2.Location = new System.Drawing.Point(11, 195);
            this.btnLoadSymbol2.Margin = new System.Windows.Forms.Padding(2);
            this.btnLoadSymbol2.Name = "btnLoadSymbol2";
            this.btnLoadSymbol2.Size = new System.Drawing.Size(98, 28);
            this.btnLoadSymbol2.TabIndex = 7;
            this.btnLoadSymbol2.Text = "Load Symbol 2";
            this.btnLoadSymbol2.UseVisualStyleBackColor = true;
            this.btnLoadSymbol2.Click += new System.EventHandler(this.btnLoadSymbol2_Click);
            // 
            // pbxSymbol2
            // 
            this.pbxSymbol2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbxSymbol2.Location = new System.Drawing.Point(113, 232);
            this.pbxSymbol2.Margin = new System.Windows.Forms.Padding(2);
            this.pbxSymbol2.Name = "pbxSymbol2";
            this.pbxSymbol2.Size = new System.Drawing.Size(75, 81);
            this.pbxSymbol2.TabIndex = 8;
            this.pbxSymbol2.TabStop = false;
            // 
            // lblSimilarity
            // 
            this.lblSimilarity.AutoSize = true;
            this.lblSimilarity.Location = new System.Drawing.Point(193, 250);
            this.lblSimilarity.Name = "lblSimilarity";
            this.lblSimilarity.Size = new System.Drawing.Size(59, 13);
            this.lblSimilarity.TabIndex = 10;
            this.lblSimilarity.Text = "Similarity: ?";
            // 
            // btnLoadCombo
            // 
            this.btnLoadCombo.Location = new System.Drawing.Point(193, 198);
            this.btnLoadCombo.Name = "btnLoadCombo";
            this.btnLoadCombo.Size = new System.Drawing.Size(75, 23);
            this.btnLoadCombo.TabIndex = 11;
            this.btnLoadCombo.Text = "Load Combo";
            this.btnLoadCombo.UseVisualStyleBackColor = true;
            this.btnLoadCombo.Click += new System.EventHandler(this.btnLoadCombo_Click);
            // 
            // txtCombo1
            // 
            this.txtCombo1.Location = new System.Drawing.Point(193, 171);
            this.txtCombo1.MaxLength = 2;
            this.txtCombo1.Name = "txtCombo1";
            this.txtCombo1.Size = new System.Drawing.Size(75, 20);
            this.txtCombo1.TabIndex = 12;
            // 
            // txtCombo2
            // 
            this.txtCombo2.Location = new System.Drawing.Point(193, 227);
            this.txtCombo2.MaxLength = 2;
            this.txtCombo2.Name = "txtCombo2";
            this.txtCombo2.Size = new System.Drawing.Size(75, 20);
            this.txtCombo2.TabIndex = 13;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(196, 81);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(133, 20);
            this.textBox1.TabIndex = 14;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(335, 76);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(62, 25);
            this.button1.TabIndex = 15;
            this.button1.Text = "Correct";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pbxFigure2
            // 
            this.pbxFigure2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbxFigure2.Location = new System.Drawing.Point(9, 232);
            this.pbxFigure2.Margin = new System.Windows.Forms.Padding(2);
            this.pbxFigure2.Name = "pbxFigure2";
            this.pbxFigure2.Size = new System.Drawing.Size(75, 81);
            this.pbxFigure2.TabIndex = 9;
            this.pbxFigure2.TabStop = false;
            // 
            // pbxFigure
            // 
            this.pbxFigure.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbxFigure.Location = new System.Drawing.Point(9, 110);
            this.pbxFigure.Margin = new System.Windows.Forms.Padding(2);
            this.pbxFigure.Name = "pbxFigure";
            this.pbxFigure.Size = new System.Drawing.Size(75, 81);
            this.pbxFigure.TabIndex = 1;
            this.pbxFigure.TabStop = false;
            this.pbxFigure.Paint += new System.Windows.Forms.PaintEventHandler(this.pbxFigure_Paint);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(196, 107);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(133, 20);
            this.textBox2.TabIndex = 16;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // Tester
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 330);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.txtCombo2);
            this.Controls.Add(this.txtCombo1);
            this.Controls.Add(this.btnLoadCombo);
            this.Controls.Add(this.lblSimilarity);
            this.Controls.Add(this.pbxFigure2);
            this.Controls.Add(this.pbxSymbol2);
            this.Controls.Add(this.btnLoadSymbol2);
            this.Controls.Add(this.btnMakeFigure);
            this.Controls.Add(this.btnRestore);
            this.Controls.Add(this.numDownsampleWindowSize);
            this.Controls.Add(this.btnDownSample);
            this.Controls.Add(this.btnLoadPic);
            this.Controls.Add(this.pbxFigure);
            this.Controls.Add(this.pbxImage);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Tester";
            this.Text = "Tester";
            ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDownsampleWindowSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSymbol2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxFigure2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxFigure)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxImage;
        private NearestNeighbourPBX pbxFigure;
        private System.Windows.Forms.Button btnLoadPic;
        private System.Windows.Forms.Button btnDownSample;
        private System.Windows.Forms.NumericUpDown numDownsampleWindowSize;
        private System.Windows.Forms.Button btnRestore;
        private System.Windows.Forms.Button btnMakeFigure;
        private System.Windows.Forms.Button btnLoadSymbol2;
        private System.Windows.Forms.PictureBox pbxSymbol2;
        private NearestNeighbourPBX pbxFigure2;
        private System.Windows.Forms.Label lblSimilarity;
        private System.Windows.Forms.Button btnLoadCombo;
        private System.Windows.Forms.TextBox txtCombo1;
        private System.Windows.Forms.TextBox txtCombo2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox2;
    }
}