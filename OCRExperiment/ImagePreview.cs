﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OCRExperiment
{
    public partial class ImagePreview : Form
    {
        Bitmap image;
        public ImagePreview(Bitmap image, string title)
        {
            InitializeComponent();
            this.Text = title;
            this.image = image;
        }

        private void ImagePreview_Load(object sender, EventArgs e)
        {
            nearestNeighbourPBX1.BackgroundImage = image;
            nearestNeighbourPBX1.BackgroundImageLayout = ImageLayout.Zoom;
        }
    }
}
