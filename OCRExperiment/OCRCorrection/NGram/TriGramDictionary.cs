﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCRExperiment.OCRCorrection.NGram
{
    [Serializable]
    public class TriGramDictionary : Dictionary<TriGram, int>
    {
        int occurenceMax;
        public void Generate(string[] orderedWords, Func<string, bool> wordValidator, Func<string, bool, bool> wordFilter)
        {
            for (int i = 0; i < orderedWords.Length - 3; i++)
            {
                
                TriGram tg = new TriGram()
                {
                    X = orderedWords[i],
                    Y = orderedWords[i + 1],
                    Z = orderedWords[i + 2],
                    yIsValid = wordValidator(orderedWords[i + 1])
                };
                if (!tg.yIsValid && tg.Y == "dont" || tg.Y == "don't")
                {
                    Debugger.Break();
                }
                if (!wordFilter(tg.Y, tg.yIsValid))
                {
                    continue;
                }
                if (ContainsKey(tg))
                {
                    this[tg] = this[tg] + 1;
                }
                else
                {
                    this[tg] = 1;
                }
            }
            
        }

        public void GenerateOccurenceMax()
        {
            occurenceMax = this.Max(kvp => kvp.Value);
        }

        public List<TriGram> GetValid()
        {
            return this.Select(kvp => kvp.Key).Where(tg => tg.yIsValid).ToList();
        }

        public List<TriGram> GetNotValid()
        {
            return this.Select(kvp => kvp.Key).Where(tg => !tg.yIsValid).ToList();
        }

        public float GetRelativePopularity(TriGram tg)
        {
            if (!ContainsKey(tg))
                return 0f;
            return this[tg] / (float)occurenceMax;
        }

        public byte[] Serialize()
        {
            byte[] bytes = new byte[Keys.Sum(k=>k.ByteLength) + Count * sizeof(int)];
            int offset = 0;
            foreach (var kvp in this)
            {
                kvp.Key.Serialize(bytes, offset);
                offset += kvp.Key.ByteLength;
                Buffer.BlockCopy(BitConverter.GetBytes(kvp.Value), 0, bytes, offset, sizeof(int));
                offset += sizeof(int);
            }
            return bytes;
        }

        public static TriGramDictionary Deserialize(byte[] bytes)
        {
            int offset = 0;
            TriGramDictionary tgd = new TriGramDictionary();
            while (offset < bytes.Length)
            {
                int serLength = BitConverter.ToInt32(bytes, offset);
                TriGram tg = TriGram.Deserialize(bytes, offset);
                offset += serLength;
                int count = BitConverter.ToInt32(bytes, offset);
                tgd[tg] = count;
                offset += sizeof(int);
            }
            tgd.GenerateOccurenceMax();
            return tgd;
        }
    }
}
