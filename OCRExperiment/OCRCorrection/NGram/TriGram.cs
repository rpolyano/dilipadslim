﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OCRExperiment.OCRCorrection.NGram
{
    [Serializable]
    public class TriGram
    {
        public bool yIsValid { get; set; }

        private string x;
        public string X
        {
            get
            {
                return x;
            }
            set
            {
                xHash = value.GetHashCode();
                x = value;
            }
        }
        private string y;
        public string Y
        {
            get
            {
                return y;
            }
            set
            {
                yHash = value.GetHashCode();
                y = value;
            }
        }
        public string CleanY
        {
            get
            {
                return Y.Replace('-', ' ').Replace('\'', ' ').Replace(" ","");
            }
        }

        private string z;
        public string Z
        {
            get
            {
                return z;
            }
            set
            {
                zHash = value.GetHashCode();
                z = value;
            }
        }

        public int xHash
        {
            get;
            private set;
        }
        public int yHash
        {
            get;
            private set;
        }
        public int zHash
        {
            get;
            private set;
        }

        public bool XZMatch(TriGram other)
        {
            return xHash == other.xHash && zHash == other.zHash;
        }

        public bool XYZMatch(TriGram other)
        {
            return xHash == other.xHash && yHash == other.yHash && zHash == other.zHash;
        }

        public override bool Equals(object obj)
        {
            var other = obj as TriGram;
            if (other == null)
            {
                return false;
            }
            else
            {
                return XYZMatch(other);
            }
        }

        public override int GetHashCode()
        {
            int result = 37; 

            result *= 397; 
            result += xHash;

            result *= 397;
            result += yHash;

            result *= 397;
            result += zHash;

            return result;

        }

        public override string ToString()
        {
            return x + " " + y + " " + z + " (" + (yIsValid ? "" : "NOT ") + "valid)";
        }

        public int ByteLength
        {
            get
            {
                return sizeof(int) + 3 + (x.Length + y.Length + z.Length) * sizeof(char) + 1;
            }
        }

        public void Serialize(byte[] res, int offset)
        {
            //byte[] res = new byte[ByteLength];

            Buffer.BlockCopy(BitConverter.GetBytes(ByteLength), 0, res, offset, sizeof(int));
            res[offset + sizeof(int)] = (byte)x.Length;
            res[offset + sizeof(int) + 1] = (byte)y.Length;
            res[offset + sizeof(int) + 2] = (byte)z.Length;
            Buffer.BlockCopy(x.ToCharArray(), 0, res, offset + sizeof(int) + 3, x.Length * sizeof(char));
            Buffer.BlockCopy(y.ToCharArray(), 0, res, offset + sizeof(int) + 3 + x.Length * sizeof(char), y.Length * sizeof(char));
            Buffer.BlockCopy(z.ToCharArray(), 0, res, offset + sizeof(int) + 3 + (x.Length + y.Length) * sizeof(char), z.Length * sizeof(char));
            res[offset + ByteLength - 1] = (byte)(yIsValid ? 1 : 0);
        }

        public static TriGram Deserialize(byte[] bytes, int offset)
        {
            int length = BitConverter.ToInt32(bytes, offset);
            char[] xChars = new char[bytes[offset + sizeof(int)]];
            char[] yChars = new char[bytes[offset + sizeof(int) + 1]];
            char[] zChars = new char[bytes[offset + sizeof(int) + 2]];

            Buffer.BlockCopy(bytes, offset + sizeof(int) + 3, xChars, 0, xChars.Length * sizeof(char));
            Buffer.BlockCopy(bytes, offset + sizeof(int) + 3 + xChars.Length * sizeof(char), yChars, 0, yChars.Length * sizeof(char));
            Buffer.BlockCopy(bytes, offset + sizeof(int) + 3 + (xChars.Length + yChars.Length) * sizeof(char), zChars, 0, zChars.Length * sizeof(char));

            bool yValid = bytes[offset + length - 1] == 1;

            TriGram tr = new TriGram()
            {
                X = new string(xChars),
                Y = new string(yChars),
                Z = new string(zChars),
                yIsValid = yValid
            };
            return tr;
        }
    }
}
