﻿using NLPHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCRExperiment.OCRCorrection
{
    public interface ILevenshteinFactor
    {
        float Weight
        {
            get;
        }

        float GetFactor(Levenshtein lev);
    }
}
