﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLPHelpers;

namespace OCRExperiment.OCRCorrection
{
    public class LevenshteinDistanceFactor : ILevenshteinFactor
    {
        public float Weight
        {
            get
            {
                return 0.3f;
            }
        }

        public float GetFactor(Levenshtein lev)
        {
            return 1 - lev.GetScore() / 3f;
        }
    }
}
