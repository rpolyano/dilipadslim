﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLPHelpers;

namespace OCRExperiment.OCRCorrection
{
    public class OperationClassFactor : ILevenshteinFactor
    {
        public float Weight
        {
            get
            {
                return 0.2f;
            }
        }

        public float GetFactor(Levenshtein lev)
        {
            float sum = 0f;
            foreach (var op in lev.Operations)
            {
                switch (op.Type)
                {
                    case StringOperationType.Deletion:
                        sum += 0.5f;
                        break;
                    case StringOperationType.Insertion:
                        sum += 0.1f;
                        break;
                    case StringOperationType.Substitution:
                        sum += 0.85f;
                        break;
                    case StringOperationType.TwoForOneSubstitution:
                        sum += 0.6f;
                        break;
                    case StringOperationType.OneForTwoSubstitution:
                        sum += 1f;
                        break;
                }
            }
            return sum / lev.Operations.Length;
        }
    }
}
