﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLPHelpers;

namespace OCRExperiment.OCRCorrection
{
    public class PopularityFactor : ILevenshteinFactor
    {
        Dictionary<string, float> wordPopularities;

        public PopularityFactor(Dictionary<string, float> popularities)
        {
            wordPopularities = popularities;
        }

        public float Weight
        {
            get
            {
                return 0.5f;
            }
        }

        public float GetFactor(Levenshtein lev)
        {
            if (wordPopularities.ContainsKey(lev.Target))
            {
                return wordPopularities[lev.Target];
            }
            else
            {
                return 0;
            }
        }
    }
}
