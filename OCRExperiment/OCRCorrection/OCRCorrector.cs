﻿using AForge.Imaging;
using NLPHelpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCRExperiment.OCRCorrection
{
    public enum VisualSimilarityMethod
    {
        BidirectionalExhaustiveTemplate,
        MonodirectionExhaustiveTemplate,
        MaximumPixelOverlay,
        NaiveOverlay
    }
    public class OCRCorrector
    {
        public ILevenshteinFactor[] factors;
        string[] wordList;
        public OCRCorrector(string[] wordList, Func<string, string> filter = null, params ILevenshteinFactor[] factors)
        {
            this.factors = factors;
            if (filter == null)
            {
                this.wordList = wordList;
            }
            else
            {
                this.wordList = wordList.Select(word => filter(word)).ToArray();
            }
        }

        public string Correct(string word, bool nonames = true, bool lengthfilter = true)
        {
            //word = word.ToLower();
            string[] wordList = this.wordList.Where(wd => (!lengthfilter || Math.Abs(wd.Length - word.Length) < 4) && (!nonames || Char.IsLower(wd[0]))).ToArray();
            var lds = wordList.Select(goodWord =>
            {
                var ld = new Levenshtein(word.Replace(" ", ""), goodWord.Replace(" ",""));
                ld.ComputeMatrix();
                ld.ComputeOperations(false);
                return ld;
            }).OrderBy(ld => ld.Operations.Length).ToArray();

            var firstScore = lds.First().GetScore();
            var closest = lds.TakeWhile(ld => ld.GetScore() <= firstScore + 1).ToArray();

            if (closest.First().GetScore() == 0 || closest.Length == 1)
            {
                return closest.First().Target;
            }
            else
            {
                Dictionary<Levenshtein, float> scores = new Dictionary<Levenshtein, float>();
                foreach (var ld in closest)
                {
                    float score = 0f;
                    float totalFactorWeight = factors.Sum(f => f.Weight);
                    foreach (var factor in factors)
                    {
                        score += (factor.GetFactor(ld) * factor.Weight / totalFactorWeight);
                    }
                    
                    scores[ld] = score;
                }
                var orderedScores = scores.OrderByDescending(kvp => kvp.Value);
                var minLd = orderedScores.First().Value;
                return scores.GroupBy(kvp => kvp.Value)
                    .OrderByDescending(grp => grp.Key).First()
                    .Select(kvp => kvp.Key)
                    .GroupBy(ld => ld.GetScore())
                    .OrderByDescending(ld => ld.Key).First()
                    .Select(ld => ld.Target)
                    .Aggregate((a, b) => a + ", " + b);

            }
        }

        
    }
}
