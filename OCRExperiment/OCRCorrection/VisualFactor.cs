﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLPHelpers;
using System.Drawing;
using AForge.Imaging;
using System.IO;

namespace OCRExperiment.OCRCorrection
{
    public class VisualFactor : ILevenshteinFactor
    {
        public float Weight
        {
            get
            {
                return 1f;
            }
        }

        public float GetFactor(Levenshtein lev)
        {
            float score = 0f;
            int count = 0;
            foreach (var operation in lev.Operations)
            {
                count++;
                if (operation.Type == StringOperationType.Deletion || operation.Type == StringOperationType.Insertion)
                {
                    score += operation.Type == StringOperationType.Deletion ? 0.6f : 0.3f;
                    continue;
                }
                float visualSim = GetVisualSimilarity(LoadCombo(operation.From.Trim()), LoadCombo(operation.To.Trim()), VisualSimilarityMethod.NaiveOverlay);
                score += visualSim;

            }
            if (count > 0)
            {
                score /= count;
            }
            return score;
        }

        public static Bitmap LoadCombo(string chars)
        {
            string path = "combinations/" + chars.Select(ch => ((int)ch).ToString()).Aggregate((a, b) => a + "." + b) + ".png";
            if (!File.Exists(path))
            {
                return null;
            }
            return new Bitmap(path);
        }

        public float GetVisualSimilarity(Bitmap one, Bitmap two, VisualSimilarityMethod method)
        {
            if (method == VisualSimilarityMethod.BidirectionalExhaustiveTemplate)
            {
                using (Bitmap oneBin = one.Binarize(0))
                {
                    using (Bitmap twoBin = two.Binarize(0))
                    {
                        ExhaustiveTemplateMatching tm = new ExhaustiveTemplateMatching(0);

                        var bestOneTwo = tm.ProcessImage(oneBin.Pad(Math.Max(twoBin.Width - oneBin.Width, 0),
                                                                    Math.Max(twoBin.Height - oneBin.Height, 0)),
                                                         twoBin)
                                                .OrderByDescending(match => match.Similarity)
                                                .First();

                        var bestTwoOne = tm.ProcessImage(twoBin.Pad(Math.Max(oneBin.Width - twoBin.Width, 0),
                                                                    Math.Max(oneBin.Height - twoBin.Height, 0)),
                                                         oneBin)
                                                .OrderByDescending(match => match.Similarity)
                                                .First();

                        return Math.Min(bestOneTwo.Similarity, bestTwoOne.Similarity) *
                            Math.Min((float)one.Width / two.Width, (float)two.Width / one.Width);// *
                                                                                                 //Math.Min((float)one.Height / two.Height, (float)two.Height / one.Height);//bestOneTwo.Similarity / 2 + bestTwoOne.Similarity / 2;
                    }
                }
            }
            else if (method == VisualSimilarityMethod.MonodirectionExhaustiveTemplate)
            {
                using (Bitmap oneBin = one.Binarize(0))
                {
                    using (Bitmap twoBin = two.Binarize(0))
                    {
                        ExhaustiveTemplateMatching tm = new ExhaustiveTemplateMatching(0);

                        var bestOneTwo = tm.ProcessImage(oneBin.Pad(Math.Max(twoBin.Width - oneBin.Width, 0),
                                                                    Math.Max(twoBin.Height - oneBin.Height, 0)),
                                                         twoBin)
                                                .OrderByDescending(match => match.Similarity)
                                                .First();

                        //var bestTwoOne = tm.ProcessImage(twoBin.Pad(Math.Max(oneBin.Width - twoBin.Width, 0),
                        //                                            Math.Max(oneBin.Height - twoBin.Height, 0)),
                        //                                 oneBin)
                        //                        .OrderByDescending(match => match.Similarity)
                        //                        .First();

                        return bestOneTwo.Similarity;
                    }
                }
            }
            else if (method == VisualSimilarityMethod.NaiveOverlay)
            {
                using (Bitmap oneBin = one/*.DownSample(2, 0.5f)*/.Binarize(0))
                {
                    using (Bitmap twoBin = two/*.DownSample(2, 0.5f)*/.Binarize(0))
                    {
                        BinaryImage bi1 = new BinaryImage(oneBin);
                        BinaryImage bi2 = new BinaryImage(twoBin);
                        return bi1.OverlayCompare(bi2, 0, 0);
                    }
                }
            }
            else if (method == VisualSimilarityMethod.MaximumPixelOverlay)
            {
                return one.OverlayCompare(two);
            }
            return -1f;
        }
    }
}
