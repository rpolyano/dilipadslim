﻿namespace OCRExperiment
{
    partial class ImagePreview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nearestNeighbourPBX1 = new OCRExperiment.NearestNeighbourPBX();
            ((System.ComponentModel.ISupportInitialize)(this.nearestNeighbourPBX1)).BeginInit();
            this.SuspendLayout();
            // 
            // nearestNeighbourPBX1
            // 
            this.nearestNeighbourPBX1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nearestNeighbourPBX1.Location = new System.Drawing.Point(0, 0);
            this.nearestNeighbourPBX1.Name = "nearestNeighbourPBX1";
            this.nearestNeighbourPBX1.Size = new System.Drawing.Size(976, 712);
            this.nearestNeighbourPBX1.TabIndex = 0;
            this.nearestNeighbourPBX1.TabStop = false;
            // 
            // ImagePreview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(976, 712);
            this.Controls.Add(this.nearestNeighbourPBX1);
            this.Name = "ImagePreview";
            this.Text = "ImagePreview";
            this.Load += new System.EventHandler(this.ImagePreview_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nearestNeighbourPBX1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private NearestNeighbourPBX nearestNeighbourPBX1;
    }
}