﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
namespace XMLToSQLTransformer
{
    public static class XDocumentExtensions
    {
        public static string GetAttributeValue(this XElement xelem, string attributeName)
        {
            var attribute = xelem.Attributes().SingleOrDefault(a => a.Name.LocalName == attributeName);
            if (attribute != null)
            {
                return attribute.Value.Trim();
            }
            return null;
            
        }

        public static string XPathValue(this XDocument xdoc, string xpath, XmlNamespaceManager nms)
        {
            var eval = (IEnumerable)xdoc.XPathEvaluate(xpath, nms);
            XObject node = eval.Cast<XObject>().FirstOrDefault();
            XAttribute attr = node as XAttribute;
            if (attr != null)
            {
                return attr.Value.Trim();
            }
            XElement elem = node as XElement;
            if (elem != null)
            {
                return elem.Value.Trim();
            }
            
            return null;
        }

        public static IEnumerable<XElement> XPathElements(this XDocument xdoc, string xpath, XmlNamespaceManager nms)
        {
            var eval = (IEnumerable)xdoc.XPathEvaluate(xpath, nms);
            XObject node = eval.Cast<XObject>().FirstOrDefault();
            XElement elem = node as XElement;
            if (elem != null)
            {
                return elem.Elements();
            }
            return null;
        }

    }
}
