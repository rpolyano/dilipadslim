﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using EntityFramework.BulkInsert.Extensions;
namespace XMLToSQLTransformer
{
    public enum LinkType
    {
        Unknown = -1,
        Trusted = 0,
        Photo = 1,
        Dbpedia = 2,
        Wikipedia = 3,
    }

    public enum StageDirectionType
    {
        Unknown = -1,
        General = 0,
        Question = 1
    }

    public enum SpecialElementType
    {
        StageDirection = 0,
        Table = 1,
        Vote = 2,
        Unknown = -1,

    }

    class Program
    {
        static List<Member_Member> membersDB;
        static List<Member_Link> newMemberLinks = new List<Member_Link>();
        static List<Member_Affiliation> newMemberAffiliations = new List<Member_Affiliation>();
        static List<Member_Party> newMemberPartys = new List<Member_Party>();
        static List<Member_Membership> newMemberMemberships = new List<Member_Membership>();
        static List<Member_Bio> newMemberBios = new List<Member_Bio>();
        static List<Member_ExtraCurricular> newMemberExtraCurs = new List<Member_ExtraCurricular>();

        static List<Hansard_Proceedings> newHansardProcs = new List<Hansard_Proceedings>();
        static List<Hansard_SpeechElement> newHansardSpeeches = new List<Hansard_SpeechElement>();
        static List<Hansard_ElementContainer> newHansardContainers = new List<Hansard_ElementContainer>();
        static List<Hansard_SpecialElement> newHansardSpecElems = new List<Hansard_SpecialElement>();
        static List<Hansard_P> newHansardPs = new List<Hansard_P>();

        static HashSet<string> memberIds = new HashSet<string>();

        static List<string> partyCache;
        static DateTime MINDT = new DateTime(9998, 1, 1);
        static void Main(string[] args)
        {
            dilipadEntities ents = new dilipadEntities();
            Console.WriteLine("Enter members folder");
            string path = Console.ReadLine();
            
            if (!String.IsNullOrEmpty(path))
            {
                var members = Directory.GetFiles(path, "*.xml", SearchOption.AllDirectories);
                ents.Member_Member.RemoveRange(ents.Member_Member);

                ents.SaveChanges();
                membersDB = new List<Member_Member>();

                partyCache = ents.Member_Party.Select(party => party.Ref).ToList();
                int i = 0;
                foreach (var member in members)
                {
                    i++;
                    XDocument memberDoc = XDocument.Load(member);

                    var reader = memberDoc.CreateReader();
                    var nms = new XmlNamespaceManager(reader.NameTable);
                    nms.AddNamespace("owl", "http://www.w3.org/2002/07/owl#");
                    nms.AddNamespace("pmx", "http://www.politicalmashup.nl/extra");
                    nms.AddNamespace("dc", "http://purl.org/dc/elements/1.1/");
                    nms.AddNamespace("pmd", "http://www.politicalmashup.nl/docinfo");
                    nms.AddNamespace("openpx", "https://openparliament.ca/extra");
                    nms.AddNamespace("dcterms", "http://purl.org/dc/terms/");
                    nms.AddNamespace("html", "http://www.w3.org/1999/xhtml");
                    nms.AddNamespace("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
                    nms.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema-datatypes");
                    nms.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                    nms.AddNamespace("pm", "http://www.politicalmashup.nl");

                    Member_Member m_member;
                    var id = memberDoc.Descendants().Single(x => x.Name.LocalName == "member").Attributes().First(x => x.Name.LocalName == "id").Value;
                    if (ents.Member_Member.Any(mem => mem.GUID == id))
                    {
                        Console.WriteLine("Member with ID {0} Exists. Will delete and recreate it.", id);
                        m_member = ents.Member_Member.Single(mem => mem.GUID == id);
                        ents.Member_Member.Remove(m_member);
                        ents.SaveChanges();
                    }
                    m_member = new Member_Member();
                    m_member.Id = id;
                    m_member.GUID = id;
                    m_member.BirthPlace = memberDoc.XPathValue("/root/pm:member/pm:personal/pm:born/@pm:place", nms);
                    var bornDate = memberDoc.XPathValue("/root/pm:member/pm:personal/pm:born/@pm:date", nms);
                    if (!String.IsNullOrEmpty(bornDate))
                    {
                        m_member.Born = DateTime.ParseExact(bornDate, "yyyy-MM-dd", null);

                    }
                    else
                    {
                        m_member.Born = MINDT;
                    }
                    var deadDate = memberDoc.XPathValue("/root/pm:member/pm:personal/pm:deceased/@pm:date", nms);
                    if (!String.IsNullOrEmpty(deadDate))
                    {
                        if (deadDate.Length > 10)
                        {
                            m_member.Died = MINDT;
                        }
                        else
                        {
                            m_member.Died = DateTime.ParseExact(deadDate, "yyyy-MM-dd", null);
                        }
                    }


                    m_member.FirstName = memberDoc.XPathValue("/root/pm:member/pm:name/pm:first", nms);
                    m_member.LastName = memberDoc.XPathValue("/root/pm:member/pm:name/pm:last", nms);
                    m_member.OfficialTitle = memberDoc.XPathValue("/root/pm:member/pm:name/pm:full", nms);

                    string gender = memberDoc.XPathValue("/root/pm:member/pm:personal/pm:gender", nms);
                    m_member.Gender = gender.ToLower().StartsWith("f") ? 0 : (gender.ToLower().StartsWith("m") ? 1 : -1);

                    var currics = memberDoc.XPathElements("/root/pm:member/pm:curriculum", nms);
                    if (currics != null)
                    {
                        foreach (var curric in currics)
                        {
                            Member_ExtraCurricular m_excurr = new Member_ExtraCurricular();
                            m_excurr.Source = curric.GetAttributeValue("source");
                            m_excurr.Name = curric.Elements().First().Value;
                            m_excurr.MemberId = id;
                            newMemberExtraCurs.Add(m_excurr);
                        }


                    }
                    var links = memberDoc.XPathElements("/root/pm:member/pm:links", nms);
                    if (links != null)
                    {
                        foreach (var link in links)
                        {
                            Member_Link m_link = new Member_Link();
                            string type = link.GetAttributeValue("linktype");
                            LinkType lt;
                            if (!Enum.TryParse<LinkType>(type, true, out lt))
                            {
                                lt = LinkType.Unknown;
                            }

                            m_link.Type = (int)lt;
                            m_link.Description = link.GetAttributeValue("description");
                            m_link.Source = link.GetAttributeValue("source");
                            m_link.URL = Regex.Replace(link.Value, "^/Users/kasparbeelen/.*?/Source/", m_link.Source + "/Files/");
                            m_link.MemberId = id;
                            newMemberLinks.Add(m_link);
                        }
                    }


                    var memberships = memberDoc.XPathElements("/root/pm:member/pm:memberships", nms);
                    if (memberships != null)
                    {
                        foreach (var membership in memberships)
                        {
                            string partyRef = membership.GetAttributeValue("party-ref");
                            if (String.IsNullOrEmpty(partyRef))
                            {
                                continue;
                            }
                            if (partyRef == "not-available")
                            {
                                string name = membership.GetAttributeValue("party-name");
                                if (String.IsNullOrEmpty(name))
                                {
                                    name = "unknown";
                                }
                                partyRef = "ca.p.special." + new String(name.Where(ch => Char.IsLetterOrDigit(ch)).ToArray()).ToLower();
                            }
                            if (!partyCache.Any(party => party == partyRef))
                            {
                                Member_Party party = new Member_Party();
                                party.Name = membership.GetAttributeValue("party-name");
                                party.Ref = partyRef;
                                newMemberPartys.Add(party);
                                partyCache.Add(party.Ref);
                                //ents.SaveChanges();
                                Console.WriteLine("Discovered new party - {0} ({1})", party.Name, party.Ref);
                            }

                            var periods = membership.Elements();
                            foreach (var period in periods)
                            {
                                Member_Membership m_membership = new Member_Membership();
                                m_membership.Body = membership.GetAttributeValue("body");
                                m_membership.District = membership.GetAttributeValue("district");
                                m_membership.Province = membership.GetAttributeValue("province");
                                m_membership.PartyRef = partyRef;
                                var from = period.GetAttributeValue("from").Replace(".", "-");
                                if (from.Length == 4)
                                    from = from + "-01-01";
                                else if (from.Length == 7)
                                    from = from + "-01";
                                m_membership.From = DateTime.ParseExact(from, "yyyy-MM-dd", null);
                                var to = period.GetAttributeValue("till").Replace(".", "-");
                                if (to != "present")
                                {
                                    if (to.Length == 4)
                                        to = to + "-01-01";
                                    else if (to.Length == 7)
                                        to = to + "-01";
                                    m_membership.To = DateTime.ParseExact(to, "yyyy-MM-dd", null);
                                }
                                m_membership.MemberId = id;
                                newMemberMemberships.Add(m_membership);
                            }
                        }

                    }
                    var affils = memberDoc.XPathElements("/root/pm:member/openpx:party-affiliations", nms);
                    if (affils != null)
                    {
                        foreach (var affil in affils)
                        {
                            var periods = affil.Elements();
                            foreach (var period in periods)
                            {
                                Member_Affiliation m_affil = new Member_Affiliation();
                                m_affil.PartyRef = affil.GetAttributeValue("party-ref");
                                if (m_affil.PartyRef == "not-available")
                                {
                                    string name = affil.GetAttributeValue("party-name");
                                    if (String.IsNullOrEmpty(name))
                                    {
                                        name = "unknown";
                                    }
                                    m_affil.PartyRef = "ca.p.special." + new String(name.Where(ch => Char.IsLetterOrDigit(ch)).ToArray()).ToLower();
                                }
                                if (!partyCache.Any(party => party == m_affil.PartyRef))
                                {
                                    Member_Party party = new Member_Party();
                                    party.Name = affil.GetAttributeValue("party-name");
                                    party.Ref = m_affil.PartyRef;
                                    newMemberPartys.Add(party);
                                    partyCache.Add(party.Ref);
                                    //ents.SaveChanges();
                                    Console.WriteLine("Discovered new party - {0} ({1})", party.Name, party.Ref);
                                }
                                var from = period.GetAttributeValue("from").Replace(".", "-");
                                if (from.Length == 4)
                                    from = from + "-01-01";
                                else if (from.Length == 7)
                                    from = from + "-01";
                                m_affil.From = DateTime.ParseExact(from, "yyyy-MM-dd", null);
                                var to = period.GetAttributeValue("till").Replace(".", "-");
                                if (to != "present")
                                {
                                    if (to.Length == 4)
                                        to = to + "-01-01";
                                    else if (to.Length == 7)
                                        to = to + "-01";
                                    m_affil.To = DateTime.ParseExact(to.Replace(".", "-"), "yyyy-MM-dd", null);
                                }
                                m_affil.MemberId = id;
                                newMemberAffiliations.Add(m_affil);
                            }
                        }

                    }

                    var bios = memberDoc.XPathElements("/root/pm:member/pm:biographies", nms);
                    if (bios != null)
                    {
                        foreach (var bio in bios)
                        {
                            Member_Bio m_bio = new Member_Bio();
                            m_bio.Source = bio.GetAttributeValue("source");
                            m_bio.Bio = bio.Value;
                            m_bio.MemberId = id;
                            newMemberBios.Add(m_bio);

                        }
                    }

                    membersDB.Add(m_member);
                    //ents.Member_Member.Add(m_member);
                    Console.WriteLine("[{3}] Added {0} {1} - {2}", m_member.FirstName, m_member.LastName, m_member.GUID, i);
                }
                Console.WriteLine("Saving...");
                ents.BulkInsert(membersDB);
                ents.BulkInsert(newMemberPartys);
                ents.BulkInsert(newMemberAffiliations);
                ents.BulkInsert(newMemberBios);
                ents.BulkInsert(newMemberExtraCurs);
                ents.BulkInsert(newMemberLinks);
                ents.BulkInsert(newMemberMemberships);
                //ents.Member_Member.AddRange(membersDB);
                ents.SaveChanges();
                Console.WriteLine("Done");
            }

            //ents.Member_Member.AddRange(membersDB);
            ents.Dispose();
            ents = new dilipadEntities();

            membersDB = ents.Member_Member.ToList();
            membersDB.ForEach(mem => memberIds.Add(mem.Id));

            Console.WriteLine("Enter input folder");
            path = Console.ReadLine();
            path = path.Trim('"');
            bool isDir = (File.GetAttributes(path) & FileAttributes.Directory) == FileAttributes.Directory;
            var files = isDir ? Directory.GetFiles(path, "*.xml", SearchOption.AllDirectories) : new string[] { path };

            int count = 0;
            foreach (var file in files)
            {
                count++;
                Console.WriteLine("Working on " + Path.GetFileNameWithoutExtension(file));
                DateTime dt = DateTime.ParseExact(Path.GetFileNameWithoutExtension(file).Split('.').Last(), "MMMM_dd_yyyy", null);
                if (ents.Hansard_Proceedings.Any(hp => hp.Date == dt))
                {
                    Console.WriteLine("Error {0} - hansard with this date already exists", Path.GetFileNameWithoutExtension(file).Split('.').Last());
                    continue;
                }

                Hansard_Proceedings proc = new Hansard_Proceedings()
                {
                    Date = dt,
                    Language = "Eng",
                    Id = dt.ToString("yyyyMMdd")
                };
                newHansardProcs.Add(proc);
                XDocument xdoc = XDocument.Load(file);

                CollectContainers(xdoc.Descendants().First(x => x.Name.LocalName == "proceedings"), null, proc);
                if (count % 100 == 0)
                {
                    Console.WriteLine("Saving...");
                    ents.BulkInsert(newHansardProcs);
                    string longestTitle = newHansardContainers.OrderByDescending(c => c.Title == null? 0 : c.Title.Length).First().Title;
                    ents.BulkInsert(newHansardContainers);
                    ents.BulkInsert(newHansardSpeeches);
                    ents.BulkInsert(newHansardPs);
                    ents.BulkInsert(newHansardSpecElems);
                    newHansardProcs.Clear();
                    newHansardPs.Clear();
                    newHansardContainers.Clear();
                    newHansardSpeeches.Clear();
                    newHansardSpecElems.Clear();
                }
                //ents.Hansard_Proceedings.Add(proc);
                //ents.SaveChanges();
            }

            
        }

        static void CollectContainers(XElement currentContainer, Hansard_ElementContainer lastHContainer, Hansard_Proceedings proceedings)
        {
            var subtopics = currentContainer.Elements().Where(el => "scene topic subtopic".Contains(el.Name.LocalName));
            foreach (var cont in subtopics)
            {
                Hansard_ElementContainer h_container = new Hansard_ElementContainer()
                {
                    ProceedingsId = proceedings.Id,
                    
                    Type = (cont.Name.LocalName == "topic" ? 0 : (cont.Name.LocalName == "subtopic" ? 1 : (cont.Name.LocalName == "scene" ? 2 : -1))),
                    //TagId = cont.GetAttributeValue("id"),
                    Id = cont.GetAttributeValue("id")
                };
                var titleAttr = cont.Attributes().SingleOrDefault(x => x.Name.LocalName == "title");
                if (titleAttr != null)
                {
                    h_container.Title = titleAttr.Value;
                }
                if (lastHContainer != null)
                    h_container.ContainerId = lastHContainer.Id;
                else
                    h_container.ContainerId = null;
                newHansardContainers.Add(h_container);

               

                var speeches = cont.Elements().Where(el => el.Name.LocalName == "speech");
                foreach (var speech in speeches)
                {
                    Hansard_SpeechElement h_speech = new Hansard_SpeechElement();
                    h_speech.Meta = "";
                    var timeAttr = speech.Attributes().SingleOrDefault(attr => attr.Name.LocalName == "time");
                    if (timeAttr != null)
                    {
                        DateTime parsed;
                        if (DateTime.TryParseExact(timeAttr.Value, "HH:mm:ss", null, System.Globalization.DateTimeStyles.None, out parsed))
                            h_speech.Time = parsed;
                        else
                            h_speech.Meta = "time=" + timeAttr.Value + ";";

                    }
                    //h_speech.TagId = speech.GetAttributeValue("id");
                    h_speech.Id = speech.GetAttributeValue("id");
                    string speaker = speech.GetAttributeValue("speaker-id");
                    if (!string.IsNullOrEmpty(speaker) && speaker != "unmatched" && speaker != "intervention" && memberIds.Contains(speaker))
                    {
                        h_speech.SpeakerId = speaker;
                    }
                    else
                    {
                        string speakerLinkResult = "";
                        if (speaker == "unmatched" || speaker == "intervention")
                        {
                            if (speaker == "intervention")
                                speaker += "-or-unparseable";
                            speakerLinkResult = "speaker-link-result=" + speaker + ";";
                            speaker = "";
                        }
                        if (string.IsNullOrEmpty(speaker))
                        {
                            speaker = speech.GetAttributeValue("speaker-old");
                        }
                        if (string.IsNullOrEmpty(speaker))
                        {
                            speaker = speech.GetAttributeValue("speaker");
                        }
                        if (string.IsNullOrEmpty(speaker))
                        {
                            speaker = "UNKNOWN";
                        }
                        h_speech.Meta += "speaker-name=" + speaker + ";" + speakerLinkResult;
                    }
                    h_speech.ContainerId = h_container.Id;
                    newHansardSpeeches.Add(h_speech);
                    var ps = speech.Elements();
                    foreach (var p in ps)
                    {
                        Hansard_P h_p = new Hansard_P();
                        h_p.Text = p.Value;
                        h_p.TagId = p.GetAttributeValue("id");
                        h_p.SpeechId = h_speech.Id;
                        newHansardPs.Add(h_p);
                    }
                }

                var notSpeech = cont.Elements().Where(el => el.Name.LocalName != "speech");
                foreach (var elem in notSpeech)
                {
                    Hansard_SpecialElement h_spec = new Hansard_SpecialElement();
                    h_spec.ContainerId = h_container.Id;
                    //h_spec.TagId = elem.GetAttributeValue("id");
                    h_spec.Id = elem.GetAttributeValue("id");
                    SpecialElementType elemType;
                    if (!Enum.TryParse<SpecialElementType>(elem.Name.LocalName.Replace("-", ""), true, out elemType))
                    {
                        elemType = SpecialElementType.Unknown;
                    }
                    if (elemType == SpecialElementType.StageDirection)
                    {
                        string type = elem.GetAttributeValue("type");
                        if (type != null && type == "question")
                        {
                            string author = elem.GetAttributeValue("question-author");
                            string number = elem.GetAttributeValue("question-num");
                            string authorId = elem.GetAttributeValue("question-author-id");
                            h_spec.Meta = "type=question;question-author=" + author + ";question-num=" + number + ";" + "question-author-id=" + authorId + ";";
                        }
                    }
                    else if (elemType == SpecialElementType.Vote)
                    {
                        h_spec.Meta = "type=vote;";
                        string yays = elem.GetAttributeValue("yeas");
                        string nays = elem.GetAttributeValue("nays");
                        h_spec.Meta += "yeas=" + yays + ";nays=" + nays + ";";
                    }
                    h_spec.Type = (int)elemType;
                    string childContents = "";
                    if (elem.Elements().Count() > 0)
                    {
                        childContents = elem.Elements().Select(x => x.Value).Aggregate((a, b) => a + "\r\n" + b);
                    }
                    h_spec.Content = elem.Value.Trim() + "\r\n" + childContents;
                    newHansardSpecElems.Add(h_spec);
                }
                CollectContainers(cont, h_container, proceedings);
            }
        }
    }
}
