//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace XMLToSQLTransformer
{
    using System;
    using System.Collections.Generic;
    
    public partial class Hansard_ElementContainer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Hansard_ElementContainer()
        {
            this.Hansard_ElementContainer1 = new HashSet<Hansard_ElementContainer>();
            this.Hansard_SpecialElement = new HashSet<Hansard_SpecialElement>();
            this.Hansard_SpeechElement = new HashSet<Hansard_SpeechElement>();
        }
    
        public string Id { get; set; }
        public int Type { get; set; }
        public string Title { get; set; }
        public string ContainerId { get; set; }
        public string ProceedingsId { get; set; }
        public string Meta { get; set; }
        public string TagId { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hansard_ElementContainer> Hansard_ElementContainer1 { get; set; }
        public virtual Hansard_ElementContainer Hansard_ElementContainer2 { get; set; }
        public virtual Hansard_Proceedings Hansard_Proceedings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hansard_SpecialElement> Hansard_SpecialElement { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hansard_SpeechElement> Hansard_SpeechElement { get; set; }
    }
}
