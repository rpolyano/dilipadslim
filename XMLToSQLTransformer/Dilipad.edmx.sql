
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 08/11/2015 22:00:32
-- Generated from EDMX file: E:\Documents\Sources\DilipadExperiments\XMLToSQLTransformer\Dilipad.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[000dilipad].[FK__Hansard_S__Conta__2F10007B]', 'F') IS NOT NULL
    ALTER TABLE [000dilipad].[hansard_speechelement] DROP CONSTRAINT [FK__Hansard_S__Conta__2F10007B];
GO
IF OBJECT_ID(N'[000dilipad].[FK_Member_Affiliation_Member_Member]', 'F') IS NOT NULL
    ALTER TABLE [000dilipad].[member_affiliation] DROP CONSTRAINT [FK_Member_Affiliation_Member_Member];
GO
IF OBJECT_ID(N'[000dilipad].[FK_Member_Membership_ToTable]', 'F') IS NOT NULL
    ALTER TABLE [000dilipad].[member_membership] DROP CONSTRAINT [FK_Member_Membership_ToTable];
GO
IF OBJECT_ID(N'[000dilipad].[FK_Self]', 'F') IS NOT NULL
    ALTER TABLE [000dilipad].[hansard_elementcontainer] DROP CONSTRAINT [FK_Self];
GO
IF OBJECT_ID(N'[000dilipad].[FK_Hansard_ElementContainer_fk1]', 'F') IS NOT NULL
    ALTER TABLE [000dilipad].[hansard_elementcontainer] DROP CONSTRAINT [FK_Hansard_ElementContainer_fk1];
GO
IF OBJECT_ID(N'[000dilipad].[FK_Hansard_P_fk0]', 'F') IS NOT NULL
    ALTER TABLE [000dilipad].[hansard_p] DROP CONSTRAINT [FK_Hansard_P_fk0];
GO
IF OBJECT_ID(N'[000dilipad].[FK_Hansard_SpecialElement_fk0]', 'F') IS NOT NULL
    ALTER TABLE [000dilipad].[hansard_specialelement] DROP CONSTRAINT [FK_Hansard_SpecialElement_fk0];
GO
IF OBJECT_ID(N'[000dilipad].[FK_Hansard_SpeechElement_fk1]', 'F') IS NOT NULL
    ALTER TABLE [000dilipad].[hansard_speechelement] DROP CONSTRAINT [FK_Hansard_SpeechElement_fk1];
GO
IF OBJECT_ID(N'[000dilipad].[FK_Member_Affiliation_fk0]', 'F') IS NOT NULL
    ALTER TABLE [000dilipad].[member_affiliation] DROP CONSTRAINT [FK_Member_Affiliation_fk0];
GO
IF OBJECT_ID(N'[000dilipad].[FK_Member_Bio_fk0]', 'F') IS NOT NULL
    ALTER TABLE [000dilipad].[member_bio] DROP CONSTRAINT [FK_Member_Bio_fk0];
GO
IF OBJECT_ID(N'[000dilipad].[FK_Member_ExtraCurricular_fk0]', 'F') IS NOT NULL
    ALTER TABLE [000dilipad].[member_extracurricular] DROP CONSTRAINT [FK_Member_ExtraCurricular_fk0];
GO
IF OBJECT_ID(N'[000dilipad].[FK_Member_Link_fk0]', 'F') IS NOT NULL
    ALTER TABLE [000dilipad].[member_link] DROP CONSTRAINT [FK_Member_Link_fk0];
GO
IF OBJECT_ID(N'[000dilipad].[FK_Member_Membership_fk0]', 'F') IS NOT NULL
    ALTER TABLE [000dilipad].[member_membership] DROP CONSTRAINT [FK_Member_Membership_fk0];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[000dilipad].[hansard_elementcontainer]', 'U') IS NOT NULL
    DROP TABLE [000dilipad].[hansard_elementcontainer];
GO
IF OBJECT_ID(N'[000dilipad].[hansard_p]', 'U') IS NOT NULL
    DROP TABLE [000dilipad].[hansard_p];
GO
IF OBJECT_ID(N'[000dilipad].[hansard_proceedings]', 'U') IS NOT NULL
    DROP TABLE [000dilipad].[hansard_proceedings];
GO
IF OBJECT_ID(N'[000dilipad].[hansard_specialelement]', 'U') IS NOT NULL
    DROP TABLE [000dilipad].[hansard_specialelement];
GO
IF OBJECT_ID(N'[000dilipad].[hansard_speechelement]', 'U') IS NOT NULL
    DROP TABLE [000dilipad].[hansard_speechelement];
GO
IF OBJECT_ID(N'[000dilipad].[member_affiliation]', 'U') IS NOT NULL
    DROP TABLE [000dilipad].[member_affiliation];
GO
IF OBJECT_ID(N'[000dilipad].[member_bio]', 'U') IS NOT NULL
    DROP TABLE [000dilipad].[member_bio];
GO
IF OBJECT_ID(N'[000dilipad].[member_extracurricular]', 'U') IS NOT NULL
    DROP TABLE [000dilipad].[member_extracurricular];
GO
IF OBJECT_ID(N'[000dilipad].[member_link]', 'U') IS NOT NULL
    DROP TABLE [000dilipad].[member_link];
GO
IF OBJECT_ID(N'[000dilipad].[member_member]', 'U') IS NOT NULL
    DROP TABLE [000dilipad].[member_member];
GO
IF OBJECT_ID(N'[000dilipad].[member_membership]', 'U') IS NOT NULL
    DROP TABLE [000dilipad].[member_membership];
GO
IF OBJECT_ID(N'[000dilipad].[member_party]', 'U') IS NOT NULL
    DROP TABLE [000dilipad].[member_party];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'hansard_elementcontainer'
CREATE TABLE [dbo].[hansard_elementcontainer] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Type] int  NOT NULL,
    [Title] varchar(256)  NOT NULL,
    [ContainerId] int  NULL,
    [ProceedingsId] int  NOT NULL,
    [Meta] longtext  NULL,
    [TagId] varchar(64)  NOT NULL
);
GO

-- Creating table 'hansard_p'
CREATE TABLE [dbo].[hansard_p] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Text] longtext  NOT NULL,
    [SpeechId] int  NOT NULL,
    [Meta] longtext  NULL,
    [TagId] varchar(64)  NOT NULL
);
GO

-- Creating table 'hansard_proceedings'
CREATE TABLE [dbo].[hansard_proceedings] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [Language] varchar(3)  NOT NULL
);
GO

-- Creating table 'hansard_specialelement'
CREATE TABLE [dbo].[hansard_specialelement] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Type] int  NOT NULL,
    [Meta] longtext  NULL,
    [ContainerId] int  NOT NULL,
    [Content] longtext  NOT NULL,
    [TagId] varchar(64)  NOT NULL
);
GO

-- Creating table 'hansard_speechelement'
CREATE TABLE [dbo].[hansard_speechelement] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Type] int  NOT NULL,
    [Time] datetime  NULL,
    [ContainerId] int  NOT NULL,
    [SpeakerId] int  NULL,
    [Meta] longtext  NULL,
    [TagId] varchar(64)  NOT NULL
);
GO

-- Creating table 'member_affiliation'
CREATE TABLE [dbo].[member_affiliation] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PartyRef] varchar(128)  NOT NULL,
    [From] datetime  NOT NULL,
    [To] datetime  NULL,
    [MemberId] int  NOT NULL
);
GO

-- Creating table 'member_bio'
CREATE TABLE [dbo].[member_bio] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Source] varchar(256)  NOT NULL,
    [Bio] longtext  NOT NULL,
    [MemberId] int  NOT NULL
);
GO

-- Creating table 'member_extracurricular'
CREATE TABLE [dbo].[member_extracurricular] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] varchar(256)  NOT NULL,
    [Source] varchar(256)  NULL,
    [MemberId] int  NOT NULL
);
GO

-- Creating table 'member_link'
CREATE TABLE [dbo].[member_link] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Type] int  NOT NULL,
    [Source] varchar(256)  NULL,
    [Description] varchar(256)  NULL,
    [URL] varchar(256)  NOT NULL,
    [MemberId] int  NOT NULL
);
GO

-- Creating table 'member_member'
CREATE TABLE [dbo].[member_member] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [OfficialTitle] varchar(256)  NOT NULL,
    [FirstName] varchar(256)  NOT NULL,
    [LastName] varchar(256)  NOT NULL,
    [Born] datetime  NULL,
    [Died] datetime  NULL,
    [BirthPlace] varchar(128)  NULL,
    [GUID] varchar(64)  NOT NULL,
    [Gender] int  NOT NULL
);
GO

-- Creating table 'member_membership'
CREATE TABLE [dbo].[member_membership] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Body] varchar(64)  NOT NULL,
    [District] varchar(128)  NOT NULL,
    [Province] varchar(128)  NOT NULL,
    [PartyRef] varchar(128)  NOT NULL,
    [From] datetime  NOT NULL,
    [To] datetime  NULL,
    [MemberId] int  NOT NULL
);
GO

-- Creating table 'member_party'
CREATE TABLE [dbo].[member_party] (
    [Ref] varchar(128)  NOT NULL,
    [Name] varchar(128)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'hansard_elementcontainer'
ALTER TABLE [dbo].[hansard_elementcontainer]
ADD CONSTRAINT [PK_hansard_elementcontainer]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'hansard_p'
ALTER TABLE [dbo].[hansard_p]
ADD CONSTRAINT [PK_hansard_p]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'hansard_proceedings'
ALTER TABLE [dbo].[hansard_proceedings]
ADD CONSTRAINT [PK_hansard_proceedings]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'hansard_specialelement'
ALTER TABLE [dbo].[hansard_specialelement]
ADD CONSTRAINT [PK_hansard_specialelement]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'hansard_speechelement'
ALTER TABLE [dbo].[hansard_speechelement]
ADD CONSTRAINT [PK_hansard_speechelement]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'member_affiliation'
ALTER TABLE [dbo].[member_affiliation]
ADD CONSTRAINT [PK_member_affiliation]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'member_bio'
ALTER TABLE [dbo].[member_bio]
ADD CONSTRAINT [PK_member_bio]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'member_extracurricular'
ALTER TABLE [dbo].[member_extracurricular]
ADD CONSTRAINT [PK_member_extracurricular]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'member_link'
ALTER TABLE [dbo].[member_link]
ADD CONSTRAINT [PK_member_link]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'member_member'
ALTER TABLE [dbo].[member_member]
ADD CONSTRAINT [PK_member_member]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'member_membership'
ALTER TABLE [dbo].[member_membership]
ADD CONSTRAINT [PK_member_membership]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Ref] in table 'member_party'
ALTER TABLE [dbo].[member_party]
ADD CONSTRAINT [PK_member_party]
    PRIMARY KEY CLUSTERED ([Ref] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ContainerId] in table 'hansard_speechelement'
ALTER TABLE [dbo].[hansard_speechelement]
ADD CONSTRAINT [FK__Hansard_S__Conta__2F10007B]
    FOREIGN KEY ([ContainerId])
    REFERENCES [dbo].[hansard_elementcontainer]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Hansard_S__Conta__2F10007B'
CREATE INDEX [IX_FK__Hansard_S__Conta__2F10007B]
ON [dbo].[hansard_speechelement]
    ([ContainerId]);
GO

-- Creating foreign key on [ContainerId] in table 'hansard_elementcontainer'
ALTER TABLE [dbo].[hansard_elementcontainer]
ADD CONSTRAINT [FK_Self]
    FOREIGN KEY ([ContainerId])
    REFERENCES [dbo].[hansard_elementcontainer]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Self'
CREATE INDEX [IX_FK_Self]
ON [dbo].[hansard_elementcontainer]
    ([ContainerId]);
GO

-- Creating foreign key on [ProceedingsId] in table 'hansard_elementcontainer'
ALTER TABLE [dbo].[hansard_elementcontainer]
ADD CONSTRAINT [FK_Hansard_ElementContainer_fk1]
    FOREIGN KEY ([ProceedingsId])
    REFERENCES [dbo].[hansard_proceedings]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Hansard_ElementContainer_fk1'
CREATE INDEX [IX_FK_Hansard_ElementContainer_fk1]
ON [dbo].[hansard_elementcontainer]
    ([ProceedingsId]);
GO

-- Creating foreign key on [ContainerId] in table 'hansard_specialelement'
ALTER TABLE [dbo].[hansard_specialelement]
ADD CONSTRAINT [FK_Hansard_SpecialElement_fk0]
    FOREIGN KEY ([ContainerId])
    REFERENCES [dbo].[hansard_elementcontainer]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Hansard_SpecialElement_fk0'
CREATE INDEX [IX_FK_Hansard_SpecialElement_fk0]
ON [dbo].[hansard_specialelement]
    ([ContainerId]);
GO

-- Creating foreign key on [SpeechId] in table 'hansard_p'
ALTER TABLE [dbo].[hansard_p]
ADD CONSTRAINT [FK_Hansard_P_fk0]
    FOREIGN KEY ([SpeechId])
    REFERENCES [dbo].[hansard_speechelement]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Hansard_P_fk0'
CREATE INDEX [IX_FK_Hansard_P_fk0]
ON [dbo].[hansard_p]
    ([SpeechId]);
GO

-- Creating foreign key on [SpeakerId] in table 'hansard_speechelement'
ALTER TABLE [dbo].[hansard_speechelement]
ADD CONSTRAINT [FK_Hansard_SpeechElement_fk1]
    FOREIGN KEY ([SpeakerId])
    REFERENCES [dbo].[member_member]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Hansard_SpeechElement_fk1'
CREATE INDEX [IX_FK_Hansard_SpeechElement_fk1]
ON [dbo].[hansard_speechelement]
    ([SpeakerId]);
GO

-- Creating foreign key on [MemberId] in table 'member_affiliation'
ALTER TABLE [dbo].[member_affiliation]
ADD CONSTRAINT [FK_Member_Affiliation_Member_Member]
    FOREIGN KEY ([MemberId])
    REFERENCES [dbo].[member_member]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Member_Affiliation_Member_Member'
CREATE INDEX [IX_FK_Member_Affiliation_Member_Member]
ON [dbo].[member_affiliation]
    ([MemberId]);
GO

-- Creating foreign key on [PartyRef] in table 'member_affiliation'
ALTER TABLE [dbo].[member_affiliation]
ADD CONSTRAINT [FK_Member_Affiliation_fk0]
    FOREIGN KEY ([PartyRef])
    REFERENCES [dbo].[member_party]
        ([Ref])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Member_Affiliation_fk0'
CREATE INDEX [IX_FK_Member_Affiliation_fk0]
ON [dbo].[member_affiliation]
    ([PartyRef]);
GO

-- Creating foreign key on [MemberId] in table 'member_bio'
ALTER TABLE [dbo].[member_bio]
ADD CONSTRAINT [FK_Member_Bio_fk0]
    FOREIGN KEY ([MemberId])
    REFERENCES [dbo].[member_member]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Member_Bio_fk0'
CREATE INDEX [IX_FK_Member_Bio_fk0]
ON [dbo].[member_bio]
    ([MemberId]);
GO

-- Creating foreign key on [MemberId] in table 'member_extracurricular'
ALTER TABLE [dbo].[member_extracurricular]
ADD CONSTRAINT [FK_Member_ExtraCurricular_fk0]
    FOREIGN KEY ([MemberId])
    REFERENCES [dbo].[member_member]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Member_ExtraCurricular_fk0'
CREATE INDEX [IX_FK_Member_ExtraCurricular_fk0]
ON [dbo].[member_extracurricular]
    ([MemberId]);
GO

-- Creating foreign key on [MemberId] in table 'member_link'
ALTER TABLE [dbo].[member_link]
ADD CONSTRAINT [FK_Member_Link_fk0]
    FOREIGN KEY ([MemberId])
    REFERENCES [dbo].[member_member]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Member_Link_fk0'
CREATE INDEX [IX_FK_Member_Link_fk0]
ON [dbo].[member_link]
    ([MemberId]);
GO

-- Creating foreign key on [MemberId] in table 'member_membership'
ALTER TABLE [dbo].[member_membership]
ADD CONSTRAINT [FK_Member_Membership_ToTable]
    FOREIGN KEY ([MemberId])
    REFERENCES [dbo].[member_member]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Member_Membership_ToTable'
CREATE INDEX [IX_FK_Member_Membership_ToTable]
ON [dbo].[member_membership]
    ([MemberId]);
GO

-- Creating foreign key on [PartyRef] in table 'member_membership'
ALTER TABLE [dbo].[member_membership]
ADD CONSTRAINT [FK_Member_Membership_fk0]
    FOREIGN KEY ([PartyRef])
    REFERENCES [dbo].[member_party]
        ([Ref])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Member_Membership_fk0'
CREATE INDEX [IX_FK_Member_Membership_fk0]
ON [dbo].[member_membership]
    ([PartyRef]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------