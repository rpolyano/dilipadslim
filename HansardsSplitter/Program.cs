﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HansardsSplitter
{
    class Program
    {
        static string[] DAY_START_PHRASES = new string[] {
            "^the speaker took the chair",
            "^the house met"
        };
        static void Main(string[] args)
        {
            //List<string> randomSample = new List<string>();
            Random rnd = new Random();
            Console.WriteLine("Drag file/folder:");
            string path = Console.ReadLine();
            path = path.Trim('"');

            bool isDir = (File.GetAttributes(path) & FileAttributes.Directory) == FileAttributes.Directory;

            String[] files = isDir ? Directory.GetFiles(path, "*.txt") : new string[] { path };
            
            if (!Directory.Exists("output"))
            {
                Directory.CreateDirectory("output");
            }
           
            int day = 0;
            for (int filei = 0; filei < files.Length; filei++)
            {
                
                string text = File.ReadAllText(files[filei]);
                int daySplitIndex = -1;
                for (int dspii = 0; dspii < DAY_START_PHRASES.Length; dspii++)
                {
                    if (Regex.IsMatch(text, DAY_START_PHRASES[dspii], RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Multiline))
                    {
                        daySplitIndex = dspii;
                        break;
                    }
                }
                if (daySplitIndex == -1)
                {
                    Console.WriteLine("Can't find suitable split for " + Path.GetFileName(files[filei]));
                    File.AppendAllText("bad.log", Path.GetFileName(files[filei]) + "\r\n");
                    continue;
                }
                string[] days = Regex.Split(text, "(?:" + DAY_START_PHRASES[daySplitIndex] + ")", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Multiline);
                
                Console.WriteLine(Path.GetFileName(files[filei]) + " has " + days.Length + " days");
                int prevYear = 0;
                for (int dayi = 0; dayi < days.Length; dayi++)
                {
                    bool copyRandom = false;
                    string dayText = days[dayi];
                    var dates = dayText.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                        .Select(line =>
                            {
                                var parts = line.Split("()[]{}'".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                if (parts.Length == 0)
                                    return DateTime.MinValue;
                                line = parts.First().Trim('.');
                                DateTime parsed;
                                if (DateTime.TryParse(line, out parsed))
                                {
                                    return parsed;
                                }
                                return DateTime.MinValue;
                            }).Where(dt => dt != DateTime.MinValue)
                            .GroupBy(dt=>dt)
                            .OrderByDescending(grp=>grp.Count()).ToArray();
                    string name = "";
                    if (dates.Count() > 0)
                    {
                        DateTime mostCommon = dates.First().Key;
                        if (!dates.Any(dt => dt.Key.Year == prevYear))
                        {
                            prevYear = mostCommon.Year;
                        }
                        else
                        {
                            mostCommon = dates.First(dt => dt.Key.Year == prevYear).Key;
                        }

                        name = mostCommon.ToString("MMMM_dd_yyyy");//day + "." + mostCommon.ToString("MMM_dd_yyyy");
                        if (mostCommon.Year > 1900 && mostCommon.Year < 1994)
                            copyRandom = true;
                        //if (mostCommon.Year < 1999 && mostCommon.Year >= 1915)
                        //{
                            //if (rnd.Next(0, 10000) < 4)
                            //{
                            //    copyRandom = true;
                            //}
                        //}
                    }
                    else
                    {
                        name = day + "";
                    }
                   
                    day++;
                    Console.WriteLine(name);
                    dayText = dayText.Substring(dayText.IndexOf('\n'));
                    File.AppendAllText("output/" + name + ".txt", dayText);
                    if (copyRandom)
                    {
                        File.AppendAllText("sample/" + name + ".txt", dayText);
                    }
                    //using (var writer = new StreamWriter("output/" + name + ".txt", true))
                    //{
                        
                    //    writer.Write(dayText);
                    //}
                    
                    
                    
                }
                
            }
            //int numSamples = 100;
            //string[] outFiles = Directory.GetFiles("output", "*.txt");
            //outFiles = outFiles.Where(file =>
            //{
            //    int year = Int32.Parse(Path.GetFileNameWithoutExtension(file).Split('_').Last());
            //    return year > 1900 && year < 1999 && rnd.Next(0, outFiles.Length) < numSamples;
            //}).ToArray();
            //Directory.Delete("sample", true);
            //Directory.CreateDirectory("sample");
            //foreach (var file in outFiles)
            //{
            //    File.Copy(file, "sample/" + Path.GetFileName(file));
            //}
        }
    }
}
