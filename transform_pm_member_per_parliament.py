#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib,urllib2
from bs4 import BeautifulSoup as bs
from datetime import datetime as dt
import urlparse
from lxml import etree,isoschematron
import codecs
import re
import os
import time
import requests
import subprocess

class parlinfo_scraper:
	"""
	Scrape all links for one legislature from the Parliamentary File on ParlInfo, link is provided in self.links 
	"""
	def __init__(self):
		#self.links = ["http://www.parl.gc.ca/parlinfo/Files/Parliament.aspx?Item=d7a27573-9899-4e0b-b87d-83a4753ed864&Language=E&MenuID=Lists.Parliament.aspx&MenuQuery=http%3A%2F%2Fwww.parl.gc.ca%2Fparlinfo%2FLists%2FParliament.aspx&Section=ALL"]
		self.links = ["http://www.parl.gc.ca/Parlinfo/Files/Parliament.aspx?Item=4ae94798-0b56-4f9c-b48e-d2fb6c7aede6&Language=E&MenuID=Lists.Parliament.aspx&MenuQuery=http%3A%2F%2Fwww.parl.gc.ca%2FParlinfo%2FLists%2FParliament.aspx&Section=ALL"]
		self.members = []
		#
		
	def get_links(self):
		"""
		Get all links from the Parliamentary File and save it to a documents
		"""
		output = codecs.open("/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/33rdParliament/33rdParliamentMembers/links.txt","w",encoding="utf-8")
		members = []
		for i in self.links:
			page = urllib2.urlopen(i)
			soup = bs(page.read())
			mlinks = soup.findAll("a",attrs={"href":re.compile(ur"^Parliamentarian\.aspx.+$")})
			print len(mlinks)
			for m in mlinks:
				print m["href"]
				#new_page = urllib2.urlopen("http://www.parl.gc.ca/parlinfo/Files/"+m["href"]+"&Section=ALL").read()
				#new_soup = bs(new_page.read())
				#output = codecs.open('/Users/kasparbeelen/Documents/Dilipad/Parlinfo/Source/%s'%m["href"],"w",encoding="utf-8").write(new_page)
				#output.close()
				members.append(m.getText()+"\t"+"http://www.parl.gc.ca/parlinfo/Files/"+m["href"]+"&Section=ALL")
			time.sleep(1)
		self.members = list(set(members))
		output.write(u"\n".join(self.members))
		output.close()

	def scrape(self):
		"""
		Scrape all the Parliamentary File of each MP in the links.txt documents and save it
		"""
		downloaded = os.listdir("/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/33rdParliament/33rdParliamentMembers/Source")
		links = codecs.open("/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/33rdParliament/33rdParliamentMembers/links.txt","r",encoding="utf-8").read().split("\n")
		for row in links:
			n,url = row.split("\t")
			if url.split("/")[-1] in downloaded:
				print "downloaded %s" % url
				continue
			else:
				print "downloading %s"%url
				new_page = urllib2.urlopen(url).read()
				new_page = u"%s" % new_page.decode('utf-8')
				output = codecs.open(os.path.join("/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/33rdParliament/33rdParliamentMembers/Source",url.split('/')[-1]),"w",encoding="utf-8")
				output.write(new_page)
				output.close()
				time.sleep(1)



class member_file:
	"""
	The class transforms the ParlInfo file in html to and XML file in PM-members format
	"""
	def __init__(self,name,url):

		self.url = url
		self.name = name
		#self.soup = bs(urllib2.urlopen(self.url).read())
		self.soup = bs(codecs.open(url,"r",encoding="utf-8").read())
		self.title = self.soup.find('span', {'id':"ctl00_cphContent_lblTitle"}).getText()
		self.url = self.url.replace('/Users/kasparbeelen/Documents/Dilipad/Parlinfo/Source/','http://www.parl.gc.ca/parlinfo/Files/')
		self.namespaces = {"pm":"http://www.politicalmashup.nl",\
						"dc":"http://purl.org/dc/elements/1.1/",\
						"xsd":"http://www.w3.org/2001/XMLSchema-datatypes",\
						"owl":"http://www.w3.org/2002/07/owl#",\
						"pmd":"http://www.politicalmashup.nl/docinfo",\
						"rdf":"http://www.w3.org/1999/02/22-rdf-syntax-ns#",\
						"html":"http://www.w3.org/1999/xhtml",\
						"dcterms":"http://purl.org/dc/terms/",\
						"xsi":"http://www.w3.org/2001/XMLSchema-instance",\
						"pmx":"http://www.politicalmashup.nl/extra",\
						"openpx":"https://openparliament.ca/extra"}
		
		self.root = etree.Element("root",nsmap=self.namespaces)
		self.key = self.url.split('Item=')[1].split("&")[0]
		self.output = codecs.open("/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/33rdParliament/33rdParliamentMembers/ca-members/%s.xml"%self.key,"w",encoding="utf-8")
		self.deceased = None
		self.wiki_links = codecs.open("/Users/kasparbeelen/Documents/Dilipad/Parlinfo/dbwikipi_links_extended.txt","r",encoding="utf-8").read().split('\n')

	def make_element(self,parent,name,ns):
		return etree.SubElement(parent,"{%s}%s"%(self.namespaces[ns],name),nsmap={ns:self.namespaces[ns]})

	def make_element_without_ns(self,parent,name):
		return etree.SubElement(parent,"%s"%name)

	def set(self,element,name,value,ns):
		return element.set("{%s}%s"%(self.namespaces[ns],name),value)

	def add_docinfo(self):
		docinfo = self.make_element(self.root,"docinfo","pmd")
		comment = self.make_element(docinfo,"comment","pm")
		comment.text = "Data is valid with respect to the Relax NG schema http://schema.politicalmashup.nl/memberX.html. This is the open version of http://schema.politicalmashup.nl/member.html"

	def write_output(self):
		self.output.write(etree.tostring(self.root,method="xml",pretty_print=True,encoding="unicode"))
		self.output.close()

	def add_meta(self):
		meta = self.make_element_without_ns(self.root,"meta")
		#self.set(meta,"id",self.key+".meta","pm") # TO DO: change later
		self.set(meta,"id",self.key,"pm") # TO DO: change later
		identitifier = self.make_element(meta,"identifier","dc")
		identitifier.text = ""
		format = self.make_element(meta,"format","dc")
		format.text = "text/xml"
		Type = self.make_element(meta,"type","dc")
		Type.text = "Members"
		contributor = self.make_element(meta,"contributor","dc")
		contributor.text = "http://www.politicalmashup.nl"
		coverage = self.make_element(meta,"coverage","dc")
		country = self.make_element_without_ns(coverage,"country")
		self.set(country,"ISO3166-1","CA","dcterms")
		country.text = "Canada"
		creator = self.make_element(meta,"creator","dc")
		creator.text =  "http://dilipad.history.ac.uk"
		language = self.make_element(meta,"language","dc")
		plang = self.make_element(language,"language","pm")
		self.set(plang,"ISO639-2","eng","dcterms")
		plang.text = "English"
		publisher = self.make_element(meta,"publisher","dc")
		publisher.text = "http://www.parl.gc.ca/parlinfo/"
		rights = self.make_element(meta,"rights","dc")
		rights.text = "http://www.parl.gc.ca/parlinfo/"
		date = self.make_element(meta,"date","dc")
		date.text = dt.isoformat(dt.now()).split('T')[0]+"+01:00"
		title = self.make_element(meta,"title","dc")
		title.text = self.title
		description = self.make_element(meta,"description","dc")
		description.text = "Member information for: %s" % self.title
		source = self.make_element(meta,"source","dc")
		#self.make_source(source,"http://www.politicalmashup.nl","trusted")
		self.make_source(source,"http://www.parl.gc.ca/parlinfo","trusted")
		self.make_source(source,self.url,"trusted")
		self.make_element(meta,"subject","dc")
		relation = self.make_element(meta,"relation","dc")
		relation.text = "All pm:party-ref attributes appearing in http://www.politicalmashup.nl documents refer to http://resolver.politicalmashup.nl/[pm:party-ref] ."

	def make_source(self,source,url,linktype):
		subsource = self.make_element(source,"source","dc")
		self.set(subsource,"used-source","true","pm")
		self.create_link(subsource,url=url,linktype=linktype,source=url)

	def make_name(self,member):
		
		name = self.make_element(member,"name","pm")
		full = self.make_element(name,"full","pm")
		full.text = self.title
		
		name_elements = [n.strip() for n in self.title.split(',')]
		address_title = ""
		firstName_elements = name_elements[1].split('. ')
		#print firstName
		if len(firstName_elements) >= 2:
			address_title = firstName_elements[0]
			#firstName = firstName[1]
			
		# TO DO: delete title "the hon.""

		#lastName = name_elements[0]
		lastName,firstName = self.name.split(', ')


		try:
			titles = name_elements[2:]
		except IndexError:
			titles = None

		first = self.make_element(name,"first","pm")
		first.text = firstName

		last = self.make_element(name,"last","pm")
		last.text = lastName

		if not address_title == "" or not titles == None:
			titles_node = self.make_element(name,"titles","pm")
		
		if not address_title == "":
			title = self.make_element(titles_node,"title","pm")
			title.text = address_title+"."
			self.set(title,"position","prefix","pm")
			self.set(title,"honorific","honorary","pm")

		if not titles == None:	
			for hon in titles:
				title = self.make_element(titles_node,"title","pm")
				title.text = hon
				self.set(title,"position","suffix","pm")
				self.set(title,"honorific","academic","pm")

	def date_granularity(self,date,granularity):
		"""
		granularity is the precision of the date
		if granularity = 4, it means only the year is given
		"""
		if granularity == "4":
			date = str(date)+"-01-01"
			return date
		elif granularity == "6":
			date = str(date)+"-01"
			return date
		else:
			return date
		#else:
		#	date = date.split('\n')[0].strip()
		#	gr = str(len(re.findall("[0-9]",date)))
		#	date = self.date_granularity(date,gr)
		#	return date



	def make_dateofbirth(self,personal):
		try:
			dateOfBirth = self.soup.find('span', {'id':"ctl00_cphContent_DateOfBirthData"}).getText()
		except AttributeError:
			dateOfBirth ="NA"
		try:
			placeOfBirth = self.soup.find('span', {'id':"ctl00_cphContent_PlaceOfBirthData"}).getText()
		except AttributeError:
			placeOfBirth = "NA"
		
		if not dateOfBirth == "NA":
			born = self.make_element(personal,"born","pm")
			dateOfBirth = re.sub('\.',"-",dateOfBirth)
			granularity = str(len(re.findall('[0-9]',dateOfBirth)))
			dateOfBirth = self.date_granularity(dateOfBirth,granularity)
			self.set(born,"date",dateOfBirth,"pm")
			self.set(born,"granularity",granularity,"pm")
			
			if not placeOfBirth == "NA":
				self.set(born,"place",placeOfBirth,"pm")

	def make_deceased(self,personal):
		try:
			dateDeceased = self.soup.find('span', {'id':"ctl00_cphContent_DeceasedDateData"}).getText()

		except AttributeError:
			dateDeceased = "alive"

		if not dateDeceased == "alive":
			deceased = self.make_element(personal,"deceased","pm")
			dateDeceased = dateDeceased.split(" (")[0]
			dateDeceased = re.sub('\.',"-",dateDeceased)
			self.deceased = dateDeceased
			granularity = str(len(re.findall('[0-9]',dateDeceased)))
			dateDeceased = self.date_granularity(dateDeceased,granularity)
			self.set(deceased,"date",dateDeceased,"pm")
			self.set(deceased,"granularity",granularity,"pm")

	def make_occupation(self,member):
		try:
			occupation = self.soup.find('span', {'id':"ctl00_cphContent_ProfessionsData"}).getText()
		except AttributeError:
			occupation = "NA"

		if not occupation == "NA":
			curriculum = self.make_element(member,"curriculum","pm")
			function = self.make_element(curriculum,"extra-curricular","pm")
			self.set(function,'source',"http://www.parl.gc.ca/parlinfo/","pm")
			function_name = self.make_element(function,"name","pm")
			function_name.text = occupation

	def make_links(self,member):
		links = self.make_element(member,"links","pm")
		self.create_link(links,self.url,"trusted","http://www.parl.gc.ca/parlinfo","Parlinfo")
		try:
			img = self.soup.find('img',attrs={"id":"ctl00_cphContent_imgParliamentarianPicture"})
			#url = urlparse.urljoin("http://www.parl.gc.ca/parlinfo",img["src"][2:]) # find a better solution for skipping the .., try split?
		
			url = "http://www.parl.gc.ca/parlinfo"+img["src"][2:] # find a better solution for skipping the .., try split?
			self.create_link(links,url,"photo","http://www.parl.gc.ca/parlinfo","Portrait photo Parlinfo")
		except AttributeError:
			pass

		try:
			links_pi = self.soup.find("div", {"id":"ctl00_cphContent_dataLinks"})
			for a in links_pi.findAll('a'):
				self.create_link(links,a["href"],"trusted","http://www.parl.gc.ca/parlinfo",a.text)
		except AttributeError:
			pass

		for row in self.wiki_links:
			pi,db,wiki = row.split('\t')
			if pi.lower().strip() == self.key.lower().strip():
				print pi
				self.create_link(links,db,"dbpedia","http://dbpedia.org","Dbpedia ()")
				self.create_link(links,wiki,"wikipedia","http://en.wikipedia.org","Wikipedia (EN)")

	def create_link(self,links,url,linktype="",source="",description=""):
		link = self.make_element(links,"link","pm")
		if not linktype == "": self.set(link,"linktype",linktype,"pm")
		if not source == "": self.set(link,"source",source,"pm")
		if not description == "": self.set(link,"description",description,"pm")
		if "MP profil" in description:
			try:
				opener = urllib2.build_opener(urllib2.HTTPRedirectHandler)
				request = opener.open(url)
				url = request.url
			except Exception:
				pass
		link.text = url

	def _convert_htmltable_to_list(self,table,printhtml=False):
		"""
		converts the html tables to python nested list
		"""
		new_table = []
		for trs in table.findAll('tr'):
			row = []
			tds = trs.findAll('td')
			for td in tds:

				text = td.getText()
				text = text.strip()
				if printhtml == True: print text
				# I have some problems with line endings but can't fully remember where they occured
				if "-\n" in text or "-\r" in text: text = re.compile(r'([^\.\-0-9])').sub('',text)
				else: text = text.split('\n')[0] 
				row.append(text)
				
			if not row == []:
				new_table.append(row)
		return new_table
	
	def make_gender(self,personal):
		"""
		the name of female mps are stored in a separate file
		this gender_file is used to assign gender to the members
		see function get_female_mps() below
		"""
		gender_file = codecs.open('/Users/kasparbeelen/Documents/Dilipad/Parlinfo/womenmp.txt',"r",encoding="utf-8").read()
		gender = self.make_element(personal,"gender","pm")
		rows = gender_file.split('\n')
		titles = [r.split("\t")[0] for r in rows]
		if self.title in titles:
			gender.text = "female"
		else:
			gender.text = "male"

	def make_memberofparliamentrole(self,memberships):
		"""
		make membership for each time a person was elected to the House of Commons
		"""
		try: 
			table = self.soup.find("table", {"id":re.compile(r".+?(?:_grdHouseOfCommons)$")})
			table = self._convert_htmltable_to_list(table)
			for row in table:
				membership = self.make_element(memberships,"membership","pm")
				if row[-1] == "Elected":
					self.set(membership,"body","commons","pm")
					province = row[0].split(', ')[-1]
					district = ", ".join(row[0].split(', ')[:-1])
					self.set(membership,"district",district,"pm")
					self.set(membership,"province",province,"openpx")
					self.set(membership,"party-name",row[1],"pm")
					period = self.make_element(membership,"period","pm")
					date = row[2].replace('.',"-")
					granularity = str(len(re.findall("[0-9]",date)))
					date = self.date_granularity(date,granularity)
					self.set(period,"from",date,"pm")
					self.set(period,"from-granularity",granularity,"pm")
					# because Parlinfo doesn't set end dates for party membership and the period time range of membership in the House of Commons
					# it is assumed here that the members is a party member until the end of the legislature
					# see self.get_end_date function
					end_date = self.get_end_date(date)
					if end_date == "present": 
						self.set(period,"till","present","pm")
						self.set(period,"till-granularity","8","pm")
						continue
					granularity = str(len(re.findall("[0-9]",end_date)))
					end_date = self.date_granularity(end_date,granularity)
					self.set(period,"till",end_date,"pm")
					self.set(period,"till-granularity",granularity,"pm")

		except AttributeError:
			pass

	def make_memberofparliamentrole_senate(self,memberships):

		try: 
			table = self.soup.find("table", {"id":re.compile(r".+?(?:_grdSenatorialDivisions)$")})
			#table = self.soup.find("table", {"id":"ctl00_cphContent_ctl01_grdHouseOfCommons"})
			table = self._convert_htmltable_to_list(table)
			for row in table:
				membership = self.make_element(memberships,"membership","pm")
				#if not row[-1] == "Elected":
				self.set(membership,"body","senate","pm")
				province = row[0].split(', ')[-1]
				district = ", ".join(row[0].split(', ')[:-1])
				#self.set(membership,"district",row[0],"pm")
				self.set(membership,"district",district,"pm")
				self.set(membership,"province",province,"openpx")
				self.set(membership,"party-name",row[1],"pm")
				period = self.make_element(membership,"period","pm")
				dates = row[2].split("-")
				date = dates[0].replace('.',"-")
				granularity = str(len(re.findall("[0-9]",date)))
				date = self.date_granularity(date,granularity)
				self.set(period,"from",date,"pm")
				self.set(period,"from-granularity",granularity,"pm")
				if len(dates) == 2:
					date = dates[1].replace('.',"-")
					granularity = str(len(re.findall("[0-9]",date)))
					date = self.date_granularity(date,granularity)
					self.set(period,"till",date,"pm")
					self.set(period,"till-granularity",granularity,"pm")
				else:
					self.set(period,"till","present","pm")
					self.set(period,"till-granularity","8","pm")

		except AttributeError:
			pass

	def make_roleasparliamentarian(self,memberships):
		"""
		make memberships detailing the function of each MP
		restrict the memberships to the one with "Portfolio" in the heading of the table
		Later we could add committee memberships as well
		"""
		tables = self.soup.findAll("table",attrs={"id":re.compile(r".+?(?:_grdParliamentaryFunctionsRole)$")})
		for table in tables:
			if "Portfolio" in table.find("th").getText().strip():
				table = self._convert_htmltable_to_list(table)
				print table
				for row in table:
					if row == []:continue
					membership = self.make_element(memberships,"membership","pm")
					# TO DO: attribututes later
					period = self.make_element(membership,"period","pm")
					date = row[1].split('-')
					fromgranularity = str(len(re.findall("[0-9]",date[0])))
					begin_date = date[0]
					begin_date = begin_date.replace('.',"-").strip()
					begin_date = self.date_granularity(begin_date,fromgranularity)
 					self.set(period,"from",begin_date,"pm")
					self.set(period,"from-granularity",fromgranularity,"pm")
					if len(date) == 1:
						self.set(period,"till","present","pm")
						self.set(period,"till-granularity","8","pm")
					elif len(date) == 2:
						tillgranularity = str(len(re.findall("[0-9]",date[1])))
						end_date = date[1]
						end_date = end_date.replace('.',"-").strip()
						end_date = self.date_granularity(end_date,tillgranularity)
						self.set(period,"till",end_date,"pm")
						self.set(period,"till-granularity",tillgranularity,"pm")
					name = self.make_element(membership,"name","pm")
					name.text = row[0]

	def make_partyaffiliation(self,member):
		"""
		add party-affiliations in a separate node in the openpx namespace

		"""
		partyaffiliations = self.make_element(member,"party-affiliations","openpx")
		table = self.soup.find("table",attrs={"id":re.compile(r".+?(?:_grdCaucus)$")})
		table = self._convert_htmltable_to_list(table)
		for row in table:
			if row == []:continue
			partyaffiliation = self.make_element(partyaffiliations,"party-affiliation","openpx")
			self.set(partyaffiliation,"party-name",row[0],"pm")
			period = self.make_element(partyaffiliation,"period","pm")
			date = row[1].split("-")
			fromgranularity = str(len(re.findall("[0-9]",date[0])))
			date = self.date_granularity(date[0],fromgranularity)
			date = date[0].strip().replace('.',"-")
			if date == "[":
				print row
				break
			#self.set(period,"from",date[0].strip().replace('.',"-"),"pm")
			self.set(period,"from",date,"pm")
			
			self.set(period,"from-granularity",fromgranularity,"pm")
			if len(date) == 2:
				tillgranularity = str(len(re.findall("[0-9]",date[1])))
				date = date[1].strip().replace('.',"-")
				date = self.date_granularity(date,tillgranularity)
				self.set(period,"till",date,"pm")
				self.set(period,"till-granularity",tillgranularity,"pm")
			else:
				if self.deceased == None:
					self.set(period,"till","present","pm")
					self.set(period,"till-granularity","8","pm")
				else:
					tillgranularity = str(len(re.findall("[0-9]",self.deceased)))
					date = self.date_granularity(self.deceased,tillgranularity)
					self.set(period,"till",date,"pm")
					self.set(period,"till-granularity",tillgranularity,"pm")

	def get_end_date(self,d):
		dates = [("2011-05-02","2011-06-02","present"),\
		("2008-10-14","2008-11-18","2011-03-26"),\
		("2006-01-23","2006-04-03","2008-09-07"),\
		("2004-06-28","2004-10-04","2005-11-29"),\
		("2000-11-27","2001-01-29","2004-05-23"),\
		("1997-06-02","1997-09-22","2000-10-22"),\
		("1993-10-25","1994-01-17","1997-04-27"),\
		("1988-11-21","1988-12-12","1993-09-08"),\
		("1984-09-04","1984-11-05","1988-10-01"),\
		("1980-02-18","1980-04-14","1984-07-09"),\
		("1979-05-22","1979-10-09","1979-12-14"),\
		("1974-07-08","1974-09-30","1979-03-26"),\
		("1972-10-30","1973-01-04","1974-05-09"),\
		("1968-06-25","1968-09-12","1972-09-01"),\
		("1965-11-08","1966-01-18","1968-04-23"),\
		("1963-04-08","1963-05-16","1965-09-08"),\
		("1962-06-18","1962-09-27","1963-02-06"),\
		("1958-03-31","1958-05-12","1962-04-19"),\
		("1957-06-10","1957-10-14","1958-02-01"),\
		('1953-08-10','1953-11-12','1957-04-12'),\
		("1949-06-27","1949-09-15","1953-06-13"),\
		("1945-06-11","1945-09-06","1949-04-30"),\
		("1940-03-26","1940-05-16","1945-04-16"),\
		("1935-10-14","1936-02-06","1940-01-25"),\
		("1930-07-28","1930-09-08","1935-08-14"),\
		("1926-09-14","1926-12-09","1930-05-30")]

		for election,start,end in dates:
			if date(d) >= date(election) and date(d) <= date(end):
				return end
		
	def add_member(self):
		"""
		main function for converting a member from HTML to XML
		"""
		member = self.make_element(self.root,"member","pm")
		self.set(member,"id",self.key,"pm")
		self.make_name(member)
		personal = self.make_element(member,"personal","pm")
		self.make_dateofbirth(personal)
		self.make_deceased(personal)
		self.make_gender(personal)
		self.make_occupation(member)
		#try:
		self.make_links(member)
		#except Exception:
		#	print "no links found for %s" % self.title
		memberships = self.make_element(member,"memberships","pm")
		self.make_memberofparliamentrole(memberships)
		self.make_memberofparliamentrole_senate(memberships)
		self.make_roleasparliamentarian(memberships)
		self.make_partyaffiliation(member)



def date(date=None):
	if date == "present":
		date = dt.date(dt.now())
		yyyy,mm,dd = date.year,date.month,date.day
	elif len(date.split("-")) == 3:
		yyyy,mm,dd = date.split("-")
	elif len(date.split("-")) == 2:
		yyyy,mm,dd = date.split("-")[0],"01"
	elif len(date.split("-")) == 1:
		yyyy,mm,dd = date.split("-")[0],"01","01"
	return dt(int(yyyy),int(mm),int(dd))

def main():
	"""
	main function for converting all scraped HTML file to XML
	reads list of links
	scrapes the files
	transforms the files to XML
	"""
	converted = [f[:-4] for f in os.listdir('/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/33rdParliament/33rdParliamentMembers/ca-members')]
	links = codecs.open("/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/33rdParliament/33rdParliamentMembers/links.txt","r",encoding="utf-8").read().split("\n")
	#print len(links)
	source_dir = '/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/33rdParliament/33rdParliamentMembers/Source'
	members = [os.path.join(source_dir,r) for r in os.listdir(source_dir) if not r.startswith('.')]
	for r in links:
		n,i = r.split('\t')
		key = i.split('Item=')[1].split("&")[0]
		if key in converted:
			print "converted %s" % key
			continue
		for m in members:
			if key in m:
		#if key in downloaded:
		#	print "downloaded %s" % key
		#else:	
				print m,i,n
				c = member_file(n,m)
				print "making member file for %s, url=%s" % (c.title,i)
				c.add_docinfo()
				c.add_meta()
				c.add_member()
				c.write_output()
		

def get_female_mps():
	"""
	function for retrieving the list of female MPs
	"""
	output = codecs.open(u'/Users/kasparbeelen/Documents/Dilipad/Parlinfo/womenmp.txt',"w",encoding="utf-8")
	url_commons = "http://www.parl.gc.ca/ParlInfo/Lists/Occupation.aspx?Menu=PARL-HOC&Section=03d93c58-f843-49b3-9653-84275c23f3fb&Parliament=&Name=&Party=&Province=&Gender=F&CurrentParliamentarian=False&Occupation=&OccupationType="
	url_senate = "http://www.parl.gc.ca/ParlInfo/Lists/Occupation.aspx?Menu=PARL-HOC&Section=b571082f-7b2d-4d6a-b30a-b6025a9cbb98&Parliament=&Name=&Party=&Province=&Gender=F&CurrentParliamentarian=False&Occupation=&OccupationType="
	db = [url_commons,url_senate]
	for url in db:
		page = urllib2.urlopen(url)
		soup = bs(page.read())
		links = soup.findAll("a",attrs={"id":re.compile(ur".+?(?:_lnkPerson)")})
		print len (links)
		for l in links:
			member_url = "http://www.parl.gc.ca/ParlInfo"+l["href"][2:]+"&Section=ALL"
			print member_url
			member_page = urllib2.urlopen(member_url)
			member_soup = bs(member_page.read())
			title = member_soup.find('span', attrs={'id':"ctl00_cphContent_lblTitle"}).getText()
			print title
			output.write(u"{}\t{}\n".format(title,member_url))




class postprocessing:
	"""
	performs postprocessing on member files
	1) Adjusting date of entry and date of exit
	2) Adding Wikipedia links
	3) Adding Multilingual Wikipedia links
	"""
	def __init__(self):
		self.location = '/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/33rdParliament/33rdParliamentMembers/ca-members'
		self.files = [f for f in os.listdir(self.location) if not f.startswith('.')]
		self.output_location = '/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/33rdParliament/33rdParliamentMembers/ca-members-postpr'
		self.namespaces = {"pm":"http://www.politicalmashup.nl","pmx":"http://www.politicalmashup.nl/extra"}

	def get_information(self,xpath,element=True):
		items = []
		for doc in self.files:
			doc = os.path.join(self.location,doc)
			with open(doc,"rt") as xml:
				try:
					tree = etree.parse(xml)
					names = tree.xpath(xpath,namespaces={"pm":"http://www.politicalmashup.nl","pmx":"http://www.politicalmashup.nl/extra"})
					if not len(names) == 0:
						items.extend(names)
				except Exception as e:
					print e,doc
		if element == True:
			items = [i.text for i in items]
		items = list(set(items))
		for n in sorted(items): print n

	def add_information(self):
		"""
		add additional information such as links to pary-files and information about body and roles 
		this function needs a bodyandroles files which assigns to each function name the correct body and role attribute
		see bodyandroles.txt
		"""
		information = codecs.open('/Users/kasparbeelen/Documents/Dilipad/Parlinfo/bodyandroles.txt',"r",encoding="utf-8").read().split('\n')[1:]
		partyref = {u"Bloc Québécois":"ca.p.blocquebecois",\
				u"Bloc Québécois (Not in Caucus)":"ca.p.blocquebecois",\
				u"Canadian Alliance":"ca.p.canadianalliance",\
				u"Conservative":"ca.p.conservative",\
				u"Forces et Démocratie":"ca.p.forcesetdemocratie",\
				u"Green Party":"ca.p.greenparty",\
				u"Liberal":"ca.p.liberal",\
				u"New Democratic Party":"ca.p.newdemocraticparty",\
				u"Progressive Conservative":"ca.p.progressiveconservative",\
				u"Reform":"ca.p.reform",\
				u"Social Credit":"ca.p.socialcredit"}


		for doc in self.files:
			location = os.path.join(self.location,doc)

			with open(location,"rt") as xml:
				parser = etree.XMLParser(remove_blank_text=True)
				root = etree.parse(xml,parser)
				memberships = root.xpath('.//pm:membership',namespaces={"pm":"http://www.politicalmashup.nl","pmx":"http://www.politicalmashup.nl/extra"})
				affiliations = root.xpath('.//openpx:party-affiliation',namespaces={"openpx":"https://openparliament.ca/extra"})

				for affiliation in affiliations:
					# set party-ref to affiliation nodes, which is a link to the party file in PM format
					party = affiliation.xpath('@pm:party-name',namespaces={"pm":"http://www.politicalmashup.nl"})
					##if not party == []: 
					if party[0] in partyref.keys():
						affiliation.set("{http://www.politicalmashup.nl}party-ref",partyref[party[0]])
					else:
						affiliation.set("{http://www.politicalmashup.nl}party-ref","not-available")

				for membership in memberships:
					# remove membership without nested period element 
					if  membership.xpath('.//pm:period',namespaces={"pm":"http://www.politicalmashup.nl"}) == []:
						membership.getparent().remove(membership)
						continue
					party = membership.xpath('@pm:party-name',namespaces={"pm":"http://www.politicalmashup.nl","pmx":"http://www.politicalmashup.nl/extra"})
					if not party == []: 
						# set party refs for memberships
						if party[0] in partyref.keys():
							membership.set("{http://www.politicalmashup.nl}party-ref",partyref[party[0]])
						else:
							membership.set("{http://www.politicalmashup.nl}party-ref","not-available")
					else: pass 

					name = membership.xpath(".//pm:name",namespaces={"pm":"http://www.politicalmashup.nl","pmx":"http://www.politicalmashup.nl/extra"})
					if name == []: continue
					name = name[0]
					for line in information:
						function_name,body,role = line.split("\t")
						if function_name == name.text:
							membership.set("{http://www.politicalmashup.nl}body",body)


				title = root.xpath('/root/pm:member[1]/pm:name[1]/pm:titles[1]/pm:title[1]',namespaces={"pm":"http://www.politicalmashup.nl","pmx":"http://www.politicalmashup.nl/extra"})
				titles = root.xpath('/root/pm:member[1]/pm:name[1]/pm:titles[1]',namespaces={"pm":"http://www.politicalmashup.nl","pmx":"http://www.politicalmashup.nl/extra"})[0]

				if title == []:
					titles.getparent().remove(titles)


				links = root.xpath('/root/pm:member[1]/pm:links[1]',namespaces={"pm":"http://www.politicalmashup.nl","pmx":"http://www.politicalmashup.nl/extra"})[0]
				# this part takes care of linking members to Wikipedia
				# I will comment it out for now, it needs a whole bunch of other methods, which we can have a look at later
				external = self.expand_links(links,wiki=True)
				if not external == None:
					for lang,ref in external.items():
						if lang == "wikidata":continue
						ref = "http:"+u"%s"%urllib.unquote_plus(ref).decode("utf-8")
						#print ref,u"wikipedia",u"/".join(ref.split("/")[:-1]),u"Wikipedia (%s)"%lang.upper()
						self.create_link(links,ref,u"wikipedia",u"/".join(ref.split("/")[:-1]),u"Wikipedia (%s)"%lang.upper())
#
				external = self.expand_links(links,wiki=False)
				if not external == None or external == {}:
					for lang,ref in external.items():
						if lang == "wikidata":continue
						#print ref,u"dbpedia",u"/".join(ref.split("/")[:-1]),u"Dbpedia (%s)"%lang.upper()
						self.create_link(links,ref,u"dbpedia",u"/".join(ref.split("/")[:-1]),u"Dbpedia (%s)"%lang.upper())

				member = root.xpath("/root/pm:member[1]",namespaces={"pm":"http://www.politicalmashup.nl","pmx":"http://www.politicalmashup.nl/extra"})[0]
				self.add_biography(member)

				output = codecs.open(os.path.join(self.output_location,doc),"w",encoding="utf-8")
				output.write(etree.tostring(root,method="xml",pretty_print=True,encoding="unicode"))
				output.close()


	def add_biography(self,member):
		"""
		adds a biography for each member using the dbpedia XML file
		"""
		namespaces ={"pm":"http://www.politicalmashup.nl","pmx":"http://www.politicalmashup.nl/extra","rdf":"http://www.w3.org/1999/02/22-rdf-syntax-ns#","owl":"http://www.w3.org/2002/07/owl#"}
		links = member.xpath('pm:links[1]',namespaces=namespaces)[0]
		#print len(links)
		db_dir = "/Users/kasparbeelen/Documents/Dilipad/Parlinfo/dbpediamembers"
		dbLink = links.xpath('pm:link[@pm:linktype="dbpedia"][1]',namespaces=namespaces)
		if not dbLink == []:
			dbLink = dbLink[0].text + ".rdf"
			path = os.path.join(db_dir,urllib.quote(dbLink.split("/")[-1].encode("utf-8")))
			print path
			try:
				tree = etree.parse(path)
			except IOError:
				path = path.replace("(","%28").replace(')',"%29",)
				tree = etree.parse(path)
			if not dbLink.split('/')[2].lower().startswith("dbp"):
				return None
			try:
				biography_text = tree.xpath(".//dbpedia-owl:abstract[@xml:lang='en']",namespaces={"dbpedia-owl":"http://dbpedia.org/ontology/","xml":"http://www.w3.org/XML/1998/namespace"})[0].text
			except Exception as e:
				print "ERROR: no abstract for %s, take rdfs:comment instead" % dbLink
				try:
					biography_text = tree.xpath(".//rdfs:comment[@xml:lang='en']",namespaces={"rdfs":"http://www.w3.org/2000/01/rdf-schema#","xml":"http://www.w3.org/XML/1998/namespace"})[0].text
				except Exception as e:
					print "No biography for %s" % dbLink
					return None
			#print biography_text
			biographies = self.make_element(member,"biographies","pm")
			biography = self.make_element(biographies,"biography","pm")
			dbLink = dbLink.replace('/resource/',"/page/")
			self.set(biography,"source",dbLink[:-4],"pm")
			biography.text = biography_text




	def expand_links(self,links,wiki=""):
		"""
		add dbpedia and wikipedia links
		wiki attribute: if True: search for wikipedia link; otherwise if False search for dbpedia link
		add Multilingual links as well
		"""
		db_dir = "/Users/kasparbeelen/Documents/Dilipad/Parlinfo/dbpediamembers"
		wiki_dir = "/Users/kasparbeelen/Documents/Dilipad/Parlinfo/wikimembers"
		namespaces ={"pm":"http://www.politicalmashup.nl","pmx":"http://www.politicalmashup.nl/extra","rdf":"http://www.w3.org/1999/02/22-rdf-syntax-ns#","owl":"http://www.w3.org/2002/07/owl#"}
		dbLink = links.xpath('pm:link[@pm:linktype="dbpedia"]',namespaces=namespaces)
		wikiLink = links.xpath('pm:link[@pm:linktype="wikipedia"]',namespaces=namespaces)
		if not wikiLink == [] and wiki==True:
			wiki_url = wikiLink[0].text
			wiki_path = os.path.join(wiki_dir,wiki_url.split('/')[-1])
			wiki_file = codecs.open(wiki_path,"r",encoding="utf-8").read()
			wiki_soup = bs(wiki_file)
			li = wiki_soup.findAll("li", class_=re.compile("interlanguage-link"))
			OurDict = {l.a.attrs['lang']:l.a.attrs['href'] for l in li}
			wikiLink[0].text = urllib.unquote_plus(wikiLink[0].text).decode("utf-8")
			return OurDict
		if not dbLink == [] and wiki==False:
			db_url = dbLink[0].text
			db_path = os.path.join(db_dir,db_url.split('/')[-1])+".rdf"
			db_root = etree.parse(db_path)
			li = db_root.xpath("/rdf:RDF/rdf:Description[owl:sameAs]",namespaces=namespaces)
			OurDict = {}
			for l in li:
				url = l.attrib["{http://www.w3.org/1999/02/22-rdf-syntax-ns#}about"]

				lang = url.split("/")[2].split('.')[0]
				if lang == "dbpedia": continue
				OurDict[lang]=url
			#print OurDict
			dbLink[0].text = urllib.unquote_plus(dbLink[0].text).decode("utf-8")
			return OurDict

	def create_link(self,links,url,linktype="",source="",description=""):
		"""
		same function as in member_file class 
		"""
		link = self.make_element(links,"link","pm")
		if not linktype == "": self.set(link,"linktype",linktype,"pm")
		if not source == "": self.set(link,"source",source,"pm")
		if not description == "": self.set(link,"description",description,"pm")
		if "MP profil" in description:
			try:
				opener = urllib2.build_opener(urllib2.HTTPRedirectHandler)
				request = opener.open(url)
				url = request.url
			except Exception:
				pass
		link.text = url

	def make_element(self,parent,name,ns):
		"""
		same function as in member_file class 
		"""
		return etree.SubElement(parent,"{%s}%s"%(self.namespaces[ns],name),nsmap={ns:self.namespaces[ns]})

	def make_element_without_ns(self,parent,name):
		"""
		same function as in member_file class 
		"""
		return etree.SubElement(parent,"%s"%name)

	def set(self,element,name,value,ns):
		"""
		same function as in member_file class 
		"""
		return element.set("{%s}%s"%(self.namespaces[ns],name),value)

	def get_all_roles(self):
		"""
		same function as in member_file class 
		"""
		self.get_information('.//pm:membership/pm:name')

	def get_all_party_names(self):
		"""
		same function as in member_file class 
		"""
		self.get_information('.//pmx:party-affiliation/@pm:party-name',element=False)

	def check_files(self):
		for doc in self.files:
			location = os.path.join(self.location,doc)
			with open(location,"rt") as xml:
				try:
					tree = etree.parse(xml)
				except:
					key = doc[:-4]
					print "problem with %s" % key
					url = "http://www.parl.gc.ca/parlinfo/Files/Parliamentarian.aspx?Item="+key+"&Language=E&Section=ALL"
					c = member_file(url)
					c.add_meta()
					c.add_member()
					c.write_output()
					time.sleep(1)


# Following function are used to prepare the source material for linking the wikipedia and dbpedia
# This shouldn't be redone (theoretically)
def link_pm_to_dbpdia():

	members = []
	namespaces = {"rdf":"http://www.w3.org/1999/02/22-rdf-syntax-ns#","dcterms":"http://purl.org/dc/terms/"}
	dir = "/Users/kasparbeelen/Documents/Dilipad/Parlinfo/dbpedia"
	files = [f for f in os.listdir(dir) if not f.startswith('.')]
	for c in files:
		tree = etree.parse(os.path.join(dir,c))
		descriptions = tree.xpath('.//rdf:Description',namespaces=namespaces)
		for description in descriptions:
			subject = description.xpath(".//dcterms:subject",namespaces=namespaces)
			if subject != []:
				members.append(description.xpath("@rdf:about",namespaces=namespaces)[0])
	members = list(set(members))
	dbpedia_members_dir = "/Users/kasparbeelen/Documents/Dilipad/Parlinfo/dbpediamembers"

	print len(members)
	dowloaded = [o for o in os.listdir(dbpedia_members_dir) if not o.startswith('.')]
	for link in members:
		if link.split("/")[-1]+".rdf" in dowloaded: 
			print "downloaded %s" % link
			continue
		link = link.replace('/resource/',"/data/")
		print "downloading %s.rdf" % link
		response = requests.get(link+".rdf")
		with codecs.open(os.path.join(dbpedia_members_dir,link.split("/")[-1]+".rdf"),"w",encoding="utf-8") as rdf:
			rdf.write(response.text)
			time.sleep(1)
	#print members

def link_dbpedia_wiki():
	dir = "/Users/kasparbeelen/Documents/Dilipad/Parlinfo/dbpediamembers"
	output_dir = "/Users/kasparbeelen/Documents/Dilipad/Parlinfo/wikimembers"
	output_file = codecs.open('/Users/kasparbeelen/Documents/Dilipad/Parlinfo/dbwikipi_links.txt',"w",encoding="utf8")
	namespaces = {"rdf":"http://www.w3.org/1999/02/22-rdf-syntax-ns#","dcterms":"http://purl.org/dc/terms/","foaf":"http://xmlns.com/foaf/0.1/"}
	downloaded = os.listdir(output_dir)
	files = [f for f in os.listdir(dir) if not f.startswith('.')]
	for c in files:
		#print c
		tree = etree.parse(os.path.join(dir,c))
		description = tree.xpath('.//rdf:Description[foaf:primaryTopic]',namespaces=namespaces)
		if description == []:continue
		else: description = description[0]
		wiki_url = description.xpath("@rdf:about",namespaces=namespaces)[0]
		wiki_url = fixurl(wiki_url)
		wiki_path = os.path.join(output_dir,wiki_url.split('/')[-1])
		print wiki_path
		#if wiki_url.split("/")[-1] in downloaded:
		#	print "downloaded %s" % wiki_url.split("/")[-1]
		#	continue
		try:
			with codecs.open(wiki_path,"r",encoding="utf-8") as w:
			#with codecs.open(wiki_path,"w",encoding="utf-8") as w:
				#w.write(urllib2.urlopen(wiki_url).read().decode("utf-8"))
				pi_key = get_parlinfo_key(wiki_path.split("/")[-1])
				output_file.write(u"{}\t{}\t{}\n".format(pi_key,"http://dbpedia.org/resource/"+c[:-4],wiki_url))
		except Exception as e:
			print "ERROR %s with file %s" % (e,wiki_url)
		#time.sleep(1)

def write_wiki():
	dir = "/Users/kasparbeelen/Documents/Dilipad/Parlinfo/dbpediamembers"
	output_dir = "/Users/kasparbeelen/Documents/Dilipad/Parlinfo/wikimembers"
	output_file = codecs.open('/Users/kasparbeelen/Documents/Dilipad/Parlinfo/dbwikipi_links.txt',"w",encoding="utf8")
	namespaces = {"rdf":"http://www.w3.org/1999/02/22-rdf-syntax-ns#","dcterms":"http://purl.org/dc/terms/","foaf":"http://xmlns.com/foaf/0.1/"}
	downloaded = os.listdir(output_dir)
	files = [f for f in os.listdir(dir) if not f.startswith('.')]
	for c in files:
		#print c
		tree = etree.parse(os.path.join(dir,c))
		description = tree.xpath('.//rdf:Description[foaf:primaryTopic]',namespaces=namespaces)
		if description == []:continue
		else: description = description[0]
		wiki_url = description.xpath("@rdf:about",namespaces=namespaces)[0]
		wiki_url = fixurl(wiki_url)
		wiki_path = os.path.join(output_dir,c[:-4])
		print wiki_path
		if wiki_url.split("/")[-1] in downloaded:
			print "downloaded %s" % wiki_url.split("/")[-1]
			continue
		#with codecs.open(wiki_path,"r",encoding="utf-8") as w:
		with codecs.open(wiki_path,"w",encoding="utf-8") as w:
			print "downloading %s" % wiki_url
			w.write(urllib2.urlopen(wiki_url).read().decode("utf-8"))
			#pi_key = get_parlinfo_key(wiki_path.split("/")[-1])
			#output_file.write(u"{}\t{}\t{}\n".format(pi_key,"http://dbpedia.org/resource/"+c[:-4],wiki_url))
		#time.sleep(1)

def get_parlinfo_key(wiki_name):
	linktexts = ["Parliament of Canada","Parliamentarian File","Biography of","Parliamentary biography","Federal Political Experience","Parliamentary experience","PARLINFO"]
	wikidir = "/Users/kasparbeelen/Documents/Dilipad/Parlinfo/wikimembers"
	f = bs(codecs.open(os.path.join(wikidir,wiki_name),"r",encoding="utf8").read())
	pi_link = f.findAll("a",attrs={"href":re.compile(ur".+?(?:parl\.gc\.ca\/parlinfo\/.iles\/.arliamentarian\.aspx).+$",re.IGNORECASE)})
	
	if pi_link == []:
		return "na"

	else:
		if len(pi_link) == 1:
			pi_link = pi_link[0]
		else:
			Linked = False
			for pi in pi_link:
				for r in linktexts:
					if r in pi.getText():
						pi_link = pi
						Linked = True
						break
			if Linked == False:
				pi_link = pi_link[-1]

		key = ""
		try:
			key = pi_link["href"].split('Item=')[1].split('&Language')[0]
		except IndexError:
			try:
				key = pi_link["href"].split('Item=')[1].split('&language')[0]
			except IndexError:
				try:
					key = pi_link["href"].split('Item=')[1].split('&')[0]
				except IndexError:
					key = pi_link["href"]

		print key
		return key


def finalize_memberships():
	namespaces = namespaces={"pm":"http://www.politicalmashup.nl","openpx":"https://openparliament.ca/extra"}
	dir = "/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/33rdParliament/33rdParliamentMembers/ca-members-postpr"
	o_dir =  "/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/33rdParliament/33rdParliamentMembers/ca-members-final"
	files = [os.path.join(dir,f) for f in os.listdir(dir) if f.endswith('.xml')]
	for f in files:
		print f
		parser = etree.XMLParser(remove_blank_text=True)
		tree = etree.parse(f,parser)
		memberships = tree.xpath(".//pm:memberships",namespaces=namespaces)[0]
		memberships_commons = tree.xpath(".//pm:membership[@pm:party-ref]",namespaces=namespaces)
		for mc in memberships_commons:
			mc.attrib.pop("{http://www.politicalmashup.nl}party-ref")
			mc.attrib.pop("{http://www.politicalmashup.nl}party-name")
		partyaffiliations = tree.xpath(".//openpx:party-affiliation",namespaces=namespaces)
		for pa in partyaffiliations:
			pa_new = pa.__copy__()
			pa_new.tag = "{http://www.politicalmashup.nl}membership"
			#name = etree.SubElement(pa_new,"{http://www.politicalmashup.nl}name")
			#name.text = pa_new.attrib.get('{http://www.politicalmashup.nl}party-name')
			pa_new.set("{http://www.politicalmashup.nl}body","other")
			memberships.append(pa_new)
		pa_parent = tree.xpath(".//openpx:party-affiliations",namespaces=namespaces)[0]
		pa_parent.getparent().remove(pa_parent)
		output = codecs.open(os.path.join(o_dir,f.split("/")[-1]),"w",encoding="utf-8")
		output.write(etree.tostring(tree,method="xml",encoding="unicode",pretty_print=True))


def fixurl(url):
    # turn string into unicode
    if not isinstance(url,unicode):
        url = url.decode('utf8')

    # parse it
    parsed = urlparse.urlsplit(url)

    # divide the netloc further
    userpass,at,hostport = parsed.netloc.rpartition('@')
    user,colon1,pass_ = userpass.partition(':')
    host,colon2,port = hostport.partition(':')

    # encode each component
    scheme = parsed.scheme.encode('utf8')
    user = urllib.quote(user.encode('utf8'))
    colon1 = colon1.encode('utf8')
    pass_ = urllib.quote(pass_.encode('utf8'))
    at = at.encode('utf8')
    host = host.encode('idna')
    colon2 = colon2.encode('utf8')
    port = port.encode('utf8')
    path = '/'.join(  # could be encoded slashes!
        urllib.quote(urllib.unquote(pce).encode('utf8'),'')
        for pce in parsed.path.split('/')
    )
    query = urllib.quote(urllib.unquote(parsed.query).encode('utf8'),'=&?/')
    fragment = urllib.quote(urllib.unquote(parsed.fragment).encode('utf8'))

    # put it back together
    netloc = ''.join((user,colon1,pass_,at,host,colon2,port))
    return urlparse.urlunsplit((scheme,netloc,path,query,fragment))

#class postprocessing_final:
#	def __init__(self):
#		pass


class validator:
	def __init__(self,directory='/Users/kasparbeelen/Documents/Dilipad/Parlinfo/ca-members-final'):
		self.dir = directory
		jing_dir = "/Users/kasparbeelen/Downloads/jing-20091111"
		os.chdir(jing_dir)
		self.files = [os.path.join(self.dir,f) for f in os.listdir(self.dir) if not f.startswith('.')]
		self.log = codecs.open('/Users/kasparbeelen/Documents/Dilipad/Parlinfo/logFile.txt','w',encoding='utf-8')
		self.schemaLocation = "/Users/kasparbeelen/Documents/Dilipad/OpenParliament/Schemas/rnc/memberX.rnc"
	
	def validate_all(self):
		for f in self.files:
			time.sleep(3)
			#print "validating %s" % f
			#call = "java -jar bin/jing.jar -c -t %s %s" % (self.schemaLocation,f)
			call = ["java","-jar","bin/jing.jar","-c",self.schemaLocation,f]
			p = subprocess.Popen(call,stderr=self.log) 
        	p.communicate()


        	
if __name__=="__main__":
	#link_pm_to_dbpdia()
	#write_wiki()
	#link_dbpedia_wiki()
	#temp()

	#get_female_mps()
	c= parlinfo_scraper()
	c.get_links()
	c.scrape()
	main()
	#
	postprocessing().add_information()
	#c=linker()
	#c.make_table()
	#c.link()
	#postprocessing().get_all_party_names()
	#temp2()
	#validator().validate_all()
	#temp3()
	#temp4()
	finalize_memberships()
	


