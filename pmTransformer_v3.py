#/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import codecs,os,re,math
from operator import itemgetter
from lxml import etree as Etree
from xml.sax.saxutils import escape
import traceback
import sys
from datetime import datetime as dt
from datetime import timedelta
from collections import Counter
from operator import itemgetter


#******************************
#****** date function *********
#******************************

def date(date=None):
    if date == "present":
        date = dt.date(dt.now())
        yyyy,mm,dd = date.year,date.month,date.day
    elif len(date.split("-")) == 3:
        yyyy,mm,dd = date.split("-")
    else:
        yyyy,mm,dd = date.split("-")[0],"01","01"
    return dt(int(yyyy),int(mm),int(dd))


#******************************
#******** transform ***********
#******************************

class Transformer(object):
    def __init__(self,f,directory,output_directory):
        self.text = re.compile(ur"’").sub("'",escape(codecs.open(os.path.join(directory,f),'r',encoding="utf-8").read()))
        #self.text = re.compile(ur'[a-z](\-\n)[a-z]',re.UNICODE|re.MULTILINE).sub("",self.text)
        #self.text = re.compile(ur'(\n)moved',re.UNICODE|re.MULTILINE).sub("",self.text).split("\n")
        self.output = codecs.open(os.path.join(output_directory,f[:-4]+".xml"),"w",encoding="utf-8")
        
        #self.topic_pattern = ur"^([^a-z0-9\(*\.\t]{2,})$"
        #self.topic_pattern = ur"^([^a-z\(*\t]{3,})$"
        #self.topic_pattern = ur"^([^a-z*\t•\^\■\}\{\}]{4,})$"
        #self.topic_pattern = ur"(?:(^[0-9\-]{4,9}\sREVIEW)$)|(?:(^[A-Z]{4,})$)|(?:^([^a-z*\t•\^\■\}\{\}]{2}[A-Z\s]{2,}[^a-z*\t•\^\■\}\{\}\t]+)$)"
        self.topic_pattern = ur"((?:^[0-9\-]{4,9}\sREVIEW$)|(?:\([A-Z]{1,4}\)[A-Z\s\–\—]+$)|(?:^[A-Z]{4,}$)|(?:^[^a-z*\t•\^\■\}\{\}]{2}[A-Z\–\.\s]{2,}[^a-z*\t•\^\■\}\{\}\t]+$))"
        #self.mp_speaker_pattern = ur"^((?:Sir|M\.|Mr\.|Mr\,|Hon\.|Right\sHon\.|Miss|Mrs\.|Ms\.)\s[A-Z][\s\-\w\.\']{1,100}\s{0,1}(?:\(.+?\)){0,1}\s{0,1}(?:(?:moved)|:|;))"
        #self.mp_speaker_pattern = ur"^((?:Sir|M\.|Mr\.|Mr\,|Hon\.|The\sHon\.|Right\sHon\.|The\sRight\sHon\.|Miss|Mrs\.|Ms\.)\s(?:[A-Zdv][\-\w\.\']{1,25}\s){1,4}\s{0,1}(?:\(.+?\)){0,1}\s{0,1}(?:(?:moved)|:|;))"
        self.mp_speaker_pattern = ur"^((?:Sir|M\.|Mr\.|Mr\,|Hon\.|The\sHon\.|Right\sHon\.|The\sRight\sHon\.|Miss|Mrs\.|Ms\.)\s(?:[A-Zdv][\-\w\.\']{1,25}\s{0,1}){1,4}\s{0,1}(?:\(.+?\)){0,1}\s{0,1}(?:(?:moved:)|(?:moved)|:|;))"
        
        #self.other_speaker_pattern =ur'^(The(?:\sActing)?(?:\sAssistant)?(?:\sDeputy)?\s(?:Speaker)?(?:Chairman)?(?:\s\(.{0,50}?\)){0,1}?(?:;|:))'
        self.other_speaker_pattern =ur'^((?:The|Madam)(?:\sActing)?(?:\sAssistant)?(?:\sDeputy)?\s(?:Speaker)?(?:Chairman)?(?:\s\(.{0,50}?\)){0,1}?(?:;|:))'
        
        self.other_elements = [u"The Assistant Deputy Chairman:",
                               u"The Assistant Chairman:",
                               u"The Deputy Chairman:",
                               u"The Chairman:",
                               u"The Speaker:",
                               u"The Deputy Speaker:",
                               u"Madam Deputy Speaker:",
                               u"Madam Speaker:",]
        
        self.main_headers = [u"Government Orders",
                             u"Statements by Members",
                             u"Oral Questions",
                             u"Routine Proceedings",
                             u"Oral Question Period",
                             u"Private Members' Business",
                             u"Adjournment Proceedings",
                             u"Statements by Members",
                             u"Statements Pursuant to",
                             u"Sitting Resumed"]
        
        self.subtopics = [u"COMMITTEES OF THE HOUSE",
                          u"POINTS OF ORDER",
                          u"POINT OF ORDER",
                          u"WAYS AND MEANS",
                          u"PETITIONS",
                          u"QUESTIONS PASSED AS ORDERS FOR RETURNS",
                          u"QUESTIONS ON THE ORDER PAPER",
                          u"GOVERNMENT RESPONSE TO PETITIONS",
                          u"SUPPLEMENTARY ESTIMATES",
                          u"QUESTIONS ON THE ORDER PAPER",
                          u"PRESENCE IN GALLERY",
                          u"BUSINESS OF THE HOUSE",
                          u"PRIVILEGE",
                          u"GOVERNMENT RESPONSE TO PETITIONS",
                          u"MOTIONS FOR PAPERS",
                          u"THE BUDGET",
                          u"ORDER IN COUNCIL APPOINTMENTS",
                          u"SUPPLY",
                          u"INTERIM SUPPLY",
                          u"INTERPARLIAMENTARY DELEGATIONS"]
        
        self.primary_stage_directions = [u"* * *",
                                         u"(Motion",
                                         u"Prayers",
                                         u"Call in the members.",
                                         u"(Division",
                                         u"{Division", 
                                         u"(The House",
                                         u"{The House",
                                         u"YEAS",
                                         u"'YEAS",
                                         u"HOUSE OF COMMONS",
                                         u"PAIRED—MEMBERS",
                                         u"PAIRED MEMBERS",
                                         u"NAYS",
                                         u"SUSPENSION OF SITTING",
                                         #u"SITTING RESUMED",
                                         u"AFTER RECESS",
                                         u"The House met at",
                                         u"The House adjourned at",
                                         ]

        self.interventions_pattern = ur"(?:some|a) hon. member.{0,1}:"
        self.interventions = [u"Some hon. members:",
                                         u'Some hon. Members:', 
                                         u"Some Hon. members:",
                                         u"Some Hon. Members:",
                                         u"A hon. member:",
                                         u"A Hon. member:",
                                         u"A Hon. Member:",
                                         u"An Hon. Member:"]

        self.votes = [u"(Division",u"(division"]

    def valid_xml_char_ordinal(self,c):
            codepoint = ord(c)
            # conditions ordered by presumed frequency
            return (
                0x20 <= codepoint <= 0xD7FF or
                codepoint in (0x9, 0xA, 0xD) or
                0xE000 <= codepoint <= 0xFFFD or
                0x10000 <= codepoint <= 0x10FFFF)

    def preprocess_text_for_markup(self):
        # delete elements

        for i in [u" YEAS",u" NAYS"]:
          self.text = re.compile(i,re.UNICODE|re.MULTILINE).sub(u"\n{}".format(i.strip()),self.text)

        for i in [ur"\s\(Division"]:
          self.text = re.compile(i,re.UNICODE|re.MULTILINE).sub(ur"\n{}".format(i.strip()[3:]),self.text)

        self.text = re.compile(ur'\-\n(?=[a-z])',re.UNICODE|re.MULTILINE).sub(" ",self.text)
        self.text = re.compile(ur'\n(?=moved)',re.UNICODE|re.MULTILINE).sub(" ",self.text)
        self.text = re.compile(ur'\n.Division',re.UNICODE|re.MULTILINE).sub("\n(Division",self.text)
        pattern_so = re.compile(ur"\n[A-Z0-9]{1}\'{0,1}\.{0,1}\s{0,1}O\.{0,1}\s{0,1}[0-9]{1,4}\n",re.M|re.UNICODE)
        self.text = re.sub(pattern_so,"\n",self.text)

        pattern_pages = re.compile(ur"\n[0-9\—\-]{1,10}\n",re.M|re.UNICODE)
        self.text = re.sub(pattern_pages,"\n",self.text)

        pattern_stars = re.compile(ur"\n\*\s\*\s\*\n",re.M|re.UNICODE)
        self.text = re.sub(pattern_stars,"\n",self.text)
        

        pattern_line_break = re.compile(ur"[\—\-]\n(?=[\sA-Za-z])",re.MULTILINE|re.UNICODE)
        #print pattern_line_break.findall(self.text)
        self.text = re.sub(pattern_line_break,u"— ",self.text)
        #pattern_col = re.compile(ur'\n•\s{0,1}.\s{0,1}[A-Z0-9]{1,5}\s{0,1}.\n',re.M|re.UNICODE)
        pattern_col = re.compile(ur'\n•.{3,8}\n',re.M|re.UNICODE)
        self.text = re.sub(pattern_col,"\n",self.text)

        for i in self.main_headers:
            i = ur"\n{}\n".format(i)
            pattern = re.compile(i,re.M|re.UNICODE)
            self.text = re.sub(pattern,"\n",self.text)
        # !!find a better solutions for this later !!`
        self.text = self.text.replace(u"\nHOUSE OF COMMONS\n","\n")
        self.text = self.text.replace(u"MEASURE TO ENACT",u"Measure to Enact")
        self.text = self.text.replace(u"MEASURE TO AMEND",u"Measure to Amend")
        self.text = self.text.replace(u"Mr.\nSpeaker",u"Mr. Speaker")
        self.text = self.text.replace(u'♦',"")
        self.text =self.text.split("\n")

    def _markup_entity_by_pattern(self,element_name='',pattern=None,print_output=False,ignore_capitals=False):
        for i,line in enumerate(self.text):
            #results = re.compile(pattern,re.UNICODE|re.DOTALL|re.M).findall(line.strip())
            if ignore_capitals == True:
              results = re.compile(pattern,re.UNICODE|re.DOTALL|re.M|re.IGNORECASE).findall(line.strip())
              results = set(results)
            else:
              results = re.compile(pattern,re.UNICODE|re.DOTALL|re.M).findall(line.strip())
            for r in results:
                self.text[i] = line.replace(r,u"<{0}>{1}</{0}>".format(element_name,r))
                if print_output==True:
                    print r
                
    def _markup_entity_by_list(self,element_name="",elements_list=[],upper=False,whole_line=False):
        for i,line in enumerate(self.text):
            for element in elements_list:
                if upper == True:
                    element = element.upper()
                else:
                    pass
                if self.text[i].strip().startswith(element):
                    if whole_line == True:
                        self.text[i] = line.replace(self.text[i],u"<{0}>{1}</{0}>".format(element_name,self.text[i]))
                    else:
                        self.text[i] = line.replace(element,u"<{0}>{1}</{0}>".format(element_name,element))
    
    def markup_entities(self,exclude=""):
        self._markup_entity_by_list('vote_heading',self.votes,whole_line=True)
        self._markup_entity_by_pattern('speaker_entity',self.interventions_pattern,ignore_capitals=True)
        self._markup_entity_by_pattern("speaker_entity",self.mp_speaker_pattern,print_output=False)
        #self._markup_entity_by_pattern("speaker_entity",self.mp_speaker_pattern,print_output=False)
        self._markup_entity_by_pattern("speaker_entity",self.other_speaker_pattern)
        self._markup_entity_by_list('stage-direction',self.primary_stage_directions,whole_line=True)
        self._markup_entity_by_list('main_topic_title',self.main_headers,upper=True,whole_line=True)
        
        #self._markup_entity_by_list('intermediate_topic_title',self.subtopics)
        
        self._markup_entity_by_pattern('topic_title',self.topic_pattern,print_output=True)
         
    
    def _markup_all_lines(self):
        
        self.text = re.compile(ur'><',re.UNICODE|re.DOTALL).sub(ur'>\n<',self.text)
        self.text = self.text.split("\n")
        
        in_speech = False
        
        for i,line in enumerate(self.text):
            line = line.strip()
            # traverse document differentatiating between <speech> and <stage-direction>
            if line.startswith("<speech>"):
                in_speech = True
            elif line.endswith("</speech>"):
                in_speech = False

            if in_speech == True and not line.startswith(ur'<') and not line.endswith(ur">"):
                self.text[i] = line.replace(line,ur"<p>{}</p>".format(line.strip()))
                continue
            elif in_speech == True and not line.startswith('<') and line.endswith(ur"</speech>"):
                sent = line.split(ur'</speech>')[0]
                sent = ur"<p>{}</p>{}".format(sent,ur"</speech>")
                self.text[i] = sent
                continue
            elif in_speech == True and '</speaker_entity>' in line:
                line = line.split("</speaker_entity>")
                p = line[1].replace(line[1],ur"<p>{}</p>".format(line[1]))
                self.text[i] = "</speaker_entity>".join([line[0],p])
                continue
            elif in_speech == False and  not line.startswith(ur'<') and not line.endswith(ur">"):
                self.text[i] = line.replace(line,ur"<stage-direction><p>{}</p></stage-direction>".format(line.strip()))
                continue
            elif in_speech == False and not line.startswith('<') and line.endswith("</scene>"):
                sent = line.split(ur'</scene>')[0]
                sent = ur"<stage-direction><p>{}</p></stage-direction>{}".format(sent,ur"</scene>")
                self.text[i] = sent
                continue

        
        self.text = "\n".join(self.text)
    
    def set_first_topic_to_main(self):
        before_chapter = True
        topic_changed = False

        for i,line in enumerate(self.text):
        # be sure that first topic encountered is equaly main_topic_title
            if ur"<topic_title>" in line and before_chapter == True and topic_changed == False:
                self.text[i] = line.replace(ur"<topic_title>",ur"<main_topic_title>").replace(ur"</topic_title>",ur"</main_topic_title>")
                topic_changed = True
            #elif ur"<intermediate_topic_title>" in line and before_chapter == True and topic_changed == False:
            #    self.text[i] = line.replace(ur"<intermediate_topic_title>",ur"<main_topic_title>").replace(ur"</intermediate_topic_title>",ur"</main_topic_title>")
            #    topic_changed = True
            elif '<main_topic_title>' in line:
                before_chapter = False
        return self.text

    def _markup_element_by_pattern(self,pattern,element_name,strip_text=False):
        elements = re.compile(pattern,re.UNICODE|re.DOTALL).findall(self.text)
        for el in list(set(elements)):
          if strip_text == True:
            self.text = self.text.replace(el,u"<{0}>{1}</{0}>".format(element_name,el.strip()))
          else:
            self.text = self.text.replace(el,u"<{0}>{1}</{0}>".format(element_name,el))
    

    def markup_elements(self):
        self.text = self.set_first_topic_to_main()
        self.text = u"\n".join(self.text)
        #self._markup_element_by_pattern(ur"<intervention_element>.+?(?:(?=<speaker_entity>|<stage-direction>|<topic_title>|<main_topic_title>|<intermediate_topic_title>|<intervention_element>|<vote_heading>|<stage-direction>|$))","intervention",strip_text=True)
        self._markup_element_by_pattern(ur"<vote_heading>.+?(?:(?=<speaker_entity>|<topic_title>|<main_topic_title>|<intermediate_topic_title>|<intervention_element>|$))","vote_element",strip_text=True)
        self._markup_element_by_pattern(ur"<speaker_entity>.+?(?:(?=<speaker_entity>|<stage-direction>|<topic_title>|<main_topic_title>|<intermediate_topic_title>|<vote_element>|$))","speech")
        self._markup_element_by_pattern(ur"<main_topic_title>.+?(?:(?=<main_topic_title>|$))","topic")
        
        #self._markup_element_by_pattern(ur"<vote_element>.+?(?:(?=<intermediate_topic_title>|<topic>|</topic>|$))","subtopic")
        #self._markup_element_by_pattern(ur"<topic_title>[^<>]+?(?:</topic_title>)[^<>]+(?:<speech>).+?(?:(?=<topic_title>|<subtopic>|</topic>|</subtopic>|</topic>))","scene")
        
        self._markup_all_lines()

        self.text = self.text.replace(u'>\n<',"><")
        self._markup_element_by_pattern(ur"<topic_title>[^<>]+?(?:</topic_title>)[^<>]*?(?:(?:<speech>)|(?:<stage-direction>)).+?(?:(?=<topic_title>|<subtopic>|</topic>|</subtopic>|</topic>|$))","scene")
        
        self.output.write(u"<proceedings>{}</proceedings>".format(self.text))
        self.output.close()



#******************************
#***** transform to xml *******
#******************************

class xmlTransformer(Transformer):
    def __init__(self,f,directory,output_directory):
        parser = Etree.XMLParser(remove_blank_text=True)
        self.tree = Etree.parse(os.path.join(directory,f),parser)
        #self.mp_ns= "http://www.politicalmashup.nl"
        #self.root = Etree.Element('{%s}proceedings' % (self.mp_ns), nsmap={None: self.mp_ns})
        #self.root = Etree.Element("proceedings",nsmap=self.nsmap)
        self.output = codecs.open(os.path.join(output_directory,f),"w",encoding="utf-8")
        self.titles = [u"Mr.",u"Miss",u"Hon.",u"The Hon.",u"The hon.",u"Mrs.",u"The Right Hon.",u"The right Hon.",u"Right Hon.","Ms."]

    
    def valid_xml_char_ordinal(self,string):
        return super(xmlTransformer,self).valid_xml_char_ordinal(string)
    
    def clean_string(self,string):
        cleaned = u"".join(c for c in string if self.valid_xml_char_ordinal(c))
        return cleaned

    def _change_element_name(self,element="",new_name="",delete_tags=False,element_to_delete=""):
        query = ".//{}".format(element)
        elements = self.tree.xpath(query)
        for el in elements:
          if delete_tags == True:
            Etree.strip_tags(el,element_to_delete)
            p = Etree.SubElement(el,"p")
            p.text = el.text
            el.text = ""
          el.tag = new_name
    
    def _make_attributes(self,element_type="",element_selected="",attribute_name="",normalisation=""):
        query_1 = ".//{}".format(element_type)
        query_2 = ".//{}".format(element_selected)
        elements = self.tree.xpath(query_1)
        for el in elements:
            try:
                el_sel = el.xpath(query_2)[0]
                el_sel.text = self.normalize_text(el_sel.text,normalisation)
                el.get(attribute_name)
                el.set(attribute_name,el_sel.text)
                el.remove(el_sel)
            except Exception,err:
                pass
                #print traceback.format_exc()

    def _make_attribute(self,element_type="",attribute_name="",tag_name=""):
        query_1 = ".//{}".format(element_type)
        elements = self.tree.xpath(query_1)
        for el in elements:
            try:
                el_new = Etree.Element(tag_name)
                el_new.set(attribute_name,el.text.strip())
                el.getparent().replace(el,el_new)
                el.getparent().remove(el)
            except Exception  as err:
              #print err
              pass
              #print traceback.format_exc()


    # copy paste later in other class
    def normalize_text(self,text,normalisation):
        if normalisation == "name":
            if "Acting" in text: return text[:-1].strip()
            if "(" in text:
                text = text.split("(")[0]
            text = text.replace(':',"").replace("moved","")
            for t in self.titles:
                if text.startswith(t):
                    text = text.replace(t,"")
        return " ".join((text.strip().split()))
            
    def add_metadata(self):
        #self._make_attributes("speech","speaker_entity","speaker",normalisation="name")
        self._make_attributes("speech","speaker_entity","speaker")
        self._make_attributes("scene","topic_title","title")
        self._make_attributes("subtopic","intermediate_topic_title","title")
        self._make_attributes("topic","main_topic_title","title")
        self._make_attribute("topic_title","title","scene")
        self._make_attribute("vote_heading","title","vote")
        #self._make_attribute("vote_element","vote_heading","title")

    def modify_topic_hierarchy(self):
      topics = self.tree.xpath('.//topic')
      for topic in topics:
        scenes = topic.xpath('.//scene')
        for i,scene in enumerate(scenes):
          #print scene.getchildren()
          if scene.getchildren() == []:
            try:
              scene.append(scenes[i+1])
            except Exception as e:
              print "Error %s for next scene of %s" %(e,scene.get('title'))
              scene.getparent().remove(scene)

    def delete_empty_paragrahps(self,tagname): # TO DO MISTAKE HERE
      elements = self.tree.xpath('.//{}'.format(tagname))
      for element in elements:
        if element.text == None:
          element.getparent().remove(element)

    #def change_namespace(self):
    #  namespaces = {"pm":"http://www.politicalmashup.nl",\
    #        "dc":"http://purl.org/dc/elements/1.1/",\
    #        "xsd":"http://www.w3.org/2001/XMLSchema-datatypes",\
    #        "owl":"http://www.w3.org/2002/07/owl#",\
    #        "pmd":"http://www.politicalmashup.nl/docinfo",\
    #        "rdf":"http://www.w3.org/1999/02/22-rdf-syntax-ns#",\
    #        "html":"http://www.w3.org/1999/xhtml",\
    #        "dcterms":"http://purl.org/dc/terms/",\
    #        "xsi":"http://www.w3.org/2001/XMLSchema-instance",\
    #        "pmx":"http://www.politicalmashup.nl/extra",\
    #        "openpx":"https://openparliament.ca/extra"}
    #  root = Etree.Element("root",nsmap=namespaces)
    #  proceedings = self.tree.xpath('/proceedings')
    #  attributes = self.tree.xpath("//*")
    #  print len(elements)
    #  for e in elements:
    #    print e.tag
    #    e.tag = Etree.QName("http://www.politicalmashup.nl",e.tag)
    #    print e.tag
      #for a in attributes:
      #  a.tag = Etree.QName("http://www.politicalmashup.nl",a.tag)

      #root.append(self.tree)
      #return root


    def transform(self):
      self.add_metadata()
      self._change_element_name("intervention","stage-direction",delete_tags=True,element_to_delete="intervention_element")
      self.delete_empty_paragrahps("p")
      #self.delete_empty_paragrahps('stage-direction')
      self._change_element_name('subtopic',"scene")
      self.modify_topic_hierarchy()
      #self.tree = self.change_namespace()
      #self.change_namespace()
      self.output.write(Etree.tostring(self.tree, pretty_print=True,method="xml",encoding="unicode"))


#******************************
#***** pm compliant xml *******
#******************************     


class PMTransformer(Transformer):
    def __init__(self,f,directory,output_directory):
        parser = Etree.XMLParser(remove_blank_text=True)
        self.tree = Etree.parse(os.path.join(directory,f),parser)
        #self.root_proceedings = Etree.Element("proceedings")
        self.output = codecs.open(os.path.join(output_directory,f),"w",encoding="utf-8")
        self.f = f
        self.namespaces = {"pm":"http://www.politicalmashup.nl",\
            "dc":"http://purl.org/dc/elements/1.1/",\
            "xsd":"http://www.w3.org/2001/XMLSchema-datatypes",\
            "owl":"http://www.w3.org/2002/07/owl#",\
            "pmd":"http://www.politicalmashup.nl/docinfo",\
            "rdf":"http://www.w3.org/1999/02/22-rdf-syntax-ns#",\
            "html":"http://www.w3.org/1999/xhtml",\
            "dcterms":"http://purl.org/dc/terms/",\
            "xsi":"http://www.w3.org/2001/XMLSchema-instance",\
            "pmx":"http://www.politicalmashup.nl/extra",\
            "openpx":"https://openparliament.ca/extra",\
            "dp":"http://dilipad.history.ac.uk"}
        self.root = Etree.Element("root",nsmap=self.namespaces)
        
    def transform(self):
      self.add_docinfo()
      self.add_meta()
      self.filter_outside_topic()
      self.rest_functions()
      self.arrange_topic_hierarchy()
      self.add_required_attributes()
      self.arrange_votes()
      self.add_ids()
      self.set_namespace()
      self.output.write(Etree.tostring(self.root, pretty_print=True,method="xml",encoding="unicode"))

    def add_ids(self):
      dateid = str(convert_date_filename(self.f)).split()[0]
      print dateid
      start = self.tree.xpath("/proceedings")[0]
      start.set("{http://www.politicalmashup.nl}id","ca.proc.d.%s"%dateid)
      self.recursive_ids(start,"ca.proc.d.%s"%dateid)

    def rest_functions(self):
      stagedirections = self.tree.xpath('.//stage-direction')
      for st in stagedirections:
        try:
          if not len(st.text) == 0:
            #print st.text
            p = Etree.SubElement(st,"p")
            p.text = st.text
            st.text = None
        except:
          pass

      #speeches = stagedirections = self.tree.xpath('.//speech')
      #for speech in speeches:
      #  subspeeches = speech.xpath(".//speech")
      #  if len(subspeeches) > 0:
      #    parent = speech.getparent()
      #    position = parent.index(speech)
      #    for i,sp in enumerate(subspeeches):
      #      parent.insert(i+1,sp)
      
      #elements = self.tree.xpath(".//*")
      #for e in elements:
      #  if not e.tag == "p":
      #    try:
      #      if len(e.text)>0:
      #        print e.text
      #        n=Etree.SubElement(e,"stage-direction")
      #        n.text = e.text
      #        e.text = None
      #    except Exception as e:
            #print e
      #      pass





    def recursive_ids(self,start,begin_tag=""):
      children = start.getchildren()
      for i,child in enumerate(children):
        child.set("id",begin_tag+"."+str(i+1))
        if not len(child.getchildren()) == 0:
          self.recursive_ids(child,begin_tag=begin_tag+".%s"%(i+1))

    def filter_outside_topic(self):
      proceedings = self.tree.xpath("/proceedings")[0]
      children = proceedings.getchildren()
      for child in children:
        if not child.tag == "topic":
          child.getparent().remove(child)

    def make_element(self,parent,name,ns):
      return Etree.SubElement(parent,"{%s}%s"%(self.namespaces[ns],name),nsmap={ns:self.namespaces[ns]})
    
    def set(self,element,name,value,ns):
      return element.set("{%s}%s"%(self.namespaces[ns],name),value)

    def make_element_without_ns(self,parent,name):
      return Etree.SubElement(parent,"%s"%name)

    def make_source(self,source,url,linktype):
      subsource = self.make_element(source,"source","dc")
      self.set(subsource,"used-source","true","pm")
      self.create_link(subsource,url=url,linktype=linktype,source=url)

    def create_link(self,links,url="",linktype="",source="",description=""):
      link = self.make_element(links,"link","pm")
      if not linktype == "": self.set(link,"linktype",linktype,"pm")
      if not source == "": self.set(link,"source",source,"pm")
      if not description == "": self.set(link,"description",description,"pm")
      if "MP profil" in description:
        try:
          opener = urllib2.build_opener(urllib2.HTTPRedirectHandler)
          request = opener.open(url)
          url = request.url
        except Exception:
          pass
      link.text = url

    def set_namespace(self):
      proceedings = self.tree.xpath("/proceedings")[0]
      proceedings.tag = Etree.QName("http://www.politicalmashup.nl",proceedings.tag)
      elements = proceedings.xpath('.//*')
      #self.set(proceedings,"id","ca.p.0","pm") 
      for e in elements:
        if e.tag == "subtopic":
          e.tag = Etree.QName("http://dilipad.history.ac.uk",e.tag)
        else:
          e.tag = Etree.QName("http://www.politicalmashup.nl",e.tag)
        #self.set(e,"id","ca.p.0","pm") 
        for a in e.attrib.keys():
          a_value = e.get(a)
          e.set('{http://www.politicalmashup.nl}%s'%a,a_value)
          e.attrib.pop(a)
        #e.set('{http://www.politicalmashup.nl}%s'%"id","ca.p.0")
      self.root.append(proceedings)

    def arrange_topic_hierarchy(self):
      scenes = self.tree.xpath('.//scene')
      for scene in scenes:
        if scene.find('scene') != None:
          scene.tag = "subtopic"
          scene.set('type',"topic")

    def arrange_votes(self):
      vote_elements = self.tree.xpath('.//vote_element')
      for v in vote_elements:
        #parent = v.getparent()
        #position = parent.index(v)
        v.tag = "stage-direction"
        v.set("type","vote")
        title = v.xpath('vote/@title')[0]
        old= v.xpath('vote')[0]
        old.getparent().remove(old)

    def add_required_attributes(self):
      scenes = self.tree.xpath('.//scene')
      for scene in scenes:
        scene.set('type','topic')

    def add_docinfo(self):
      docinfo = self.make_element(self.root,"docinfo","pmd")
      comment = self.make_element(docinfo,"comment","pm")
      comment.text = "Data is valid with respect to the Relax NG schema http://schema.politicalmashup.nl/memberX.html. This is the open version of http://schema.politicalmashup.nl/member.html"

    def add_meta(self):
      meta = Etree.SubElement(self.root,"meta")
      #self.set(meta,"id",self.key+".meta","pm") # TO DO: change later
      dateid = str(convert_date_filename(self.f)).split()[0]

      self.set(meta,"id","ca.proc.d.%s"%dateid,"pm") # TO DO: change later
      identitifier = self.make_element(meta,"identifier","dc")
      identitifier.text = ""
      format = self.make_element(meta,"format","dc")
      format.text = "text/xml"
      Type = self.make_element(meta,"type","dc")
      Type.text = "Members"
      contributor = self.make_element(meta,"contributor","dc")
      contributor.text = "http://www.politicalmashup.nl"
      coverage = self.make_element(meta,"coverage","dc")
      country = self.make_element_without_ns(coverage,"country")
      self.set(country,"ISO3166-1","CA","dcterms")
      country.text = "Canada"
      creator = self.make_element(meta,"creator","dc")
      creator.text =  "http://dilipad.history.ac.uk"
      language = self.make_element(meta,"language","dc")
      plang = self.make_element(language,"language","pm")
      self.set(plang,"ISO639-2","eng","dcterms")
      plang.text = "English"
      publisher = self.make_element(meta,"publisher","dc")
      publisher.text = "http://www.parl.gc.ca/parlinfo/"
      rights = self.make_element(meta,"rights","dc")
      rights.text = "http://www.parl.gc.ca/parlinfo/"
      date = self.make_element(meta,"date","dc")
      date.text = dt.isoformat(dt.now()).split('T')[0]+"+01:00"
      title = self.make_element(meta,"title","dc")
      title.text = "Canadian House of Commons Proceedings"
      description = self.make_element(meta,"description","dc")
      description.text = "Canadian House of Commons Proceedings"
      source = self.make_element(meta,"source","dc")
      #self.make_source(source,"http://www.politicalmashup.nl","trusted")
      self.make_source(source,"http://www.parl.gc.ca/parlinfo","trusted")
      self.make_source(source,"na","trusted")
      self.make_element(meta,"subject","dc")
      relation = self.make_element(meta,"relation","dc")
      relation.text = "All pm:party-ref attributes appearing in http://www.politicalmashup.nl documents refer to http://resolver.politicalmashup.nl/[pm:party-ref] ."

#******************************
#***** speaker function *******
#******************************

class speakerFunction():
    """
    This class loops through all the documents and tries to link names to member based on certain linking rules.
    The rules are saved in a separate documents "speaker_function" (and "not_linked" for members who couldn't be linked.
    """
    def __init__(self, members_directory="/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/34thParliament/34thParliamentMembers/ca-members-postpr",parliament_directory="/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/34thParliament/34thParliament_stage_2",begin="1988-11-21",end="1993-09-08"):
        self.directory = members_directory
        self.proc_directory = parliament_directory
        self.files = [os.path.join(self.directory,f) for f in os.listdir(self.directory) if not f.startswith(".")]
        self.proceedings = [os.path.join(self.proc_directory,f) for f in os.listdir(self.proc_directory) if not f.startswith(".")]
        self.begin = date(begin)
        self.end = date(end)
        self.output_metadata = codecs.open('/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/34thParliament/speaker_function.txt',"w",encoding="utf-8")
        self.output_not_linked = codecs.open('/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/34thParliament/not_linked.txt',"w",encoding="utf-8")
        self.unique = []
        self.ambiguous = []
        self.namespaces = {"pm":"http://www.politicalmashup.nl"}
        self.titles = [u"M.",u"Mr.","Mr,",u"Miss",u"The Hon.",u"The hon.",u"Mrs.",u"The Right Hon.",u"The right Hon.",u"Right Hon.","Ms.",u"Hon."]
        #self.temp_output = codecs.open("/Users/kasparbeelen/Desktop/normalisation.txt","w",encoding="utf-8")
    
    def check_for_presence(self,tree): # TO DO: manually chech later
      """
      Check whether an MP could be present during the given legislature
      Legislature boundaries are set in __init__
      """
      name = tree.xpath("/root/pm:member[1]/pm:name[1]/pm:full[1]",namespaces=self.namespaces)[0].text
      try:
        commons = tree.xpath('/root/pm:member[1]/pm:memberships[1]/pm:membership[@pm:body="commons"]',namespaces=self.namespaces)
        if commons == []: 
          print "%s is not present" % name
          return False
        for c in commons:
          begin = date(c.xpath("pm:period/@pm:from",namespaces=self.namespaces)[0])
          end = date(c.xpath('pm:period/@pm:till',namespaces=self.namespaces)[0])
          if self.begin <= begin and self.end >= end:
            print "%s is present" % name
            return True
      except Exception as e:
        print "Error{%s} for %s" % (e,name)
        return True

    def get_ambiguous_names(self):
      """
      Loops through the member files, checks if a member could be present at a given time, and then separates uniques surnames from ambiguous ones

      """
      full_names = []
      surnames = {}
      for f in self.files:
        # collect all surnames from present members
        with open(f,"rt") as member:
          tree = Etree.parse(member)
          if not self.check_for_presence(tree) == True: continue
          surname = tree.xpath('/root/pm:member[1]/pm:name[1]/pm:last[1]',namespaces={"pm":"http://www.politicalmashup.nl"})[0].text
          firstname = tree.xpath('/root/pm:member[1]/pm:name[1]/pm:first[1]',namespaces={"pm":"http://www.politicalmashup.nl"})[0].text
          id = tree.xpath('/root/pm:member[1]/@pm:id',namespaces={"pm":"http://www.politicalmashup.nl"})[0]
          full_names.append((firstname,surname,id))
          surnames.setdefault(surname,0)
          surnames[surname]+=1
      # check which name appears more then once
      ambiguous = [i for i,j in surnames.items() if j >=2]
      for i,j,id in full_names:
        if not j in ambiguous:
          self.unique.append((i,j,id))
        else:
          self.ambiguous.append((i,j,id))

      #print self.ambiguous
      #print len(self.ambiguous)
      #print len(self.unique)

    def find_all_speakers(self):
        """ 
        Collect all speaker names in the XML documents.
        """
        speakers = []
        #print "total files",len(self.proceedings)
        for xmlf in self.proceedings:
            print xmlf
            try:
              with open(xmlf,"rt") as f:
                  tree = Etree.parse(f)
                  speakers.extend(tree.xpath('.//speech/@speaker'))
            except Exception:
              continue
        #speakers = list(set([self.normalize_name(i) for i in speakers]))
        speakers = list(set(speakers))
        self.speakers = [i for i,j in sorted(Counter(speakers).items(),key=itemgetter(1),reverse=True)]
        print "total amount of speakers found",len(self.speakers)
        #print self.speakers

    def normalize_name(self,text):
          """
          A bit messy code, but tries to clean the speaker's name string. For excample The hon. Zoro (Minister for Blabla) should become just Zoro
          The idea is to prepare the name for string matching
          I haven't done anything with strings like "The Acting Speaker (Mr X)"

          """
          Text = text
          #if "acting" in text.lower() or "deputy" in text.lower() or "chair" in text.lower() or "speaker" in text.lower():
          if "acting" in text.lower():

            return text[:-1].strip()
            #try:
              #text = text.split('(')[1].strip(')')
              #self.normalize_name(text)
            #except IndexError:
              #pass
              #self.temp_output.write(u'{}\t{}\n'.format(Text,text[:-1].strip()))
              #return text[:-1].strip()
          if "(" in text:
            text = text.split("(")[0]
          else:
            pass
          text = text.replace(':',"").replace("moved","")
          for t in self.titles:
            if text.startswith(t):
              text = text.replace(t,"")
          text = text.strip().strip(')')
          #self.temp_output.write(u'{}\t{}\n'.format(Text,text))
          return text

    def link_speakers(self):
        """
        This is the main function for linking
        The idea is to link members according to certain string matching rules and keep track of this procedure in a log file
        "self.find_replacement" is the crucial function here.
        If no replacement is found the name is coded as Rule2
        """
        rules = []
        not_linked = 0
        unique_surnames = [j for i,j,id in self.unique]
        for speaker in self.speakers:
          rule = self.find_replacement(speaker,unique_surnames)
          if not rule in rules:

            if "ruleZ" in rule:
              not_linked+=1
              self.output_not_linked.write(rule)
            #else:
              #pass
          #else:
            self.output_metadata.write(rule)
            rules.append(rule)
        self.output_metadata.close()
        print not_linked
        
    def remove_diacritics(self,name):
      """
      For some reason this function doens't work properly.
      For string matching I tried to convert namaes as Chrétien to Chretien, because spelling differs between authority files and proceedings
      """
      name = u"%s"%name
      name = re.compile(ur"[éèê]",re.UNICODE|re.DOTALL).sub("e",name)
      name = re.compile(ur"[ô]",re.UNICODE|re.DOTALL).sub("o",name)
      name = re.compile(ur"[à]",re.UNICODE|re.DOTALL).sub("a",name)
      return name

    def find_replacement(self,speaker,unique_surnames=[]):
            """
            Given a name, this function returns a replacement and a rule
            Rules indicate the conditions under which strungs where matched
            """
            speaker_original = speaker
            speaker = self.normalize_name(speaker).split()
            if len(speaker) == 0:
              return "Error"

            elif len(speaker) == 1:
                speaker = speaker[0]
                if speaker in unique_surnames:
                  for i,j,id in self.unique:
                    if j == speaker:
                      return u"{}=={}\t{}\trule1\n".format(speaker_original," ".join((i,j)),id)
                      # TO DO: Something here
                else:
                  return u"{}\truleY\n".format(speaker_original)
                  
            elif len(speaker) > 1:
                firstName,lastName = speaker[0],speaker[-1]
                if lastName in unique_surnames:
                  for i,j,id in self.unique:
                    if self.remove_diacritics(j) == lastName:
                      return u"{}=={}\t{}\trule2\n".format(speaker_original," ".join((i,j)),id)
                    else:
                      pass
                #else:
                #  pass
                      
                elif lastName not in unique_surnames:
                  for i,j,id in self.ambiguous:
                    if self.remove_diacritics(i).lower() == firstName.lower() and self.remove_diacritics(j).lower() == lastName.lower():
                      return u"{}=={}\t{}\trule3\n".format(speaker_original," ".join((i,j)),id)
                      
                    elif self.remove_diacritics(i).lower().startswith(firstName[:3].lower()) and self.remove_diacritics(j).lower() == lastName.lower():
                      return u"{}=={}\t{}\trule4\n".format(speaker_original," ".join((i,j)),id)
                  
                    elif firstName.lower() in self.remove_diacritics(i).lower() and self.remove_diacritics(j).lower() == lastName.lower():
                      return u"{}=={}\t{}\trule5\n".format(speaker_original," ".join((i,j)),id)
                    elif firstName.lower() in self.remove_diacritics(i).lower() and lastName.lower() in self.remove_diacritics(j).lower():
                      return u"{}=={}\t{}\trule6\n".format(speaker_original," ".join((i,j)),id)
                    elif self.remove_diacritics(i).lower().startswith(firstName[:1].lower()) and self.remove_diacritics(j).lower() == lastName.lower():
                      return u"{}=={}\t{}\trule7\n".format(speaker_original," ".join((i,j)),id)
                    else:
                      pass
                  else:
                    pass

            return u"{}\truleZ\n".format(speaker_original)

        
        
    def date_parliaments(self,doel):
        # give for each election beginning and end date of the parliament
        # return a date of exect given date of election
        d = {date("1993-10-25"):[date("1994-01-17"),date("1997-04-27")],\
                date("1988-11-21"):[date('1988-12-12'),date("1993-09-08")],\
                date("1984-09-04"):[date('1984-11-05'),date('1988-10-01')],\
                date("1980-02-18"):[date('1980-04-14'),date('1984-07-09')],\
                date("1979-05-22"):[date('1979-10-09'),date('1979-12-14')],\
                date("1974-07-08"):[date('1974-09-30'),date('1979-03-26')],\
                date("1972-10-30"):[date('1973-01-04'),date('1974-05-09')],\
                date("1968-06-25"):[date('1968-09-12'),date('1972-09-01')],\
                date("1965-11-08"):[date('1966-01-18'),date('1968-04-23')],\
                date("1963-04-08"):[date('1963-05-16'),date('1965-09-08')],\
                date("1962-06-18"):[date('1962-09-27'),date('1963-02-06')],\
                date("1958-03-31"):[date('1958-05-12'),date('1962-04-19')],\
                date("1957-06-10"):[date('1957-10-14'),date('1958-02-01')],\
                date("1953-08-10"):[date('1953-11-12'),date('1957-04-12')],\
                date("1949-06-27"):[date('1949-09-15'),date('1953-06-13')],\
                date("1945-06-11"):[date('1945-09-06'),date('1949-04-30')]}
        for i,j in d.items():
            if doel >= i and doel <= j[1]:
                return j[1]
            else:
                pass

        return None

    def create_speaker_function(self):
        self.find_all_speakers()
        self.reduce_members_metadata()
        self.find_links()

#******************************
#******** link data ***********
#******************************


class getContextInformation(object):
    """
    Given a set of replacement rules, this class will link retrieve information from authority files and add it to the proceedings.
    """
    def __init__(self,xmlfile,directory,output_directory,members_directory):
        print xmlfile
        self.tree = Etree.parse(os.path.join(directory,xmlfile))
        self.output = codecs.open(os.path.join(output_directory,xmlfile),"w",encoding="utf-8")
        self.date = convert_date_filename(xmlfile)
        self.speakers = []
        # path the stored rules
        self.replacement_rules = set(codecs.open("/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/34thParliament/speaker_function.txt","r",encoding="utf-8").read().split('\n'))
        # there is a separate file to link the Speakers of the House (and Deputy Speaker)
        self.speaker_rules = set(codecs.open("/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/34thParliament/speakers.txt","r",encoding="utf-8").read().split('\n'))
        self.refs = {}
        self.speaker_refs = {}
        self.attribute_data = {}
        self.ambiguous = []
        self.not_linked = []
        self.members_directory = members_directory
        self.titles = [u"M.",u"Mr.","Mr,",u"Miss",u"The Hon.",u"The hon.",u"Mrs.",u"The Right Hon.",u"The right Hon.",u"Right Hon.","Ms.",u"Hon."]
        self.namespaces = {"pm":"http://www.politicalmashup.nl",\
            "dc":"http://purl.org/dc/elements/1.1/",\
            "xsd":"http://www.w3.org/2001/XMLSchema-datatypes",\
            "owl":"http://www.w3.org/2002/07/owl#",\
            "pmd":"http://www.politicalmashup.nl/docinfo",\
            "rdf":"http://www.w3.org/1999/02/22-rdf-syntax-ns#",\
            "html":"http://www.w3.org/1999/xhtml",\
            "dcterms":"http://purl.org/dc/terms/",\
            "xsi":"http://www.w3.org/2001/XMLSchema-instance",\
            "pmx":"http://www.politicalmashup.nl/extra",\
            "openpx":"https://openparliament.ca/extra"}
        
    def get_speakers(self):
        """
        find all speakers in a document
        """
        self.speakers = list(set(self.tree.xpath('.//speech/@speaker')))
        print "Total number of speaker",len(self.speakers)
    
    def load_speaker_file(self):
      """
      load the speaker file with information on the Speaker and the Deputy Speaker of the House
      add speaker to dictionary with key=name and value=id
      """
      for r in self.speaker_rules:
        #print r
        name,id,rule = r.split('\t')
        name = name.split("==")[0]
        self.speaker_refs[name] = id.lower()

    
    def load_replacement_file(self):
        """
        load the file which links all other MPs the their id
        add speaker to dictionary with key=name and value=id
        names are processed depending onth rules generated in previous stage
        """
        for r in self.replacement_rules:
          elements = r.split('\t')

          if len(elements) == 2:
            name,rule = elements
            if rule.endswith("Y"):
              self.ambiguous.append(name)
            elif rule.endswith("Z"):
              self.not_linked.append(name)
          elif len(elements) == 3:
            name,id,rule = elements
            name = name.split("==")[0]
            self.refs[name] = id.lower()
       

    def normalize_name(self,text):
          """
          make names pretty and ready for processing
          """
          Text = text
          if "acting" in text.lower():
            return text[:-1].strip()
          elif "speaker" in text.lower():
            return text[:-1].strip()
          if "(" in text:
            text = text.split("(")[0]
          else:
            pass
          text = text.replace(':',"").replace("moved","")
          for t in self.titles:
            if text.startswith(t):
              text = text.replace(t,"")
          text = text.strip().strip(')')
          return text
        
    def add_speaker_attributes(self):
        """
        set the attributes to the speeches
        first for the Speaker of the House, then for other MPs, and then for the ambiguous names
        """
        count_speeches = 0
        count_refs = 0
        speeches = self.tree.xpath('.//speech')
        for speech in speeches:
            count_speeches+=1
            speaker = speech.xpath("@speaker")[0]
            #speaker = self.normalize_name(speaker)
            if speaker in self.speaker_refs.keys():
              count_refs+=1
              speech.get(u'member-ref')
              speech.set(u'member-ref',self.speaker_refs[speaker])
              self.add_other_attributes(self.speaker_refs[speaker],speech)

            elif speaker in self.refs.keys():
              count_refs+=1
              speech.get(u'member-ref')
              speech.set(u'member-ref',self.refs[speaker])
              self.add_other_attributes(self.refs[speaker],speech)
            elif speaker in self.ambiguous:
              search = self.link_ambiguous(speech,speeches,speaker)
              if search != None:
                self.add_other_attributes(search,speech)
                count_refs+=1
              else:
                pass
              
        return count_refs,count_speeches
        print "%0.3f speeches linked {%d,%d}"%(count_refs/count_speeches,count_refs,count_speeches)

    def link_ambiguous(self,speech,speeches,speaker):
        """
        if a surname in the proceedings is ambiguous, meaning only the surname is notated and multiple members have the surname
        then go up and search for the full name and link it

        """
        i = speeches.index(speech)
        untill_speech = speeches[:i]
        for s in reversed(untill_speech):
          if speaker in self.normalize_name(s.xpath('@speaker')[0]) and s.xpath("@member-ref") != []:
            #print speaker,s.xpath('@speaker')[0],s.xpath("@member-ref")
            s.set("member-ref",s.xpath("@member-ref")[0])
            return s.xpath("@member-ref")[0]
        return None

        
    def add_other_attributes(self,id,speech):
      """
      Add other attributes besides members refs
      """
      tree = Etree.parse(os.path.join(self.members_directory,id+".xml"))
      self.retrieve_attributes_by_name("party-name",tree,speech)
      self.retrieve_attributes_by_name("party-ref",tree,speech)
      self.retrieve_attributes_by_name('district',tree,speech)
      
        
    def retrieve_attributes_by_name(self,element_name,tree,speech):
        """
        search for an attribute value given the name of the attribute 
        """
        elements = tree.xpath('/root/pm:member/pm:memberships/pm:membership[@pm:%s]' % element_name,namespaces=self.namespaces)
        for element in elements:
          begin = date(element.xpath("pm:period/@pm:from",namespaces=self.namespaces)[0])
          end = date(element.xpath('pm:period/@pm:till',namespaces=self.namespaces)[0])
          if self.date >= begin and self.date <= end:
            speech.set(element_name,element.xpath("@pm:%s"%element_name,namespaces=self.namespaces)[0])
            #return element.xpath("@pm:%s"%element_name,namespaces=self.namespaces)

    def add_attributes(self):
      """
      main function
      """
      self.get_speakers()
      self.load_speaker_file()
      self.load_replacement_file()
      refs,speeches=self.add_speaker_attributes()
      self.output.write(Etree.tostring(self.tree,method="xml",encoding="unicode",pretty_print=True))
      return refs,speeches
           

#******************************
#**** additional functions ****
#******************************

def convert_date_filename(doc_date):
    print doc_date
    months = {"jan":"01","feb":"02","mar":"03","apr":"04","may":"05","jun":"06","jul":"07","aug":"08","sep":"09","oct":"10","nov":"11","dec":"12"}
    doc_date = doc_date.split('/')[-1].split('.')[0].split('_')
    day,month,year = "","",""
    month = months[doc_date[0].lower()[:3]]
    day = doc_date[1]        
    year = doc_date[2]
    return date("-".join((year,month,day)))

def main(stage_1=False,stage_2=False,stage_3=False,stage_4=False,stage_5=False):
    directories = [#'/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/TestParliament/TestParliament',\
    '/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/34thParliament/34thParliament',\
    #"/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/33rdParliament/33rdParliament",\
    #'/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/32ndParliament/32ndParliament'
    ]
    errors = open("/Users/kasparbeelen/Documents/Dilipad/CanadaHansard_XML/errors.txt","w")

    for directory in directories:
        directory_st_1 = directory+"_stage_1"
        directory_st_2 = directory+"_stage_2"
        directory_st_3 = directory+"_stage_3"
        members_files = os.path.join(directory+"Members","ca-members-final")
        print members_files
        if stage_1 == True:
            for doc_name in os.listdir(directory):
                print doc_name
                if doc_name.startswith('.'):continue
                doc = Transformer(doc_name,directory,directory_st_1)
                doc.preprocess_text_for_markup()
                doc.markup_entities()
                doc.markup_elements()

        if stage_2 == True:
            for d in os.listdir(directory_st_1):
                #print d
                if d.startswith('.'):continue
                try:
                  xml=xmlTransformer(d,directory_st_1,directory_st_2)
                  xml.transform()
                except Exception as e:
                    message = "Error={%s} with file {%s}" % (e,d)
                    print message
                    errors.write(message)

        if stage_3 == True:
          for d in os.listdir(directory_st_1):
                #print d
                if d.startswith('.'):continue
                try:
                  xml=PMTransformer(d,directory_st_2,directory_st_3)
                  xml.transform()
                except Exception as e:
                  message = "Error={%s} with file {%s}" % (e,d)
                  print message

        if stage_4 == True:
            s = speakerFunction()
            s.get_ambiguous_names()
            s.find_all_speakers()
            s.link_speakers()

        if stage_5 == True:
            warnings = []
            speech_total = 0
            refs_total = 0
            for doc_name in os.listdir(directory_st_2):
                #print doc_name
                if not doc_name.startswith('.'):
                  xml_w_attrib = getContextInformation(doc_name,directory_st_2,directory_st_3,members_files) 
                  refs,speeches = xml_w_attrib.add_attributes()
                  ratio = refs/speeches
                  if ratio <= 0.6:
                    print "WARNING: Possibly wrongly dated file {%s} ratio={%0.3f}" %(doc_name,ratio)
                    warnings.append((doc_name,ratio))
                  speech_total+=speeches
                  refs_total+=refs
                  print "Total number of speeches linked=%0.3f {%d,%d}"% (refs_total/speech_total,refs_total,speech_total)
            print warnings

if __name__ == '__main__':
    main(stage_1=False,stage_2=False,stage_3=True,stage_4=False,stage_5=False)
    #s = speakerFunction()
    #s.get_ambiguous_names()
    #s.find_all_speakers()
    #s.link_speakers()
    