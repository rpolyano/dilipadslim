﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLPHelpers
{
    public class CrossPlatformTopicValidator
    {
        static string jar;
        static string model;

        [ThreadStatic]
        static Process taggerProcess;
        public static void Init(string jarRootDir, string modelsDir)
        {
            jar = jarRootDir + "/StanfordPOSWrapper.jar";
            model = modelsDir + "/wsj-0-18-bidirectional-nodistsim.tagger";
            taggerProcess = Process.Start(new ProcessStartInfo()
            {
                FileName = "java",
                Arguments = "-jar \"" + jar + "\" " + "\"" + model + "\"",
                RedirectStandardOutput = true,
                RedirectStandardInput = true,
                WorkingDirectory = Path.GetDirectoryName(jar),
                UseShellExecute = false
            });
        }

        public bool SentenceContinues(string sent)
        {
            string tagged = TagSentence(sent);
            string last = tagged.Split(' ').Reverse().Skip(1).First();
            string pos = last.Split('/').Last().Trim();
            if (TopicValConstants.BAD_SENTENCE_ENDINGS.Contains(pos))
                return true;
            return false;
        }

        public bool SentenceIsContinuation(string sent)
        {
            string tagged = TagSentence(sent);
            string first = tagged.Split(' ').First();
            string pos = first.Split('/').Last().Trim();
            if (TopicValConstants.BAD_SENTENCE_BEGINNINGS.Contains(pos))
                return true;
            return false;
        }

        private string TagSentence(string sent)
        {
            sent = sent[0] + sent.ToLower().Substring(1).TrimEnd('.') + ".";
            taggerProcess.StandardInput.WriteLine(sent);
            return taggerProcess.StandardOutput.ReadLine();
        }

        public static void Close()
        {
            taggerProcess.StandardInput.WriteLine();
        }
    }
}
