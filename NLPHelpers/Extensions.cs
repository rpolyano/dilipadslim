﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLPHelpers
{
    public static class Extensions
    {
        /// <summary>
        /// Returns the first index of a that is followed by b, or -1 if no such sequence occurs.
        /// </summary>
        /// <param name="str">String to look in</param>
        /// <param name="a">First character</param>
        /// <param name="b">Second character</param>
        /// <param name="except">Enumerable of indicies to ignore</param>
        public static int GetIndexOfConsecutive(this string str, char a, char b, IEnumerable<int> except)
        {
            for (int i = 0; i < str.Length - 1; i++)
            {
                if (except.Contains(i)) continue;

                if (str[i] == a && str[i + 1] == b)
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Returns the indicies of every occurence of x
        /// </summary>
        /// <param name="str">The string to look in</param>
        /// <param name="x">The character to look for</param>
        /// <returns></returns>
        public static int[] AllIndiciesOf(this string str, char x)
        {
            return str.Select((chr,i) => new { chr, i })
                .Where((a) => a.chr == x)
                .Select(a=>a.i)
                .ToArray();
        }
    }
}
