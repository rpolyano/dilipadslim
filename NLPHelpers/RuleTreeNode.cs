﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLPHelpers
{
    public class RuleApplication
    {
        public ConditionalRule Rule { get; set; }
        public int Rule1ApplicationIndex { get; set; }
        public int Rule2ApplicationIndex { get; set; }

        public override string ToString()
        {
            return Rule.Rule1.ToShortLatexString() + "@" + Rule1ApplicationIndex + " " +
                Rule.Rule2.ToShortLatexString() + "@" + Rule2ApplicationIndex + " " +
                Math.Round(Rule.ConditionalProbability, 2) + "," +
                Math.Round(Rule.Rule1Probability, 2);
        }
    }
    public class RuleTreeNode
    {
        public string WordState { get; set; }
        public IEnumerable<ConditionalRule> ApplicableRules { get; set; }
        public IEnumerable<string> AuthoritativeList { get; set; }

        /// <summary>
        /// The rule that was applied to the parent get the current state
        /// </summary>
        public RuleApplication LastApplication { get; set; }

        public IEnumerable<RuleTreeNode> Children { get; set; }
        public RuleTreeNode Parent { get; set; }
        public IEnumerable<int> UntouchableChars { get; set; }
        public IEnumerable<ConditionalRule> MasterRuleSet { get; set; }
        public int LastClosestDistance { get; set; }
        public IEnumerable<Levenshtein> LevenshteinsToAuthoritative { get; set; }
        public int ClosestDistance { get; set; }

        public RuleTreeNode(RuleTreeNode parent, string state, RuleApplication lastApplication, IEnumerable<int> untouchableChars, IEnumerable<ConditionalRule> masterRuleSet, int lastClosestDistance, IEnumerable<string> authList)
        {
            Parent = parent;
            WordState = state;
            LastApplication = lastApplication;
            UntouchableChars = untouchableChars;
            MasterRuleSet = masterRuleSet;

            if (untouchableChars == null)
                untouchableChars = new int[0];

            ApplicableRules = GetApplicableRules(masterRuleSet, state, untouchableChars).ToArray();
            LastClosestDistance = lastClosestDistance;
            AuthoritativeList = authList;
            LevenshteinsToAuthoritative = authList.Select(auth =>
            {
                var lev = new Levenshtein(state, auth);
                lev.ComputeMatrix();
                return lev;
            }).ToArray().OrderBy(lv=>lv.GetScore()).ToArray();
            ClosestDistance = LevenshteinsToAuthoritative.First().GetScore();
            LevenshteinsToAuthoritative = new Levenshtein[] { LevenshteinsToAuthoritative.First() };
        }

        public void MakeChildren()
        {
            if (ApplicableRules.Count() == 0 || ClosestDistance > LastClosestDistance)
            {
                return;
            }

            List<String> newStates = new List<String>();
            List<RuleApplication> newStateApplications = new List<RuleApplication>();

            if (UntouchableChars == null)
            {
                UntouchableChars = new int[0];
            }
            foreach (var rule in ApplicableRules)
            {
                for (int i = 0; i < WordState.Length; i++)
                {
                    if (UntouchableChars.Contains(i))
                    {
                        continue;
                    }
                    if (rule.Consecutive)
                    {
                        if (i < WordState.Length - 1 && WordState[i].ToString() == rule.Rule1.From && WordState[i + 1].ToString() == rule.Rule2.From)
                        {
                            newStates.Add(rule.Apply(WordState, i, i + 1));
                            newStateApplications.Add(new RuleApplication() { Rule = rule, Rule1ApplicationIndex = i, Rule2ApplicationIndex = i + 1 });
                        }
                    }
                    else
                    {
                        if (WordState[i].ToString() == rule.Rule1.From)
                        {
                            string newState = rule.Rule1.Apply(WordState, i);
                            for (int j = 0; j < newState.Length; j++)
                            {
                                if ((rule.Rule1.Type != StringOperationType.Deletion && j == i) || UntouchableChars.Contains(j)) continue;

                                if (newState[j].ToString() == rule.Rule2.From)
                                {
                                    newStates.Add(rule.Rule2.Apply(newState, j));
                                    newStateApplications.Add(new RuleApplication() { Rule = rule, Rule1ApplicationIndex = i, Rule2ApplicationIndex = j });
                                }
                            }

                        }
                    }
                }
            }

            RuleTreeNode[] children = new RuleTreeNode[newStates.Count];
            for (int i = 0; i < newStates.Count; i++)
            {
                var untouchables = UntouchableChars.Union(new int[] { newStateApplications[i].Rule1ApplicationIndex, newStateApplications[i].Rule2ApplicationIndex }).Distinct();
                children[i] = new RuleTreeNode(this, newStates[i], newStateApplications[i], untouchables, MasterRuleSet, ClosestDistance, AuthoritativeList);
                children[i].MakeChildren();
            }
            Children = children;
        }



        public static IEnumerable<ConditionalRule> GetApplicableRules(IEnumerable<ConditionalRule> rules, string word, IEnumerable<int> untouchableIndicies)
        {
            return rules.Where(cr => (cr.Rule1.Type == StringOperationType.Insertion ||
                                word.AllIndiciesOf(cr.Rule1.From[0]).Except(untouchableIndicies).Count() != 0) &&
                 (cr.Rule2.Type == StringOperationType.Insertion ||
                 word.AllIndiciesOf(cr.Rule2.From[0]).Except(untouchableIndicies).Count() != 0) &&
                (!cr.Consecutive || cr.Rule1.Type == StringOperationType.Insertion || cr.Rule2.Type == StringOperationType.Insertion || (word.GetIndexOfConsecutive(cr.Rule1.From[0], cr.Rule2.From[0], untouchableIndicies) != -1)));
        }

        public string ToLatex()
        {
            string latex = "Root";
            if (LastApplication != null)
            {
                latex = @"\textsc{" + LastApplication.ToString() + "} ";
            }
            string children = "";
            if (Children != null && Children.Count() > 0)
                children = " " + Children.Select(child => child.ToLatex()).Aggregate((one, two) => one + " " + two);
            latex = "[." + latex + " [." + WordState + children + "\r\n]]";
            return latex;
        }

        public RuleTreeNode[] Walk()
        {
            return new RuleTreeNode[] { this }.Union(Children == null ? new RuleTreeNode[0] : Children.SelectMany(child => child.Walk())).ToArray();
        }
    }
}
