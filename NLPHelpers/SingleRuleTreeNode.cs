﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLPHelpers
{
    public class SingleRuleApplication
    {
        public StringOperation Rule { get; set; }
        public int Rule1ApplicationIndex { get; set; }
        

        public override string ToString()
        {
            return Rule.ToShortLatexString() + "@" + Rule1ApplicationIndex;
        }
    }
    public class SingleRuleTreeNode
    {
        public string WordState { get; set; }
        public IEnumerable<StringOperation> ApplicableRules { get; set; }
        public IEnumerable<string> AuthoritativeList { get; set; }

        /// <summary>
        /// The rule that was applied to the parent get the current state
        /// </summary>
        public SingleRuleApplication LastApplication { get; set; }

        public IEnumerable<SingleRuleTreeNode> Children { get; set; }
        public SingleRuleTreeNode Parent { get; set; }
        public IEnumerable<int> UntouchableChars { get; set; }
        public IEnumerable<StringOperation> MasterRuleSet { get; set; }
        public int LastClosestDistance { get; set; }
        public IEnumerable<Levenshtein> LevenshteinsToAuthoritative { get; set; }
        public int ClosestDistance { get; set; }

        public SingleRuleTreeNode(SingleRuleTreeNode parent, string state, SingleRuleApplication lastApplication, IEnumerable<int> untouchableChars, IEnumerable<StringOperation> masterRuleSet, int lastClosestDistance, IEnumerable<string> authList)
        {
            Parent = parent;
            WordState = state;
            LastApplication = lastApplication;
            UntouchableChars = untouchableChars;
            MasterRuleSet = masterRuleSet;

            if (untouchableChars == null)
                untouchableChars = new int[0];

            ApplicableRules = GetApplicableRules(masterRuleSet, state, untouchableChars.ToArray());
            LastClosestDistance = lastClosestDistance;
            AuthoritativeList = authList;
            LevenshteinsToAuthoritative = authList.Select(auth =>
            {
                var lev = new Levenshtein(state, auth);
                lev.ComputeMatrix();
                return lev;
            }).ToArray().OrderBy(lv=>lv.GetScore()).ToArray();
            ClosestDistance = LevenshteinsToAuthoritative.First().GetScore();
        }

        public void MakeChildren()
        {
            if (ApplicableRules.Count() == 0 || ClosestDistance > LastClosestDistance)
            {
                return;
            }

            List<String> newStates = new List<String>();
            List<SingleRuleApplication> newStateApplications = new List<SingleRuleApplication>();

            if (UntouchableChars == null)
            {
                UntouchableChars = new int[0];
            }
            foreach (var rule in ApplicableRules)
            {
                for (int i = 0; i < WordState.Length; i++)
                {
                    if (UntouchableChars.Contains(i))
                    {
                        continue;
                    }
                    if (WordState[i].ToString() == rule.From)
                    {
                        string newState = rule.Apply(WordState, i);
                        var lta = AuthoritativeList.Select(auth =>
                        {
                            var lev = new Levenshtein(newState, auth);
                            lev.ComputeMatrix();
                            return lev;
                        }).ToArray().OrderBy(lv => lv.GetScore()).ToArray().First();
                        if (lta.GetScore() <= ClosestDistance)
                        {
                            newStates.Add(newState);
                            newStateApplications.Add(new SingleRuleApplication() { Rule = rule, Rule1ApplicationIndex = i });
                        }
                        
                    }
                    //if (rule.Consecutive)
                    //{
                    //    if (i < WordState.Length - 1 && WordState[i] == rule.Rule1.From && WordState[i + 1] == rule.Rule2.From)
                    //    {
                    //        newStates.Add(rule.Apply(WordState, i, i + 1));
                    //        newStateApplications.Add(new RuleApplication() { Rule = rule, Rule1ApplicationIndex = i, Rule2ApplicationIndex = i + 1 });
                    //    }
                    //}
                    //else
                    //{
                    //    if (WordState[i] == rule.Rule1.From)
                    //    {
                    //        string newState = rule.Rule1.Apply(WordState, i);
                    //        for (int j = 0; j < newState.Length; j++)
                    //        {
                    //            if ((rule.Rule1.Type != StringOperationType.Deletion && j == i) || UntouchableChars.Contains(j)) continue;

                    //            if (newState[j] == rule.Rule2.From)
                    //            {
                    //                newStates.Add(rule.Rule2.Apply(newState, j));
                    //                newStateApplications.Add(new RuleApplication() { Rule = rule, Rule1ApplicationIndex = i, Rule2ApplicationIndex = j });
                    //            }
                    //        }

                    //    }
                    //}
                }
            }

            SingleRuleTreeNode[] children = new SingleRuleTreeNode[newStates.Count];
            for (int i = 0; i < newStates.Count; i++)
            {
                var untouchables = UntouchableChars.Union(new int[] { newStateApplications[i].Rule1ApplicationIndex}).Distinct();
                children[i] = new SingleRuleTreeNode(this, newStates[i], newStateApplications[i], untouchables, MasterRuleSet, ClosestDistance, AuthoritativeList);
                children[i].MakeChildren();
            }
            Children = children;
        }



        public static IEnumerable<StringOperation> GetApplicableRules(IEnumerable<StringOperation> rules, string word, IEnumerable<int> untouchableIndicies)
        {
            return rules.Where(cr => cr.Type == StringOperationType.Insertion ||
                                word.AllIndiciesOf(cr.From[0]).Except(untouchableIndicies).Count() != 0);
        }

        public string ToLatex()
        {
            string latex = "Root";
            if (LastApplication != null)
            {
                latex = @"\textsc{" + LastApplication.ToString() + "} ";
            }
            string children = "";
            if (Children != null && Children.Count() > 0)
                children = " " + Children.Select(child => child.ToLatex()).Aggregate((one, two) => one + " " + two);
            latex = "[." + latex + " [." + WordState + children + "\r\n]]";
            return latex;
        }

        public SingleRuleTreeNode[] Walk()
        {
            return new SingleRuleTreeNode[] { this }.Union(Children == null ? new SingleRuleTreeNode[0] : Children.SelectMany(child => child.Walk())).ToArray();
        }
    }
}
