﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegExTransformer
{
    class Log
    {
        public static string errorLogPath = "errors.log";
        public static string messageLogPath = "output.log";

        static String errLock = "lock";
        public static void LogError(string file, string error, bool fatal=false)
        {
            lock (logLock)
            {
                file = Path.GetFileNameWithoutExtension(file);
                Console.ForegroundColor = (fatal ? ConsoleColor.Red : ConsoleColor.Yellow);
                Console.WriteLine("[{0}] {1}", file, error);
                Console.ForegroundColor = ConsoleColor.Gray;
                string message = file + (fatal ? " (FATAL)" : "") + " - " + error;
                
                File.AppendAllText((fatal ? errorLogPath : messageLogPath), message + "\r\n" + Enumerable.Range(0, message.Length).Select(x => "=").AggregateBy("") + "\r\n");
            }
        }

        static String logLock = "lock";
        public static void LogMessage(string message, string file="INFO", ConsoleColor color = ConsoleColor.Gray, params object[] format)
        {
            message = (format == null ? message : String.Format(message, format));
            lock(logLock)
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.Write("[" + file + "] ");
                Console.ForegroundColor = color;
                Console.WriteLine(message);
                Console.ForegroundColor = ConsoleColor.Gray;
                File.AppendAllText(messageLogPath, file + " - " + message + "\r\n");
            }
           
        }
    }
}
