﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegExTransformer.Models
{

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class root
    {

        private docinfo docinfoField;

        private rootMeta metaField;

        private proceedings proceedingsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.politicalmashup.nl/docinfo")]
        public docinfo docinfo
        {
            get
            {
                return this.docinfoField;
            }
            set
            {
                this.docinfoField = value;
            }
        }

        /// <remarks/>
        public rootMeta meta
        {
            get
            {
                return this.metaField;
            }
            set
            {
                this.metaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.politicalmashup.nl")]
        public proceedings proceedings
        {
            get
            {
                return this.proceedingsField;
            }
            set
            {
                this.proceedingsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl/docinfo")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.politicalmashup.nl/docinfo", IsNullable = false)]
    public partial class docinfo
    {

        private string commentField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.politicalmashup.nl")]
        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rootMeta
    {

        private object identifierField;

        private string formatField;

        private object subjectField;

        private string typeField;

        private string contributorField;

        private coverage coverageField;

        private string creatorField;

        private language languageField;

        private string publisherField;

        private string rightsField;

        private System.DateTime dateField;

        private string titleField;

        private string descriptionField;

        private source sourceField;

        private string relationField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://purl.org/dc/elements/1.1/")]
        public object identifier
        {
            get
            {
                return this.identifierField;
            }
            set
            {
                this.identifierField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://purl.org/dc/elements/1.1/")]
        public string format
        {
            get
            {
                return this.formatField;
            }
            set
            {
                this.formatField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://purl.org/dc/elements/1.1/")]
        public object subject
        {
            get
            {
                return this.subjectField;
            }
            set
            {
                this.subjectField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://purl.org/dc/elements/1.1/")]
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://purl.org/dc/elements/1.1/")]
        public string contributor
        {
            get
            {
                return this.contributorField;
            }
            set
            {
                this.contributorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://purl.org/dc/elements/1.1/")]
        public coverage coverage
        {
            get
            {
                return this.coverageField;
            }
            set
            {
                this.coverageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://purl.org/dc/elements/1.1/")]
        public string creator
        {
            get
            {
                return this.creatorField;
            }
            set
            {
                this.creatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://purl.org/dc/elements/1.1/")]
        public language language
        {
            get
            {
                return this.languageField;
            }
            set
            {
                this.languageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://purl.org/dc/elements/1.1/")]
        public string publisher
        {
            get
            {
                return this.publisherField;
            }
            set
            {
                this.publisherField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://purl.org/dc/elements/1.1/")]
        public string rights
        {
            get
            {
                return this.rightsField;
            }
            set
            {
                this.rightsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://purl.org/dc/elements/1.1/", DataType = "date")]
        public System.DateTime date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://purl.org/dc/elements/1.1/")]
        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://purl.org/dc/elements/1.1/")]
        public string description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://purl.org/dc/elements/1.1/")]
        public source source
        {
            get
            {
                return this.sourceField;
            }
            set
            {
                this.sourceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://purl.org/dc/elements/1.1/")]
        public string relation
        {
            get
            {
                return this.relationField;
            }
            set
            {
                this.relationField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://purl.org/dc/elements/1.1/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://purl.org/dc/elements/1.1/", IsNullable = false)]
    public partial class coverage
    {

        private country countryField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public country country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class country
    {

        private string iSO31661Field;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("ISO3166-1", Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://purl.org/dc/terms/")]
        public string ISO31661
        {
            get
            {
                return this.iSO31661Field;
            }
            set
            {
                this.iSO31661Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://purl.org/dc/elements/1.1/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://purl.org/dc/elements/1.1/", IsNullable = false)]
    public partial class language
    {

        private language1 language1Field;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("language", Namespace = "http://www.politicalmashup.nl")]
        public language1 language1
        {
            get
            {
                return this.language1Field;
            }
            set
            {
                this.language1Field = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl")]
    [System.Xml.Serialization.XmlRootAttribute("language", Namespace = "http://www.politicalmashup.nl", IsNullable = false)]
    public partial class language1
    {

        private string iSO6392Field;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("ISO639-2", Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://purl.org/dc/terms/")]
        public string ISO6392
        {
            get
            {
                return this.iSO6392Field;
            }
            set
            {
                this.iSO6392Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://purl.org/dc/elements/1.1/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://purl.org/dc/elements/1.1/", IsNullable = false)]
    public partial class source
    {

        private sourceSource source1Field;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("source")]
        public sourceSource source1
        {
            get
            {
                return this.source1Field;
            }
            set
            {
                this.source1Field = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://purl.org/dc/elements/1.1/")]
    public partial class sourceSource
    {

        private link linkField;

        private bool usedsourceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.politicalmashup.nl")]
        public link link
        {
            get
            {
                return this.linkField;
            }
            set
            {
                this.linkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("used-source", Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.politicalmashup.nl")]
        public bool usedsource
        {
            get
            {
                return this.usedsourceField;
            }
            set
            {
                this.usedsourceField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.politicalmashup.nl", IsNullable = false)]
    public partial class link
    {

        private string linktypeField;

        private string sourceField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string linktype
        {
            get
            {
                return this.linktypeField;
            }
            set
            {
                this.linktypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string source
        {
            get
            {
                return this.sourceField;
            }
            set
            {
                this.sourceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.politicalmashup.nl", IsNullable = false)]
    public partial class proceedings
    {

        private proceedingsTopic[] topicField;

        private string idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("topic")]
        public proceedingsTopic[] topic
        {
            get
            {
                return this.topicField;
            }
            set
            {
                this.topicField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl")]
    public partial class proceedingsTopic
    {

        private object[] itemsField;

        private string titleField;

        private string idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("speech", typeof(speech1), Namespace = "http://dilipad.history.ac.uk")]
        [System.Xml.Serialization.XmlElementAttribute("scene", typeof(proceedingsTopicScene))]
        [System.Xml.Serialization.XmlElementAttribute("speech", typeof(proceedingsTopicSpeech))]
        [System.Xml.Serialization.XmlElementAttribute("stage-direction", typeof(proceedingsTopicStagedirection))]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://dilipad.history.ac.uk")]
    [System.Xml.Serialization.XmlRootAttribute("speech", Namespace = "http://dilipad.history.ac.uk", IsNullable = false)]
    public partial class speech1
    {

        private p pField;

        private string speakerField;

        private string timeField;

        private string idField;

        private string speakeroldField;

        private string speakeridField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.politicalmashup.nl")]
        public p p
        {
            get
            {
                return this.pField;
            }
            set
            {
                this.pField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.politicalmashup.nl")]
        public string speaker
        {
            get
            {
                return this.speakerField;
            }
            set
            {
                this.speakerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.politicalmashup.nl")]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("speaker-old", Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string speakerold
        {
            get
            {
                return this.speakeroldField;
            }
            set
            {
                this.speakeroldField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("speaker-id", Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string speakerid
        {
            get
            {
                return this.speakeridField;
            }
            set
            {
                this.speakeridField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.politicalmashup.nl", IsNullable = false)]
    public partial class p
    {

        private string idField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl")]
    public partial class proceedingsTopicScene
    {

        private object[] itemsField;

        private string titleField;

        private string typeField;

        private string idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("subtopic", typeof(subtopic), Namespace = "http://dilipad.history.ac.uk")]
        [System.Xml.Serialization.XmlElementAttribute("speech", typeof(proceedingsTopicSceneSpeech))]
        [System.Xml.Serialization.XmlElementAttribute("stage-direction", typeof(proceedingsTopicSceneStagedirection))]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://dilipad.history.ac.uk")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://dilipad.history.ac.uk", IsNullable = false)]
    public partial class subtopic
    {

        private object[] itemsField;

        private string titleField;

        private string idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("subtopic", typeof(subtopicSubtopic))]
        [System.Xml.Serialization.XmlElementAttribute("speech", typeof(speech), Namespace = "http://www.politicalmashup.nl")]
        [System.Xml.Serialization.XmlElementAttribute("stage-direction", typeof(stagedirection), Namespace = "http://www.politicalmashup.nl")]
        [System.Xml.Serialization.XmlElementAttribute("vote", typeof(vote), Namespace = "http://www.politicalmashup.nl")]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.politicalmashup.nl")]
        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.politicalmashup.nl")]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://dilipad.history.ac.uk")]
    public partial class subtopicSubtopic
    {

        private object[] itemsField;

        private string titleField;

        private string idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("subtopic", typeof(subtopicSubtopicSubtopic))]
        [System.Xml.Serialization.XmlElementAttribute("speech", typeof(speech), Namespace = "http://www.politicalmashup.nl")]
        [System.Xml.Serialization.XmlElementAttribute("stage-direction", typeof(stagedirection), Namespace = "http://www.politicalmashup.nl")]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.politicalmashup.nl")]
        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.politicalmashup.nl")]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://dilipad.history.ac.uk")]
    public partial class subtopicSubtopicSubtopic
    {

        private object[] itemsField;

        private string titleField;

        private string idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("subtopic", typeof(subtopicSubtopicSubtopicSubtopic))]
        [System.Xml.Serialization.XmlElementAttribute("speech", typeof(speech), Namespace = "http://www.politicalmashup.nl")]
        [System.Xml.Serialization.XmlElementAttribute("stage-direction", typeof(stagedirection), Namespace = "http://www.politicalmashup.nl")]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.politicalmashup.nl")]
        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.politicalmashup.nl")]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://dilipad.history.ac.uk")]
    public partial class subtopicSubtopicSubtopicSubtopic
    {

        private object[] itemsField;

        private string titleField;

        private string idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("subtopic", typeof(subtopicSubtopicSubtopicSubtopicSubtopic))]
        [System.Xml.Serialization.XmlElementAttribute("speech", typeof(speech), Namespace = "http://www.politicalmashup.nl")]
        [System.Xml.Serialization.XmlElementAttribute("stage-direction", typeof(stagedirection), Namespace = "http://www.politicalmashup.nl")]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.politicalmashup.nl")]
        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.politicalmashup.nl")]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://dilipad.history.ac.uk")]
    public partial class subtopicSubtopicSubtopicSubtopicSubtopic
    {

        private stagedirection stagedirectionField;

        private speech[] speechField;

        private string titleField;

        private string idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("stage-direction", Namespace = "http://www.politicalmashup.nl")]
        public stagedirection stagedirection
        {
            get
            {
                return this.stagedirectionField;
            }
            set
            {
                this.stagedirectionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("speech", Namespace = "http://www.politicalmashup.nl")]
        public speech[] speech
        {
            get
            {
                return this.speechField;
            }
            set
            {
                this.speechField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.politicalmashup.nl")]
        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.politicalmashup.nl")]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl")]
    [System.Xml.Serialization.XmlRootAttribute("stage-direction", Namespace = "http://www.politicalmashup.nl", IsNullable = false)]
    public partial class stagedirection
    {

        private stagedirectionP pField;

        private string idField;

        private string timeField;

        /// <remarks/>
        public stagedirectionP p
        {
            get
            {
                return this.pField;
            }
            set
            {
                this.pField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://dilipad.history.ac.uk")]
        public string time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl")]
    public partial class stagedirectionP
    {

        private string idField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.politicalmashup.nl", IsNullable = false)]
    public partial class speech
    {

        private speechP[] pField;

        private string speakerField;

        private string idField;

        private string speakeroldField;

        private string speakeridField;

        private string timeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("p")]
        public speechP[] p
        {
            get
            {
                return this.pField;
            }
            set
            {
                this.pField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string speaker
        {
            get
            {
                return this.speakerField;
            }
            set
            {
                this.speakerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("speaker-old", Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://dilipad.history.ac.uk")]
        public string speakerold
        {
            get
            {
                return this.speakeroldField;
            }
            set
            {
                this.speakeroldField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("speaker-id", Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://dilipad.history.ac.uk")]
        public string speakerid
        {
            get
            {
                return this.speakeridField;
            }
            set
            {
                this.speakeridField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://dilipad.history.ac.uk")]
        public string time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl")]
    public partial class speechP
    {

        private string idField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.politicalmashup.nl", IsNullable = false)]
    public partial class vote
    {

        private byte yeasField;

        private byte naysField;

        private string timeField;

        private string idField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public byte yeas
        {
            get
            {
                return this.yeasField;
            }
            set
            {
                this.yeasField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public byte nays
        {
            get
            {
                return this.naysField;
            }
            set
            {
                this.naysField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://dilipad.history.ac.uk")]
        public string time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl")]
    public partial class proceedingsTopicSceneSpeech
    {

        private proceedingsTopicSceneSpeechP[] pField;

        private string speakerField;

        private string idField;

        private string speakeroldField;

        private string speakeridField;

        private string timeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("p")]
        public proceedingsTopicSceneSpeechP[] p
        {
            get
            {
                return this.pField;
            }
            set
            {
                this.pField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string speaker
        {
            get
            {
                return this.speakerField;
            }
            set
            {
                this.speakerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("speaker-old", Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://dilipad.history.ac.uk")]
        public string speakerold
        {
            get
            {
                return this.speakeroldField;
            }
            set
            {
                this.speakeroldField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("speaker-id", Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://dilipad.history.ac.uk")]
        public string speakerid
        {
            get
            {
                return this.speakeridField;
            }
            set
            {
                this.speakeridField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://dilipad.history.ac.uk")]
        public string time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl")]
    public partial class proceedingsTopicSceneSpeechP
    {

        private string idField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl")]
    public partial class proceedingsTopicSceneStagedirection
    {

        private proceedingsTopicSceneStagedirectionP pField;

        private string idField;

        private string typeField;

        private ushort questionnumField;

        private bool questionnumFieldSpecified;

        private string questionauthorField;

        private string timeField;

        /// <remarks/>
        public proceedingsTopicSceneStagedirectionP p
        {
            get
            {
                return this.pField;
            }
            set
            {
                this.pField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("question-num", Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://dilipad.history.ac.uk")]
        public ushort questionnum
        {
            get
            {
                return this.questionnumField;
            }
            set
            {
                this.questionnumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool questionnumSpecified
        {
            get
            {
                return this.questionnumFieldSpecified;
            }
            set
            {
                this.questionnumFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("question-author", Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://dilipad.history.ac.uk")]
        public string questionauthor
        {
            get
            {
                return this.questionauthorField;
            }
            set
            {
                this.questionauthorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://dilipad.history.ac.uk")]
        public string time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl")]
    public partial class proceedingsTopicSceneStagedirectionP
    {

        private string idField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl")]
    public partial class proceedingsTopicSpeech
    {

        private proceedingsTopicSpeechP[] pField;

        private string speakerField;

        private string idField;

        private string speakeroldField;

        private string speakeridField;

        private string timeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("p")]
        public proceedingsTopicSpeechP[] p
        {
            get
            {
                return this.pField;
            }
            set
            {
                this.pField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string speaker
        {
            get
            {
                return this.speakerField;
            }
            set
            {
                this.speakerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("speaker-old", Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://dilipad.history.ac.uk")]
        public string speakerold
        {
            get
            {
                return this.speakeroldField;
            }
            set
            {
                this.speakeroldField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("speaker-id", Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://dilipad.history.ac.uk")]
        public string speakerid
        {
            get
            {
                return this.speakeridField;
            }
            set
            {
                this.speakeridField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://dilipad.history.ac.uk")]
        public string time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl")]
    public partial class proceedingsTopicSpeechP
    {

        private string idField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl")]
    public partial class proceedingsTopicStagedirection
    {

        private proceedingsTopicStagedirectionP pField;

        private string idField;

        private string typeField;

        private ushort questionnumField;

        private bool questionnumFieldSpecified;

        private string questionauthorField;

        /// <remarks/>
        public proceedingsTopicStagedirectionP p
        {
            get
            {
                return this.pField;
            }
            set
            {
                this.pField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("question-num", Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://dilipad.history.ac.uk")]
        public ushort questionnum
        {
            get
            {
                return this.questionnumField;
            }
            set
            {
                this.questionnumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool questionnumSpecified
        {
            get
            {
                return this.questionnumFieldSpecified;
            }
            set
            {
                this.questionnumFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("question-author", Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://dilipad.history.ac.uk")]
        public string questionauthor
        {
            get
            {
                return this.questionauthorField;
            }
            set
            {
                this.questionauthorField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.politicalmashup.nl")]
    public partial class proceedingsTopicStagedirectionP
    {

        private string idField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }


}
