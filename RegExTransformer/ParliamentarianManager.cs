﻿using NLPHelpers;
using RegExTransformer.MemberInfo;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace RegExTransformer
{
    public struct MiniMember
    {
        public static readonly MiniMember EMPTY = new MiniMember();

        string prefix;
        public string Prefix
        {
            get
            {
                return (prefix == null ? "" : prefix);
            }
            set
            {
                prefix = value.Trim();
            }
        }
        string firstname;
        public string FirstName
        {
            get
            {
                return (firstname == null ? "" : firstname);
            }
            set
            {
                firstname = value.Trim();
            }
        }
        string lastname;
        public string LastName
        {
            get
            {
                return (lastname == null ? "" : lastname);
            }
            set
            {
                lastname = value.Trim();
            }
        }
        string initials;
        public string Initials
        {
            get
            {
                return (initials == null ? "" : initials);
            }
            set
            {
                initials = value.Trim();
            }
        }
        string constit;
        public string Constituency
        {
            get
            {
                return (constit == null ? "" : constit);
            }
            set
            {
                constit = value.Trim();
            }
        }

        public int Length
        {
            get
            {
                return this.ToString().Length;
            }
        }

        public bool Speaker { get; set; }

        public override string ToString()
        {
            return (Prefix + " " + FirstName + " " + Initials + " " + LastName + "@" + Constituency).Trim();
        }

        public static bool operator ==(MiniMember a, MiniMember b)
        {
            return a.ToString() == b.ToString();
        }

        public static bool operator !=(MiniMember a, MiniMember b)
        {
            return a.ToString() != b.ToString();
        }

        public int PropCount
        {
            get
            {
                return (String.IsNullOrEmpty(Prefix) ? 0 : 1) +
                    (String.IsNullOrEmpty(FirstName) ? 0 : 1) +
                    (String.IsNullOrEmpty(Initials) ? 0 : 1) +
                    (String.IsNullOrEmpty(LastName) ? 0 : 1) +
                    (String.IsNullOrEmpty(Constituency) ? 0 : 1);
            }
        }
    }

    public class ParliamentarianManager
    {
        [Flags]
        public enum MatchBasis
        {
            OCRFlexibility = 1,
            None = 0,
            FirstName = 2,
            BiographyName = 4,
            LastName = 8,
            FlexibleMembership = 16,
            Membership = 32,
            Constituency = 64,
        }
        public static List<MemberFile> members;

        /// <summary>
        /// Loads all member files in the given directory into a cache.
        /// This must be called before MatchMember.
        /// </summary>
        /// <param name="dir">The directory to load member files from</param>
        public static void LoadAll(string dir)
        {
            members = new List<MemberFile>();
            string[] files = Directory.GetFiles(dir, "*.xml");
            XmlSerializer serializer = new XmlSerializer(typeof(MemberFile));
            foreach (var file in files)
            {
                using (var stream = File.Open(file, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    members.Add((MemberFile)serializer.Deserialize(stream));
                }
            }
            Log.LogMessage("Loaded " + members.Count + " member files");
        }

        static Dictionary<string, MemberFile> memberMatchCache = new Dictionary<string, MemberFile>();
        static Dictionary<string, MatchBasis> matchBasisCache = new Dictionary<string, MatchBasis>();
        /// <summary>
        /// Finds the Member File for the given member and session date.
        /// LoadAll must be called at least once before running this.
        /// </summary>
        /// <param name="member">The member whose file to find</param>
        /// <param name="date">The date of the session</param>
        /// <param name="dateRange">The date range within which this document should belong</param>
        public static MemberFile MatchMember(MiniMember member, DateTime date, DateRange dateRange, bool ocrflexible, MemberFile[] membersDuringDateRange)
        {
            if (members != null)
            {
                //Simple caching system
                string requestHash = member.ToString().GetHashCode() + " " + date.GetHashCode();
                if (memberMatchCache.ContainsKey(requestHash))
                {

                    return memberMatchCache[requestHash];
                }
                //This if for debugging, but can be used as a quick initial check.
                //It is not too reliable as a test since this last name check
                //can be overriden by biography in IsMemberMatch
                var name = membersDuringDateRange.Where(mem => mem.member.name.last.EndsWith(member.LastName));

                var matches = membersDuringDateRange.Select(mem =>
                {
                    MatchBasis b;
                    var match = IsMemberMatch(member, date, mem, out b, ocrflexible, dateRange, membersDuringDateRange);
                    return new { IsMatch = match, Member = mem, Basis = b };

                }).Where(match => match.IsMatch).ToArray();

                if (matches.Length == 0)
                    return null;

                var bestMatch = matches.OrderByDescending(match => (int)match.Basis - (match.Basis.HasFlag(MatchBasis.OCRFlexibility) ? 2 : 0)).First();
                //if (matches.Count() > 1)
                //{
                //    Debugger.Break();
                //}
                memberMatchCache[requestHash] = bestMatch.Member;
                matchBasisCache[requestHash] = bestMatch.Basis;


                return memberMatchCache[requestHash];
            }
            return null;
        }

        public static MatchBasis GetMatchBasis(MiniMember member, DateTime date)
        {
            string requestHash = member.ToString().GetHashCode() + " " + date.GetHashCode();
            if (matchBasisCache.ContainsKey(requestHash))
            {
                return matchBasisCache[requestHash];
            }
            return MatchBasis.None;
        }

        /// <summary>
        /// Matches the member to the file, during the given session date
        /// </summary>
        /// <param name="member">The member to match</param>
        /// <param name="date">The date of the session</param>
        /// <param name="file">The file containing member data</param>
        /// <returns>True if the file likely contains information about the given member</returns>
        private static bool IsMemberMatch(MiniMember member, DateTime date, MemberFile file, out MatchBasis matchBasis, bool ocrflexible, DateRange documentDateRange, MemberFile[] membersDuringDateRange)
        {

            matchBasis = MatchBasis.None;

            if (ocrflexible)
                matchBasis = MatchBasis.OCRFlexibility;

            if (member == MiniMember.EMPTY)
                return false;

            //Sometimes the initials come before the first name...for some reason
            if (member.LastName.Contains(" ") && String.IsNullOrEmpty(member.FirstName) && !member.LastName.Contains(". "))
            {
                member.FirstName = member.LastName.Split(' ').First();
                member.LastName = member.LastName.Split(' ').Skip(1).AggregateBy(" ");
            }

            var name = file.member.name;

            //All valid memberships for this member
            var memberships = file.member.memberships.Where(m =>
                m.period.AsDateRange().Contains(date) || m.period.AsDateRange().Intersects(documentDateRange));

            //TODO: If no memberships, abort?

            //string debugName = "King".ToLower();
            //if (name.last.RemoveDiacritics().ToLower().Contains("king") &&
            //      member.LastName.RemoveDiacritics().ToLower().Contains(debugName))
            //    Debugger.Break();


            //Does the constituency match the one in the file?
            //This is a required match, but can be overriden by a full name match
            bool constituencyMatch = memberships.Any(m =>
                (m.district != null && m.district.AlphaEquals(member.Constituency)) ||
                (m.name != null && m.name.AlphaEquals(member.Constituency, softmatch: true, ocrflexible: ocrflexible)));

            if (constituencyMatch)
                matchBasis |= MatchBasis.Constituency;

            //If we do not have access to a constituency, but the person was a member during this session
            //then we assume the constituency matches
            if (!constituencyMatch)
            {
                constituencyMatch = memberships.Count() > 0;// && String.IsNullOrEmpty(member.Constituency);
                if (constituencyMatch)
                    matchBasis |= MatchBasis.Membership;
            }


            if (!constituencyMatch && ocrflexible)
            {
                constituencyMatch = true; //We would only be looking at members during the date range
                if (constituencyMatch)
                    matchBasis |= MatchBasis.FlexibleMembership;
            }
            //Does the last name match? This is a required match.
            bool lastNameMatch = member.LastName.AlphaEquals(name.last, true, ocrflexible: ocrflexible);

            if (lastNameMatch)
                matchBasis |= MatchBasis.LastName;

            if (member.Speaker && memberships.Count() > 0)
            {
                if (memberships.Any(m => m.period.AsDateRange().Contains(date) && !String.IsNullOrEmpty(m.name) && Regex.IsMatch(m.name, "^" + member.Constituency)))
                {
                    constituencyMatch = true;
                    lastNameMatch = true;
                    matchBasis |= MatchBasis.Constituency;
                }
            }

            if (!lastNameMatch && ocrflexible)
            {
                string closest;
                if (NameHasDistinctClosest(member.LastName,
                    membersDuringDateRange.Select(mf => mf.member.name.last).Distinct().ToArray(),
                    out closest,
                    date.Year <= 1949 || member.LastName.Replace("Mac", "").Replace("Mc", "").ToUpper() == member.LastName.Replace("Mac", "").Replace("Mc", "")))
                {
                    member.LastName = closest;
                    lastNameMatch = closest == file.member.name.last;
                    if (lastNameMatch)
                        matchBasis |= MatchBasis.LastName | MatchBasis.OCRFlexibility;
                }
            }
            //Does the first name match? TODO: This should be imporved to include more permutations of initials
            bool firstNameMatch = member.FirstName.AlphaEquals(name.first.Contains(' ') ? name.first.Split(' ').First() : name.first) || //Direct first name match
                (!String.IsNullOrWhiteSpace(member.FirstName) && Regex.IsMatch(name.first, @"\b" + member.FirstName + @"\b", RegexOptions.IgnoreCase)) || //First name contains member name as word
                AsInitials(name.first).AlphaEquals(member.Initials) || //Single initial match
                AsInitials(name.first + " " + name.last).AlphaEquals(member.Initials, softmatch: true) || //Multiple initials (soft) match
                (member.FirstName != "" &&
                    AsInitials(name.first).AlphaEquals(member.FirstName[0] + ". " + member.Initials)); //Multiple initials (soft) match including first name

            if (firstNameMatch)
                matchBasis |= MatchBasis.FirstName;

            //Does the name in the biography match? FIXME: Maybe this should be given less 
            //credit in case it matches a family member mentioned in the bio
            bool bioNameMatch = false;

            if (!"Speaker Member Members".Contains(member.LastName) && !member.Speaker && file.member.biographies != null && file.member.biographies.biography != null && file.member.biographies.biography.Value != null)
            {
                //Get the words in the biography and extract the first instance of a full name with this
                //member's last name

                string[] bioWords = Regex.Split(file.member.biographies.biography.Value.Substring(0, Math.Min(140, file.member.biographies.biography.Value.Length)), @"\b")
                    .Where(word => !String.IsNullOrEmpty(word.Trim())).ToArray();

                int index = -1;
                for (int i = 0; i < 10; i++)
                {
                    if (bioWords[i].AlphaEquals(member.LastName))
                    {
                        index = i;
                        break;
                    }
                }
                //a b c
                if (index > 0)
                {
                    string[] bioName = bioWords
                        .SkipWhile(word => !Char.IsUpper(word[0]))
                        .TakeWhile((word, i) => i <= index).ToArray();

                    string fullBioName = bioName.AggregateBy(" ");
                    bioNameMatch = fullBioName.AlphaEquals(member.FirstName + member.Initials + member.LastName) || //FIXME: Is this necessary given the check below?
                        (member.FirstName.AlphaEquals(bioName.First())
                            && member.LastName.AlphaEquals(bioName.Last()));
                    if (bioNameMatch)
                        matchBasis |= MatchBasis.BiographyName;
                    //Biography match overrides first name match (i.e. biography match automatically means 
                    //full name match). FIXME: See decleration of bioNameMatch.
                    lastNameMatch = lastNameMatch || bioNameMatch;
                    firstNameMatch = firstNameMatch || bioNameMatch;
                }




            }

            //A match requires a last name match and either a constituency match or a firstname match
            //along with a guarantee that the member was present in parliament
            return lastNameMatch && (constituencyMatch || (memberships.Count() > 0 && firstNameMatch));
        }

        public static MemberFile[] GetMembersForDateRange(DateRange dr)
        {
            return members.Where(member =>
                member.member.memberships
                .Select(msship => msship.period.AsDateRange())
                .Any(msp => msp.Intersects(dr))).ToArray();
        }

        struct NHDC_in
        {
            public bool ignoreCase;
            public string name;
            public int candidatesHash;
            public void SetCandidates(string[] candidates)
            {
                candidatesHash = candidates.AggregateBy("|").GetHashCode();
            }
        }

        struct NHDC_out
        {
            public string closest;
            public bool value;
        }

        static Dictionary<NHDC_in, NHDC_out> NHDCCache = new Dictionary<NHDC_in, NHDC_out>();
        public static bool NameHasDistinctClosest(string name, string[] candidates, out string closest, bool ignoreCase)
        {
            NHDC_in input = new NHDC_in()
            {
                name = ignoreCase ? name.ToLower() : name,
                ignoreCase = ignoreCase
            };
            input.SetCandidates(candidates);

            NHDC_out output;
            if (NHDCCache.ContainsKey(input))
            {
                output = NHDCCache[input];
            }
            else
            {
                var levs = candidates.Select(candidate => new Levenshtein(name, candidate, ignoreCase));
                var scores = levs.Select(lev =>
                    {
                        lev.ComputeMatrix();
                        return new { Str = lev.Target, Score = lev.GetScore() };
                    }).ToList();
                scores.Sort((x, y) => x.Score.CompareTo(y.Score));
                int minScore = scores.First().Score;
                if (minScore > Math.Max(name.Length / 3, 2))
                {
                    output = new NHDC_out()
                    {
                        closest = name,
                        value = false
                    };
                }
                else
                {
                    output = new NHDC_out()
                    {
                        closest = scores.First().Str,
                        value = scores.TakeWhile(sc => sc.Score == minScore).Count() == 1
                    };
                }
               
            }
            NHDCCache[input] = output;
            closest = output.closest;
            return output.value;
        }

        public struct DateRange
        {
            public DateTime Start { get; set; }
            public DateTime End { get; set; }
            public bool invalid;
            public int DaySpan
            {
                get
                {
                    return (int)(End - Start).TotalDays;
                }
            }

            public DateRange(DateTime start, DateTime end)
                : this()
            {
                invalid = false;
                Start = start;
                End = end;
            }

            public bool Contains(DateTime dateTime)
            {
                return Start <= dateTime && dateTime <= End;
            }



            public override string ToString()
            {
                return Start.ToString("MMMM_dd_yyyy") + " - " + End.ToString("MMMM_dd_yyyy");
            }

            public static List<DateRange> Merge(DateRange[] lst)
            {
                for (int i = 1; i < lst.Length; i++)
                {
                    if (lst[i - 1].End.AddDays(1) == lst[i].Start)
                    {
                        lst[i - 1].invalid = true;
                        lst[i].Start = lst[i - 1].Start;
                    }
                }
                return lst.Where(d => !d.invalid).ToList();
            }

            public bool Intersects(DateRange dr)
            {
                return ((this.End >= dr.Start && this.End <= dr.End) || (this.Start <= dr.End && this.Start >= dr.Start)) ||
                       ((dr.End >= this.Start && dr.End <= this.End) || (dr.Start <= this.End && dr.Start >= this.Start));
            }

            public bool Contains(DateRange dr)
            {
                return Start <= dr.Start && End >= dr.End;
            }
        }

        public static DateRange[] GetMembershipsForLastName(string lastName)
        {
            var matchingLastNames = members.Where(mem => mem.member.name.last.AlphaEquals(lastName, true));
            if (matchingLastNames.Count() == 0)
                return null;
            return matchingLastNames.SelectMany(mem =>
                mem.member.memberships
                .Select(membership => membership.period.AsDateRange()))
            .ToArray();
        }

        public static DateRange GetCommonDateRange(MiniMember[] members, DateTime defaultDt, out DateRange maxRange)
        {
            var lastNames = members.Select(member =>
                {
                    if (member.LastName.Contains(" "))
                    {
                        return member.LastName.Split(' ').Skip(1).AggregateBy(" ");
                    }
                    return member.LastName;
                }).Distinct();

            var linkedMemberships = lastNames.Select(ln => new { lastName = ln, mems = GetMembershipsForLastName(ln) }).Where(mem => mem.mems != null).ToArray();
            var orderedMemberships = linkedMemberships.OrderBy(x => members.Count(member => member.LastName.EndsWith(x.lastName)));
            var allMemberships = orderedMemberships.Select(x => x.mems).ToArray();


            if (allMemberships.Length == 0)
            {

                return maxRange = new DateRange(DateTime.MinValue, DateTime.Now);
            }
            maxRange = new DateRange(allMemberships.SelectMany(dr => dr).Select(d => d.Start).Min(), allMemberships.SelectMany(dr => dr).Select(d => d.Start).Max());
            IEnumerable<DateRange> commonRanges = allMemberships.SelectMany(dt => dt).Distinct();
            //int i = 0;
            //foreach (var item in allMemberships)
            //{
            //    var next = commonRanges.Where(range => item.Any(it => (it.Start <= range.Start && it.End >= range.End) || (range.Start <= it.Start && range.End >= it.End)));

            //    if (next.Count() == 0)
            //    {
            //        continue;
            //    }
            //    i++;
            //    commonRanges = next;
            //}

            if (commonRanges.Count() == 0)
            {
                Log.LogError(defaultDt.ToString("MMM_dd_yyyy"), "Could not infer date range for document.");
                return maxRange;
            }

            var results = commonRanges.Select(item =>
                new
                {
                    range = item,
                    count = allMemberships.Count(drarr => drarr.Any(dr =>
                        dr.Contains(item) || item.Contains(dr)
                    ))
                }).OrderByDescending(x => x.count).ToArray();

            int max = results[0].count;
            results = results.TakeWhile(x => x.count == max).ToArray();

            DateRange safest = new DateRange(results.Min(dr => dr.range.Start), results.Max(dr => dr.range.End));

            var narrow = results.Where(dr => dr.range.Contains(defaultDt));
            if (narrow.Count() != 0)
            {
                DateRange narrowest = new DateRange(narrow.Min(dr => dr.range.Start), narrow.Max(dr => dr.range.End));
                Log.LogMessage("Narrow date range is " + narrowest, defaultDt.ToString("MMM_dd_yyyy"), ConsoleColor.Cyan);
            }

            return safest;


        }

        /// <summary>
        /// Converts a name or string of names into initials.
        /// For example "Robert" Becomes "R." and "Robert William"
        /// becomes "R. W."
        /// </summary>
        /// <param name="name">The name to convert</param>
        private static string AsInitials(string name)
        {
            return (name.Contains(" ") ?
                name.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                .Select(n => n[0] + ".")
                .AggregateBy(" ")
                :
                name[0] + ".");
        }
    }
}
