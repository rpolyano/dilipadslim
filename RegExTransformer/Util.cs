﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace RegExTransformer
{
    class Util
    {
        public enum NormalizationMode
        {
            /// <summary>
            /// Remove unwanted parts of names like colons and titles
            /// </summary>
            Name,

            /// <summary>
            /// Removes/replaces unicode characters
            /// </summary>
            Unicode,

            /// <summary>
            /// Removes all non XML compatible characters
            /// </summary>
            XML,

            /// <summary>
            /// Changes time from OCRed [DOT] (hhmm) format to hh:mm:ss format
            /// </summary>
            Time,

            /// <summary>
            /// Does some extra things XML normalization does not, like replacing quotes
            /// </summary>
            Attributes
        }

        public static string[] TITLES = new string[] {"M.","Mr.","Mr,","Miss","The Hon.","The hon.","Mrs.","The Right Hon.","The right Hon.","Right Hon.","Ms.","Hon."};
        public const char UNICODE_DOT = '\u2022';
        public const char UNICODE_SQUARE = '\u25A0';
        public const char UNICODE_DIAMOND = '\u2666';
        public const char UNICODE_OPEN_SINGLE_QUOTE = '\u2018';
        public const char UNICODE_CLOSE_SINGLE_QUOTE = '\u2019';
        public const char UNICODE_LONG_DASH = '\u2014';
        public const char UNICODE_SHORT_DASH = '\u2013';
        public const char UNICODE_CLOSE_DOUBLE_QUOTE = '\u201D';
        public const char UNICODE_OPEN_DOUBLE_QUOTE = '\u201C';
        public const char UNICODE_DOUBLE_ARROW = '\u00BB';
        public const char UNICODE_FILLED_STAR = '\u2605';
        static Regex TIME_CLEANER_REGEX1 = new Regex(@"(?:\[DOT\])\s?\(([0-9HIl]{1,2})([0-9HIl]{2})\s?\)", RegexOptions.Compiled);
        static Regex TIME_CLEANER_REGEX2 = new Regex(@"\[DOT\][ (]+([0-9HIl]+):([0-9HIl]+)\s?(?:(a|p)\.?m\.?)?\s?\)?", RegexOptions.Compiled);

        /// <summary>
        /// Cleans given text according to given mode
        /// </summary>
        /// <param name="text">The text to clean</param>
        /// <param name="mode">The mode to use. See <see cref="Util.NormalizationMode"/></param>
        /// <returns></returns>
        public static string NormalizeText(string text, NormalizationMode mode)
        {
            switch (mode)
            {
                case NormalizationMode.Name:
                    if (text.Contains("Acting"))
                        return text.Substring(0, text.Length - 1);
                    if (text.Contains("("))
                        text = text.Split('(')[0];
                    text = text.Replace(":", "").Replace("moved", "");
                    foreach (var title in TITLES)
                    {
                        if (text.StartsWith(title))
                        {
                            text = text.Replace(title, "");
                        }
                    }
                    return text.Trim().Split(' ').AggregateBy(" ");
                case NormalizationMode.Unicode:
                    text = text.Replace(UNICODE_DOT.ToString(), "[DOT]");
                    text = text.Replace(UNICODE_SQUARE.ToString(), "*"); //TODO: Figure out if this is a good idea
                    text = text.Replace(UNICODE_DIAMOND.ToString(), ""); //FIXME: If we decide to use these as anchors this needs to not happen
                    text = text.Replace(UNICODE_OPEN_SINGLE_QUOTE.ToString(), "'");
                    text = text.Replace(UNICODE_CLOSE_SINGLE_QUOTE.ToString(), "'");
                    text = text.Replace(UNICODE_OPEN_DOUBLE_QUOTE.ToString(), "\"");
                    text = text.Replace(UNICODE_CLOSE_DOUBLE_QUOTE.ToString(), "\"");
                    text = text.Replace(UNICODE_LONG_DASH.ToString(), "-");
                    text = text.Replace(UNICODE_SHORT_DASH.ToString(), "-");
                    text = text.Replace(UNICODE_DOUBLE_ARROW.ToString(), "*");
                    text = text.Replace(UNICODE_FILLED_STAR.ToString(), "*");
                    return text;
                case NormalizationMode.XML:
                    text = new string(text
                        .Where(chr => XmlConvert.IsXmlChar(chr))
                        .ToArray());

                    text = text.Replace("&", "&amp;");
                    text = text.Replace("<", "&lt;");
                    text = text.Replace("<", "&gt;");
                    
                    return text;
                case NormalizationMode.Time:
                    if (text.Contains(":"))
                    {
                        return TIME_CLEANER_REGEX2.Replace(text, "$1:$2:00$3m").Replace("'", "");
                    }
                    return TIME_CLEANER_REGEX1.Replace(text, "$1:$2:00").Replace("'","");
                case NormalizationMode.Attributes:
                    text = text.Replace("\"", "&quot;");
                    return text;
                default:
                    return text;
            }
        }

        
    }
}
