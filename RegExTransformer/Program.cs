﻿using NLPHelpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace RegExTransformer
{
    struct ProcessReport
    {
        public TimeSpan runTime;
        public int topicsFixed;
        public int totalMembers;
        public int membersFixed;
        public int membersUnlinked;

        public static ProcessReport Parse(string[] logLines)
        {
            logLines = logLines.Reverse().Take(7).Reverse().ToArray();
            
            ProcessReport rep = new ProcessReport();

            var runTimeMatch = Regex.Match(logLines[0], "([0-9]+)[^0-9]+([0-9]+)[^0-9]+([0-9]+)");
            rep.runTime = new TimeSpan(Int32.Parse(runTimeMatch.Groups[1].Value), Int32.Parse(runTimeMatch.Groups[2].Value), Int32.Parse(runTimeMatch.Groups[3].Value));
            
            var fixedTopicMatch = Regex.Match(logLines[1], "([0-9]+)");
            rep.topicsFixed = Int32.Parse(fixedTopicMatch.Groups[1].Value);
            
            var totalMembersMatch = Regex.Match(logLines[2], "([0-9]+)");
            rep.totalMembers = Int32.Parse(totalMembersMatch.Groups[1].Value);

            var unlinkedMembersMatch = Regex.Match(logLines[3], "([0-9]+)");
            rep.membersUnlinked = Int32.Parse(unlinkedMembersMatch.Groups[1].Value);

            var membersFixedMatch = Regex.Match(logLines[4], "([0-9]+)");
            rep.membersFixed = Int32.Parse(membersFixedMatch.Groups[1].Value);

            return rep;
        }
    }
    class Program
    {
        const string ENVVAR_PROCESS_NUM = "DPPRNUM";
        const string ENVVAR_SAMPLE_FOLDER = "DPSAMPFLDR";
        const string ENVVAR_FILE_NAMES = "DPPROCFILES";
        const string ENVVAR_STAGES = "DPSTAGES";
        static bool debugMode = false;
        static void Main(string[] args)
        {
            //string[][] test1 = new string[][]
            //{
            //    new string[]{"A", "B", "C"},
            //    new string[]{"D", "E"},
            //    new string[]{"F", "G", "H"},
            //    new string[]{"G", "F", "H"},
            //    new string[]{"K", "L", "M"},
            //};
            //string[][] test2 = new string[][]
            //{
            //    new string[]{"B", "C"},
            //    new string[]{"D", "E"},
            //    new string[]{"I", "J"},
            //    new string[]{"F", "G"},
            //};
            //var common = GetCommonElements(test1, test2);

            //RegexPattern dummy = new RegexPattern()
            //{
            //    constants = null,
            //    feature=RegexFeature.Topic,
            //    pattern="abcpattern",
            //    relevancyEnd=DateTime.Now.AddYears(-1000),
            //    relevancyStart=DateTime.Now.AddYears(-1050),
            //};
            //var ser = new XmlSerializer(typeof(RegexPattern));
            //ser.Serialize(File.Open("regex/dummy.xml", FileMode.Create), dummy);
            //string a = "Mcllraiih".AlphaProcessString(true, true, true);
            if (args.Length == 0)
            {
                Log.messageLogPath = "logs/hub.log";
                Log.errorLogPath = "logs/hub-errors.log";
                bool prevSettings;
                Console.WriteLine("Running as Hub.");
                Console.WriteLine("Drag file/folder or nothing to use previous settings:");
                string path = Console.ReadLine();
                path = path.Trim('"');
                prevSettings = String.IsNullOrEmpty(path);

                string stageLine = "";
                String[] files = new string[0];
                int numThreads = Directory.GetFiles("logs", "output*").Length;
                
                if (!prevSettings)
                {
                    bool isDir = (File.GetAttributes(path) & FileAttributes.Directory) == FileAttributes.Directory;

                    files = isDir ? Directory.GetFiles(path, "*.txt", SearchOption.AllDirectories) : new string[] { path };

                    Console.WriteLine("Stage numbers:");
                    stageLine = Console.ReadLine();

                    Console.WriteLine("Number of processes (or nothing for {0}): ", Environment.ProcessorCount);
                    
                    if (!Int32.TryParse(Console.ReadLine(), out numThreads))
                    {
                        numThreads = Environment.ProcessorCount;
                    }
                    if (numThreads < 0)
                    {
                        debugMode = true;
                        Console.WriteLine("Running in debug mode");
                    }
                }
                
                
                if (debugMode)
                {
                    bool stage1 = stageLine.Contains("1");
                    bool stage2 = stageLine.Contains("2");
                    bool stage3 = stageLine.Contains("3");
                    bool stage4 = stageLine.Contains("4");
                    bool stage4_1 = stageLine.Contains("5");

                    if (stage4 || stage4_1)
                        ParliamentarianManager.LoadAll("members");

                    TransformerStage2.hierarchies = Directory.GetFiles("hierarchies", "*.xml");
                    RegexManager.Load(Path.Combine(Environment.CurrentDirectory, "regex"));

                    string dataPath = Environment.CurrentDirectory.TrimEnd('/', '\\') + "/../../../SanfordPOSTagger";

                    if (stage1)
                        TopicValidator.Init(dataPath, dataPath + "/models");

                    
                    ProcessFiles(files, stage1, stage2, stage3, stage4, stage4_1, dataPath);
                    Console.WriteLine("Done");
                }
                else
                {
                    int numFilesPerThread = files.Length / numThreads;
                    Process[] processes = new Process[numThreads];
                    for (int thread = 0; thread < numThreads; thread++)
                    {

                        var psi = new ProcessStartInfo()
                        { //String.Format("/num {0} /dir {1} /files {2}", thread, Path.GetDirectoryName(threadFiles[0]), threadFiles.Select(fle=>Path.GetFileName(fle)).AggregateBy("|"))
                            FileName = "RegExTransformer.exe",
                            //WorkingDirectory = Path.GetDirectoryName(threadFiles[0]),
                            UseShellExecute = true,
                            Arguments = "/num:" + thread,
                            WindowStyle = ProcessWindowStyle.Maximized
                        };
                        //psi.EnvironmentVariables.Add(ENVVAR_PROCESS_NUM, thread.ToString());
                        //psi.EnvironmentVariables.Add(ENVVAR_SAMPLE_FOLDER, Path.GetDirectoryName(threadFiles[0]));
                        //psi.EnvironmentVariables.Add(ENVVAR_FILE_NAMES, threadFiles.Select(fle => Path.GetFileName(fle)).AggregateBy(";"));
                        //psi.EnvironmentVariables.Add(ENVVAR_STAGES, stageLine);
                        if (!prevSettings)
                        {
                            string[] threadFiles;
                            if (thread == numThreads - 1)
                            {
                                threadFiles = files.Skip(thread * numFilesPerThread).ToArray();
                            }
                            else
                            {
                                threadFiles = files.Skip(thread * numFilesPerThread).Take(numFilesPerThread).ToArray();
                            }

                            if (File.Exists("inp-" + thread + ".txt"))
                            {
                                File.Delete("inp-" + thread + ".txt");
                            }
                            File.WriteAllLines("inp-" + thread + ".txt", new string[]
                            {
                                ENVVAR_PROCESS_NUM + "=" + thread.ToString(),
                                ENVVAR_SAMPLE_FOLDER + "=" +  Path.GetDirectoryName(threadFiles[0]),
                                ENVVAR_FILE_NAMES + "=" +  threadFiles.Select(fle => Path.GetFileName(fle)).AggregateBy(";"),
                                ENVVAR_STAGES + "=" +  stageLine
                            });
                        }
                        Process pr = Process.Start(psi);
                        pr.ProcessorAffinity = (IntPtr)(1 << (thread % Environment.ProcessorCount));
                        processes[thread] = pr;
                    }
                    Console.WriteLine("Waiting for all spawned processes to terminate...");
                    for (int i = 0; i < processes.Length; i++)
                    {
                        processes[i].WaitForExit();
                        processes[i].Close();
                    }
                    Thread.Sleep(5000);
                    Console.WriteLine("Collecting output...");
                    ProcessReport[] reports = new ProcessReport[processes.Length];
                    for (int i = 0; i < processes.Length; i++)
                    {
                        string[] logLines = File.ReadAllLines("logs/output-" + i + ".log");

                        reports[i] = ProcessReport.Parse(logLines);
                    }

                    var maxRunTime = reports.Max(rp => rp.runTime);
                    Log.LogMessage("Total run time {0} hours, {1} minutes, {2} seconds", "INFO", ConsoleColor.Cyan, maxRunTime.Hours, maxRunTime.Minutes, maxRunTime.Seconds);
                    Log.LogMessage("Fixed " + reports.Sum(rp => rp.topicsFixed) + " topics", "INFO", ConsoleColor.Cyan);
                    Log.LogMessage("Total Members: " + reports.Sum(rp => rp.totalMembers));
                    Log.LogMessage("Unlinked Members: " + reports.Sum(rp => rp.membersUnlinked));
                    Log.LogMessage("Members Linked With OCR Flexibility: " + reports.Sum(rp => rp.membersFixed));
                    Log.LogMessage("Percent unlinked: " + (100f * reports.Sum(rp => rp.membersUnlinked) / (reports.Sum(rp => rp.totalMembers) + 1)));
                    Log.LogMessage("Percent Fixed: " + (100f * reports.Sum(rp => rp.membersFixed) / (reports.Sum(rp => rp.totalMembers) + 1)));
                }
                Console.ReadLine();
            }
            else
            {
                DateTime startTime = DateTime.Now;

                int processNum = Int32.Parse(args[0].Split(':')[1]);
                Dictionary<string, string> settings = new Dictionary<string, string>();
                settings = File.ReadAllLines("inp-" + processNum + ".txt").ToDictionary(ln => ln.Split('=')[0], ln => ln.Split('=')[1]);
                string stageLine = settings[ENVVAR_STAGES];
                bool stage1 = stageLine.Contains("1");
                bool stage2 = stageLine.Contains("2");
                bool stage3 = stageLine.Contains("3");
                bool stage4 = stageLine.Contains("4");
                bool stage4_1 = stageLine.Contains("5");

                string folder = settings[ENVVAR_SAMPLE_FOLDER];

                string[] files = settings[ENVVAR_FILE_NAMES].Split(';').Select(file => folder + "/" + file).ToArray();
                Console.WriteLine("Folder: " + folder);
                Console.WriteLine("Stages: " + stageLine);
                Console.WriteLine("Num: " + processNum);
                Console.WriteLine("Files: " + files.Length);
                Log.messageLogPath = "logs/output-" + processNum + ".log";
                Log.errorLogPath = "logs/error-" + processNum + ".log";
                //Console.WriteLine("Confirm?");
                //Console.ReadLine();
                if (stage4 || stage4_1)
                    ParliamentarianManager.LoadAll("members");
                TransformerStage2.hierarchies = Directory.GetFiles("hierarchies", "*.xml");
                RegexManager.Load(Path.Combine(Environment.CurrentDirectory, "regex"));
                
                string dataPath = Environment.CurrentDirectory.TrimEnd('/', '\\') + "/../../../SanfordPOSTagger";
                
                if (stage1)
                    TopicValidator.Init(dataPath, dataPath + "/models");

                Log.LogError("+=+=+=+=+=", "Starting run for " + DateTime.Now.ToShortDateString() + ", " + DateTime.Now.ToShortTimeString(), true);
                ProcessFiles(files, stage1, stage2, stage3, stage4, stage4_1, dataPath);

                var runTime = DateTime.Now - startTime;
                Log.LogMessage("Total run time {0} hours, {1} minutes, {2} seconds", "INFO", ConsoleColor.Cyan, runTime.Hours, runTime.Minutes, runTime.Seconds);
                Log.LogMessage("Fixed " + TransformerStage1.NUM_TOPICS_FIXED + " topics", "INFO", ConsoleColor.Cyan);
                Log.LogMessage("Total Members: " + TransformerStage4.TOTAL_COUNT);
                Log.LogMessage("Unlinked Members: " + TransformerStage4.UNLINKED_COUNT);
                Log.LogMessage("Members Linked With OCR Flexibility: " + TransformerStage4.FIXED_COUNT);
                Log.LogMessage("Percent unlinked: " + (100 * TransformerStage4.UNLINKED_COUNT / (TransformerStage4.TOTAL_COUNT + 1)));
                Log.LogMessage("Percent Fixed: " + (100 * TransformerStage4.FIXED_COUNT / (TransformerStage4.TOTAL_COUNT + 1)));

            }




            

//#if !X_PLATFORM_TOPIC_VALIDATION
            
//#endif
            
//            Console.WriteLine("Drag file/folder:");
//            string path = Console.ReadLine();
//            path = path.Trim('"');

//            bool isDir = (File.GetAttributes(path) & FileAttributes.Directory) == FileAttributes.Directory;

//            String[] files = isDir ? Directory.GetFiles(path, "*.txt", SearchOption.AllDirectories) : new string[] { path };

//            Console.WriteLine("Stage numbers:");
//            string line = Console.ReadLine();
//            bool stage1 = line.Contains("1");
//            bool stage2 = line.Contains("2");
//            bool stage3 = line.Contains("3");
//            bool stage4 = line.Contains("4");
//            bool stage4_1 = line.Contains("5");
//            Console.WriteLine("Number of threads (or nothing for {0}): ", Environment.ProcessorCount);
//            int numThreads;
//            if (!Int32.TryParse(Console.ReadLine(), out numThreads))
//            {
//                numThreads = Environment.ProcessorCount;
//            }
            
//            Console.WriteLine("Running " + numThreads + " processing threads.");

//            int numFilesPerThread = files.Length / numThreads;

//            Thread[] threads = new Thread[numThreads];
//            DateTime startTime = DateTime.Now;
//            for (int thread = 0; thread < numThreads; thread++)
//            {
//                string[] threadFiles;
//                if (thread == numThreads - 1)
//                {
//                    threadFiles = files.Skip(thread * numFilesPerThread).ToArray();
//                }
//                else
//                {
//                    threadFiles = files.Skip(thread * numFilesPerThread).Take(numFilesPerThread).ToArray();
//                }
//                threads[thread] = new Thread(() =>
//                {
//                    ProcessFiles(threadFiles, stage1, stage2, stage3, stage4, stage4_1, dataPath);

//                }
//                );
//                threads[thread].Start();
//                //TODO use threadpool
//            }

//            do
//            {
//                Thread.Sleep(1000);
//            } while (threads.Any(thr => thr.ThreadState == System.Threading.ThreadState.Running));
//            var runTime = DateTime.Now - startTime;
//            Log.LogMessage("Total run time {0} hours, {1} minutes, {2} seconds", "INFO", ConsoleColor.Cyan, runTime.Hours, runTime.Minutes, runTime.Seconds);
//            Log.LogMessage("Fixed " + TransformerStage1.NUM_TOPICS_FIXED + " topics", "INFO", ConsoleColor.Cyan);
//            Log.LogMessage("Total Members: " + TransformerStage4.TOTAL_COUNT);
//            Log.LogMessage("Unlinked Members: " + TransformerStage4.UNLINKED_COUNT);
//            Log.LogMessage("Members Linked With OCR Flexibility: " + TransformerStage4.FIXED_COUNT);
//            Log.LogMessage("Percent unlinked: " + (100 * TransformerStage4.UNLINKED_COUNT / (TransformerStage4.TOTAL_COUNT + 1)));
//            Log.LogMessage("Percent Fixed: " + (100 * TransformerStage4.FIXED_COUNT / (TransformerStage4.TOTAL_COUNT + 1)));
//            Console.WriteLine("Finished Transform. Press X to exit, or anything else to do Extra");
            
//            if (Console.ReadKey().Key == ConsoleKey.X)
//                return;
            
//            Extra(files);
            
//            Console.WriteLine("Finished. Press any key to exit...");
//            Console.ReadKey();

        }

        static void ProcessFiles(string[] files, bool stage1, bool stage2, bool stage3, bool stage4, bool stage4_1, string topicValidatorDataPath)
        {
#if X_PLATFORM_TOPIC_VALIDATION
            CrossPlatformTopicValidator.Init(topicValidatorDataPath, topicValidatorDataPath + "/models");
#endif
            bool skipExisting = !debugMode;
            foreach (string file in files)
            {
                //try
                {
                    string fileName = Path.GetFileNameWithoutExtension(file);
                    Console.WriteLine("Working on {0}...", fileName);
                    int year = DateTime.Parse(fileName.Replace('_', ' ')).Year;
                    IRegexContainer regex = GetContainer(year);
                    Log.LogMessage("Year is {0}", fileName, ConsoleColor.Blue, year);
                    if (stage1)
                    {
                        Log.LogMessage("Running Stage 1", fileName);
                        if (skipExisting && File.Exists("stage_1/" + year + "." + Path.GetFileNameWithoutExtension(file) + ".xml"))
                        {
                            Console.WriteLine("Skipping.");
                        }
                        else
                        {
                            String text = File.ReadAllText(file);
                            TransformerStage1 tr = new TransformerStage1(regex, text, Path.GetFileNameWithoutExtension(file));
                            string newtext = tr.Transform(true);
                            try
                            {
                                Console.WriteLine("Making sure XML is parsable...", fileName);
                                XDocument doc = XDocument.Parse(newtext, LoadOptions.PreserveWhitespace);
                                Log.LogMessage("XML is parsable.", fileName);
                                Stage1Report(doc, fileName);
                                if (!Directory.Exists("stage_1"))
                                {
                                    Directory.CreateDirectory("stage_1");
                                }
                                doc.Save("stage_1/" + year + "." + Path.GetFileNameWithoutExtension(file) + ".xml");
                            }
                            catch (XmlException ex)
                            {
                                Log.LogError(file, ex.Message, false);
                                Log.LogMessage("Retrying without votes", file, ConsoleColor.DarkRed);
                                tr = new TransformerStage1(regex, text, Path.GetFileNameWithoutExtension(file));
                                newtext = tr.Transform(false);
                                try
                                {
                                    Console.WriteLine("Making sure XML is parsable...", fileName);
                                    XDocument doc = XDocument.Parse(newtext, LoadOptions.PreserveWhitespace);
                                    Log.LogMessage("XML is parsable.", fileName);
                                    Stage1Report(doc, fileName);
                                    if (!Directory.Exists("stage_1"))
                                    {
                                        Directory.CreateDirectory("stage_1");
                                    }
                                    doc.Save("stage_1/" + year + "." + Path.GetFileNameWithoutExtension(file) + ".xml");
                                }
                                catch (XmlException ex2)
                                {
                                    Log.LogError(file, ex.Message, true);
                                    continue;
                                }
                            }
                        }
                        

                    }
                    if (stage2)
                    {
                        if (skipExisting && File.Exists("stage_2/" + year + "." + Path.GetFileNameWithoutExtension(file) + ".xml"))
                        {
                            Console.WriteLine("Skipping.");
                        }
                        else
                        {
                            Log.LogMessage("Running Stage 2", fileName);
                            XDocument stage1Doc = XDocument.Load("stage_1/" + year + "." + Path.GetFileNameWithoutExtension(file) + ".xml", LoadOptions.SetLineInfo | LoadOptions.SetBaseUri);
                            TransformerStage2 tr = new TransformerStage2(stage1Doc, year);
                            XDocument stage2Doc = tr.Transform();
                            if (!Directory.Exists("stage_2"))
                            {
                                Directory.CreateDirectory("stage_2");
                            }
                            stage2Doc.Save("stage_2/" + year + "." + Path.GetFileNameWithoutExtension(file) + ".xml");
                        }
                    }
                    if (stage3)
                    {
                        if (skipExisting && File.Exists("stage_3/" + year + "." + Path.GetFileNameWithoutExtension(file) + ".xml"))
                        {
                            Console.WriteLine("Skipping.");
                        }
                        else
                        {
                            Log.LogMessage("Running Stage 3", fileName);
                            XDocument stage2Doc = XDocument.Load("stage_2/" + year + "." + Path.GetFileNameWithoutExtension(file) + ".xml", LoadOptions.SetLineInfo | LoadOptions.SetBaseUri);
                            TransformerStage3 tr = new TransformerStage3(stage2Doc, Path.GetFileNameWithoutExtension(file));
                            XDocument stage3Doc = tr.Transform();
                            if (!Directory.Exists("stage_3"))
                            {
                                Directory.CreateDirectory("stage_3");
                            }
                            stage3Doc.Save("stage_3/" + year + "." + Path.GetFileNameWithoutExtension(file) + ".xml");
                        }
                    }
                    if (stage4)
                    {
                        if (skipExisting && File.Exists("stage_4/" + year + "." + Path.GetFileNameWithoutExtension(file) + ".xml"))
                        {
                            Console.WriteLine("Skipping.");
                        }
                        else
                        {
                            Log.LogMessage("Running Stage 4", fileName);
                            XDocument stage3Doc = XDocument.Load("stage_3/" + year + "." + Path.GetFileNameWithoutExtension(file) + ".xml", LoadOptions.SetLineInfo | LoadOptions.SetBaseUri);
                            TransformerStage4 tr = new TransformerStage4(stage3Doc, Path.GetFileNameWithoutExtension(file));
                            string report = tr.Transform();
                            if (!Directory.Exists("stage_4"))
                            {
                                Directory.CreateDirectory("stage_4");
                            }
                            File.WriteAllText("stage_4/" + year + "." + Path.GetFileNameWithoutExtension(file) + ".csv", report);
                        }
                    }
                    if (stage4_1)
                    {
                        Log.LogMessage("Running Stage 4.1", fileName);
                        TransformerStage4_1 tr = new TransformerStage4_1("stage_4");
                        tr.Transform();
                    }
                }
                //catch (Exception ex)
                {
                //    Log.LogError(file, "Uncaught " + ex.GetType().Name + ": " + ex.Message + "\n\n" + ex.StackTrace, true);
                }

            }
#if X_PLATFORM_TOPIC_VALIDATION
            CrossPlatformTopicValidator.Close();
#endif
            
        }

        static void Extra(string[] files)
        {
            List<String[]> allTopics = new List<string[]>();
            Regex topicRegex = new Regex(new Regex1().GetTopicPattern(), RegexOptions.Compiled | RegexOptions.Multiline);
            foreach (string file in files)
            {
                Console.WriteLine("Extra for " + Path.GetFileNameWithoutExtension(file) + "...");
                String text = File.ReadAllText(file);
                string[] topics = topicRegex.Matches(text)
                    .Cast<Match>()
                    .Select(
                        match => match
                            .Value
                            .ToLower()
                    //.Split(' ')
                            )
                    .Distinct()
                    .ToArray();
                allTopics.Add(topics);
            }
            //string[][] all = allTopics.Aggregate(GetCommonElements).Distinct().Where(x=>x.Length > 0).ToArray();
            Dictionary<string, int> counts = new Dictionary<string, int>();
            foreach (var doc in allTopics)
            {
                foreach (var topic in doc)
                {
                    if (counts.ContainsKey(topic))
                        counts[topic] += 1;
                    else
                        counts[topic] = 1;
                }
            }
            var ordered = counts.OrderByDescending(kvp => kvp.Value);
            string output = "";
            foreach (var kvp in ordered)
            {
                output += kvp.Key + " " + kvp.Value + "\n";
            }
            File.WriteAllText("topics.txt", output);
            //string output= "";
            //foreach (var topicKeywords in all)
            //{
            //    output += topicKeywords.AggregateBy(",") + "\n";
            //}
        }
        static string[][] GetCommonElements(string[][] one, string[][] two)
        {
            float threshold = 0f;
            var res = one.SelectMany(alpha =>
                    two.Select(beta =>
                    {
                        var common = alpha.Intersect(beta);
                        if (common.Count() > Math.Max(alpha.Length, beta.Length) * threshold)
                        {
                            return common.ToArray();
                        }
                        return null;
                    }).Where(inter => inter != null).ToArray()
                ).ToArray();
            res = res.Distinct(new StringArrayComparer()).ToArray();

            return res;
        }

        class StringArrayComparer : IEqualityComparer<String[]>
        {

            public bool Equals(string[] x, string[] y)
            {
                return x.All(elem => y.Contains(elem)) && y.All(elem => x.Contains(elem));
            }

            public int GetHashCode(string[] obj)
            {
                return obj.Select(elem => elem.GetHashCode()).Aggregate((one, two) => one ^ two);
            }
        }

        /// <summary>
        /// Generates a report for the XML generated after stage 1
        /// </summary>
        /// <param name="stage1"></param>
        static void Stage1Report(XDocument stage1, string fileName)
        {
            Log.LogMessage("Stage 1 transformation report:", fileName, ConsoleColor.Green);
            Log.LogMessage("{0} topics", fileName, ConsoleColor.Cyan, (stage1.Root.Elements("topic").Count() + stage1.Root.Elements("topic_main").Count()));
            Log.LogMessage("{0} unique (names of) speakers", fileName, ConsoleColor.Cyan, stage1
                .Descendants("speech")
                .Select(el => el.Attribute("speaker"))
                .Distinct().Count());
            Log.LogMessage("{0} tables", fileName, ConsoleColor.Cyan, stage1.Descendants("table").Count());
            Log.LogMessage("{0}  written questions", fileName, ConsoleColor.Cyan, stage1
                .Descendants("stage-direction")
                .Count(el => el.Attribute("question-num") != null));
        }

        static IRegexContainer[] ALL_REGEX_CONTAINERS = new IRegexContainer[]
        {
            new Regex1(), new Regex2()
        };
        static IRegexContainer GetContainer(int year)
        {
            return ALL_REGEX_CONTAINERS.First(reg => reg.GetYearsValidFor().Contains(year));
        }
    }
}
