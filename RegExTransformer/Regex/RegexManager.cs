﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RegExTransformer
{
    public static class RegexManager
    {
        static RegexPattern[] patterns;

        public static void Load(string directory)
        {
            XmlSerializer ser = new XmlSerializer(typeof(RegexPattern));
            string[] files = Directory.GetFiles(directory, "*.xml", SearchOption.AllDirectories);
            patterns = new RegexPattern[files.Length];
            for (int i = 0; i < files.Length; i++)
            {
                patterns[i] = (RegexPattern)ser.Deserialize(new FileStream(files[i], FileMode.Open, FileAccess.Read, FileShare.Read));
            }
        }

        public static RegexPattern GetPattern(DateTime docDate, RegexFeature feature)
        {
            //if (feature == RegexFeature.Topic)
            //    Debugger.Break();
            var pattern = patterns.SingleOrDefault(rp => rp.feature == feature && rp.relevancyStart <= docDate && rp.relevancyEnd >= docDate);
            return pattern;
        }
    }
}
