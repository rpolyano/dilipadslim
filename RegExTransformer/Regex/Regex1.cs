﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RegExTransformer
{
    public class Regex1 : IRegexContainer
    {

        public string PreProccess(string text, Dictionary<string, string> meta = null)
        {
            text = Regex.Replace(text, "-{2,}", "\n\n");
            //string date = meta["DATE"];
            //DateTime dt = DateTime.Parse(date);
            //text = Regex.Replace(text,
            //   String.Format("{0} {1}(?:.| |,)? {2}",
            //       dt.ToString("MMMM").ToUpper(),
            //       dt.Day, dt.Year), "");

            //text = text.Replace("HOUSE OF COMMONS", "").Replace("COMMONS DEBATES", "");
            return text;
        }

        //Patterns

        public string GetTimeStampPattern()
        {
            return @"\s?\[DOT\][0-9 HIl()\[\]{}]{3,10}\s?";
        }

        public string GetTopicPattern()
        {
            //((?:^[0-9\-]{4,9}\sREVIEW$)|(?:\([A-Z]{1,4}\)[A-Zcflkuoxzv\s\-]+$)|(?:^[A-Zcflkuoxzv1\-]{4,}$)|(?:^[^a-z\t\*\^\u25A0\}\{\}\[]{2}[A-Zcflkuoxzv\-\.\s]{2,}[^a-z\t\*\^\u25A0\}\{\}\t\[]+$))
            return @"(?:^\*? ?)((?:[0-9\-]{4,9} REVIEW$)|(?:(?:[A-Zcflkuoxzv\'\""]+ ?){1,8}$)|(?:[A-Zcflkuoxzv\'\""][A-Zcflkuoxzv\'\""()0-9,.\- \n]+$))";
        }

        public string GetMPSPeakerPattern()
        {
            //TODO: remove special cases from [A-Z...] part. Maybe just match all lower case too?
            return @"^((?:Sir|M\.|Mr\.|Mr\,|Hon\.|The\sHon\.|Right\sHon\.|The\sRight\sHon\.|Miss|Mrs\.|Ms\.)\.?\s(?:[A-Zdvl][\-\w\.']{1,25}\s{0,1}){1,4}\s{0,1}(?:\(.+?\)){0,1}\s{0,1}(?:(?:moved:)|(?:moved)|:|;))";
        }

        public string GetOtherSpeakerPattern()
        {
            return @"^((?:The|Madam)(?:\sActing)?(?:\sAssistant)?(?:\sDeputy)?\s(?:Speaker)?(?:Chairman)?(?: \(.{0,50}?\)){0,1}?(?:;|:))";
        }

        public string GetInterventionPattern()
        {
            return @"(?:[sS]ome|[aA]n?) [hH]on. [mM]ember.?:";
        }

        //ONLY CALL THIS AFTER SPEAKERS HAVE BEEN MARKED!!
        public string GetQuestionPattern()
        {
            return @"(?:.uestion\sN..\s([0-9ilI\,\|]{1,})\-)([^:<]*?):?\r?\n([0-9\. \,]{0,3}[^<]+(?:\?|\.|\n))";
        }

        public string GetStandingOrderSubtitlesPattern()
        {
            return @"\n[A-Z0-9]{1}'{0,1}\.{0,1}\s{0,1}O\.{0,1}\s{0,1}[0-9]{1,4}\r?\n";
        }

        public string GetPageNumbersPattern()
        {
            return @"\n[0-9\-]{1,10}\r?\n";
        }

        public string GetDatesPattern()
        {
            return @"\n[A-Za-z]{3,9}\s[l1-9][l1-9]?(?:,| )?[l0-9]{4}\r?\n";
        }

        public string GetTablesPattern()
        {
            //TODO: Replace (\.|\. | \.){5,} with \t
            return @"((?:(?:[^\t\n]+)(?:\t))|(?:(?<=\t)[^\t\n]+))";
        }

        public string GetXMLTagPattern()
        {
            return @"(<\/?[^>]+>)";
        }

        public string GetExtraCleanPattern()
        {
            return "";
        }

        //Lists

        public string[] GetPrimaryStageDirections()
        {
            return new string[]
            {
                //"(Motion",
                "Prayers",
                "Call in the members.",
                //"(Division",
                //"{Division", 
                //"(The House",
                //"{The House",
                //"YEAS",
                //"'YEAS",
                //"PAIRED-MEMBERS",
                //"PAIRED MEMBERS",
                //"NAYS",
                "SUSPENSION OF SITTING",
                //"SITTING RESUMED",
                "AFTER RECESS",
                "The House met at",
                "The House adjourned at"
            };
        }

        public string[] GetSubtopic()
        {
            return new string[]{
                "COMMITTEES OF THE HOUSE",
                "POINTS OF ORDER",
                "POINT OF ORDER",
                "WAYS AND MEANS",
                "PETITIONS",
                "QUESTIONS PASSED AS ORDERS FOR RETURNS",
                "QUESTIONS ON THE ORDER PAPER",
                "GOVERNMENT RESPONSE TO PETITIONS",
                "SUPPLEMENTARY ESTIMATES",
                "QUESTIONS ON THE ORDER PAPER",
                "PRESENCE IN GALLERY",
                "BUSINESS OF THE HOUSE",
                "PRIVILEGE",
                "GOVERNMENT RESPONSE TO PETITIONS",
                "MOTIONS FOR PAPERS",
                "THE BUDGET",
                "ORDER IN COUNCIL APPOINTMENTS",
                "SUPPLY",
                "INTERIM SUPPLY",
                "INTERPARLIAMENTARY DELEGATIONS"
            };
        }

        public string[] GetMainHeaders()
        {
            return new string[]
            {
                "Government Orders",
                "Statements,Members",
                "Oral,Question",
                "Question,Period",
                "Routine Proceedings",
                "Private Member,Business",
                "Adjournment,Proceeding",
                "Statements Pursuant to",
                "Sitting Resumed"
            };
            //return new string[]
            //{
            //    "Government Orders",
            //    "Statements by Members",
            //    "Oral Questions",
            //    "Routine Proceedings",
            //    "Oral Question Period",
            //    "Private Members' Business",
            //    "Adjournment Proceedings",
            //    "Statements Pursuant to",
            //    "Sitting Resumed"
            //};
        }

        public string[] GetVotes()
        {
            return new string[]
            {
                "(Division", "(division"
            };
        }

        public int[] GetYearsValidFor()
        {
            return Enumerable.Range(1964, 2000 - 1964).ToArray();
        }
    }
}
