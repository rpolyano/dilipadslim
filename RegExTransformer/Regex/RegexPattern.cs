﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RegExTransformer
{
    public enum RegexFeature
    {
        Topic, //Topic titles
        TopicInterceptor, //Find places where topics get merged with other lines
        Timestamp, //Time stamps
        SpeakerName, //General name finding
        OtherSpeakerName, //Specialized (speaker, chairman) name finding
        Intervention, //"Some/An Hon. Member" Name finding
        Question, //Questions speech
        Table, //Tables
        InterQuestion, //Back and forth/rapid succession questions
        Votes, //Votes on motions
        NameParser, //Name dissection for stage 4
        CommonStageDir //Common stage directions
    }

    [XmlType("constant")]
    public class Constant
    {
        [XmlAttribute]
        public string name;

        [XmlText]
        public string value;
    }
    public class RegexPattern
    {
        public RegexFeature feature;

        public bool singleLine = false;

        public bool multiLine = false;
        
        //public SerializableDictionary<string, string> constants;
        public Constant[] constants;

        public string pattern;

        public string markup;

        public string Markup
        {
            get
            {
                if (markup != null)
                {
                    return markup.Replace(@"\n", "\n");
                }
                return null;
            }
        }

        [XmlIgnore]
        public DateTime relevancyStart;

        [XmlIgnore]
        public DateTime relevancyEnd;

        [XmlElement("relevancyStart")]
        public string RelevancyStartString
        {
            get { return this.relevancyStart.ToString("yyyy-MM-dd"); }
            set { this.relevancyStart = DateTime.ParseExact(value, "yyyy-MM-dd", null); }
        }

        [XmlElement("relevancyEnd")]
        public string RelevancyEndString
        {
            get { return this.relevancyEnd.ToString("yyyy-MM-dd"); }
            set { this.relevancyEnd = DateTime.ParseExact(value, "yyyy-MM-dd", null);}
        }

        public string FullPattern
        {
            get
            {
                string fullPattern = pattern;
                if (constants != null)
                {
                    foreach (var con in constants)
                    {
                        fullPattern = fullPattern.Replace("@" + con.name + "@", con.value);
                    }
                }
                return fullPattern.Replace("\n", "").Replace("\r", "");
            }
        }
    }
}
