﻿using RegExTransformer.MemberInfo;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using DateRange = RegExTransformer.ParliamentarianManager.DateRange;
namespace RegExTransformer
{
    public class TransformerStage4
    {
        public static int TOTAL_COUNT = 0;
        public static int UNLINKED_COUNT = 0;
        public static int FIXED_COUNT = 0;

        XDocument doc;
        
        DateTime date;
        string fileName;
        static Regex NAME_PARSER;// = new Regex(@"^\*?(?<prefix>(?:[A-Z][a-z]+ ?)+\. |Miss |Madam |(?:The )?Acting )(?<firstname>(?:[A-Z][a-z-]+ ?)+? )?(?<initials>(?:[A-Z]\. )*)(?<lastname>(?:(?:[A-Z][a-z-]+|[a-z]{1,3}) ?)+)(?<constit>(?:\(.+?\) ?)*)(?<movement>moved)?", RegexOptions.Compiled);
        static Regex FALLBACK_NAME_PARSER;
        
        public TransformerStage4(XDocument doc, string filename)
        {
            
            this.doc = doc;
            this.date = DateTime.Parse(filename.Replace('_', '-'));
            this.fileName = filename;
            NAME_PARSER = new Regex(RegexManager.GetPattern(this.date, RegexFeature.NameParser).FullPattern, RegexOptions.Compiled);
            FALLBACK_NAME_PARSER = new Regex(RegexManager.GetPattern(this.date, RegexFeature.NameParser).FullPattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);

        }

        public string Transform()
        {
            if (!Directory.Exists("stage_4"))
            {
                Directory.CreateDirectory("stage_4");
            }

            //Link speakers
            var speeches = doc.Descendants(TransformerStage3.NAMESPACES["pm"] + "speech")
                .Union(doc.Descendants(TransformerStage3.NAMESPACES["dp"] + "speech"))
                //.OrderBy(speech =>
                //    speech.Attribute(TransformerStage3.NAMESPACES["pm"] + "id").Value)
                    .ToArray();
            string[] docMembersNames = speeches
               .Select(speech =>
                   speech.Attribute(TransformerStage3.NAMESPACES["pm"] + "speaker").Value.Trim())
               .ToArray();

            if (docMembersNames.Length == 0)
            {
                Log.LogError(fileName, "No speeches found", true);
                doc.Save("stage_4/" + date.Year + "." + fileName + ".xml");
                return "No speeches";
            }

            MiniMember[] docMembersNamesExpanded;
            DateRange inferredDocumentDateRange;
            var guids = LinkNames(docMembersNames, new DateRange() { invalid = true }, out docMembersNamesExpanded, out inferredDocumentDateRange);

            string report = docMembersNames.Zip(docMembersNamesExpanded, (one, two) => one + "\t" + two).AggregateBy("\r\n");
            report = docMembersNames
                .Zip(docMembersNamesExpanded, (one, two) => one + "\t" + two + "\t" + 
                    "Based On " + MatchBasisToString(ParliamentarianManager.GetMatchBasis(two, date)))
                .Zip(guids, (one, two) => one + "\t" + 
                    (two == null ? "NO MATCH" : 
                    two.member.name.first + " " + two.member.name.last + " " + two.member.id))
                .AggregateBy("\r\n");
            
            string csv = docMembersNames
                .Zip(docMembersNamesExpanded, (one, two) => '"' + one + "\",\"" + two + "\"," +
                    "\"Based On " + MatchBasisToString(ParliamentarianManager.GetMatchBasis(two, date)) + '"')
                .Zip(guids, (one, two) => one + "," +
                    (two == null ? "NO MATCH" :
                    '"' + two.member.name.first + " " + two.member.name.last + " " + two.member.id + '"'))
                .AggregateBy("\r\n");

            for (int i = 0; i < speeches.Count(); i++)
            {
                Debug.Assert(speeches[i].Attribute(TransformerStage3.NAMESPACES["pm"] + "speaker").Value.Trim() == docMembersNames[i].Trim());
                if (guids[i] != null)
                {
                    speeches[i].SetAttributeValue(TransformerStage3.NAMESPACES["pm"] + "speaker", guids[i].member.name.first + " " + guids[i].member.name.last);
                    speeches[i].SetAttributeValue(TransformerStage3.NAMESPACES["dp"] + "speaker-old", docMembersNames[i]);
                    speeches[i].SetAttributeValue(TransformerStage3.NAMESPACES["dp"] + "speaker-id", guids[i].member.id);
                    speeches[i].SetAttributeValue(TransformerStage3.NAMESPACES["dp"] + "speaker-link-confidence", MakeConfidenceScore(ParliamentarianManager.GetMatchBasis(docMembersNamesExpanded[i], date)));
                }
                else if (docMembersNamesExpanded[i] == MiniMember.EMPTY || docMembersNamesExpanded[i].ToString().Length == 1)
                {
                    speeches[i].SetAttributeValue(TransformerStage3.NAMESPACES["dp"] + "speaker-id", "intervention");
                }
                else
                {
                    speeches[i].SetAttributeValue(TransformerStage3.NAMESPACES["pm"] + "speaker", docMembersNamesExpanded[i]);
                    speeches[i].SetAttributeValue(TransformerStage3.NAMESPACES["dp"] + "speaker-id", "unmatched");
                }
            }

            //Link question askers
            var questions = doc.Descendants(TransformerStage3.NAMESPACES["pm"] + "stage-direction")
                .Where(ques => ques.Attribute(TransformerStage3.NAMESPACES["dp"] + "question-author") != null &&
                    !String.IsNullOrEmpty(ques.Attribute(TransformerStage3.NAMESPACES["dp"] + "question-author").Value.Trim())).ToArray();

            if (questions.Length == 0)
            {
                Log.LogMessage("No questions asked in file", fileName, ConsoleColor.Green);
            }
            else
            {
                Log.LogMessage("Linking {0} question askers", fileName, ConsoleColor.Green, questions.Length);
                string[] questionAskerNames = questions
               .Select(ques =>
                   ques.Attribute(TransformerStage3.NAMESPACES["dp"] + "question-author").Value.Trim())
               .ToArray();

                MiniMember[] questionAskersParsed;

                MemberFile[] linkedQuestionAskers = LinkNames(questionAskerNames, inferredDocumentDateRange, out questionAskersParsed, out inferredDocumentDateRange);

                for (int i = 0; i < questions.Count(); i++)
                {
                    Debug.Assert(questions[i].Attribute(TransformerStage3.NAMESPACES["dp"] + "question-author").Value.Trim() == questionAskerNames[i].Trim());
                    if (linkedQuestionAskers[i] != null)
                    {
                        questions[i].SetAttributeValue(TransformerStage3.NAMESPACES["dp"] + "question-author", linkedQuestionAskers[i].member.name.first + " " + linkedQuestionAskers[i].member.name.last);
                        questions[i].SetAttributeValue(TransformerStage3.NAMESPACES["dp"] + "question-author-old", questionAskerNames[i]);
                        questions[i].SetAttributeValue(TransformerStage3.NAMESPACES["dp"] + "question-author-id", linkedQuestionAskers[i].member.id);
                        questions[i].SetAttributeValue(TransformerStage3.NAMESPACES["dp"] + "question-author-link-confidence", MakeConfidenceScore(ParliamentarianManager.GetMatchBasis(questionAskersParsed[i], date)));
                    }
                    else
                    {
                        questions[i].SetAttributeValue(TransformerStage3.NAMESPACES["dp"] + "question-author-id", "unmatched");
                    }
                }

            }
            
            doc.Save("stage_4/" + date.Year + "." + fileName + ".xml");
            return csv;
            //Debugger.Break();

        }

        private MemberFile[] LinkNames(string[] names, DateRange dateRangeOverride, out MiniMember[] parsedNames, out DateRange inferredDocumentDateRange)
        {
            

            Match[] nameRegexMatches = names.Select(name =>
            {
                name = Filter(name);
                var match = NAME_PARSER.Match(name);
                if (!match.Success)
                {
                    match = FALLBACK_NAME_PARSER.Match(name);
                }
                return match;
            }).ToArray();

            MiniMember[] namesExpanded = DisambiguateMembers(names, nameRegexMatches);
            //TODO: Do this again with OCR flex 

            DateRange maxRange;
            //var range = ParliamentarianManager.GetCommonDateRange(namesExpanded, date, out maxRange);
            DateRange range = ParliamentarianManager.GetCommonDateRange(namesExpanded, date, out maxRange);
            inferredDocumentDateRange = range;
            if (!dateRangeOverride.invalid)
            {
                range = dateRangeOverride;
            }



            Log.LogMessage("Date range is " + range, fileName, ConsoleColor.Cyan);
            var specificMembers = ParliamentarianManager.GetMembersForDateRange(new DateRange(date.AddDays(-1), date.AddDays(1)));

            if (!range.Contains(date))
            {
                Log.LogError(fileName, "File date is wrong! Should be between " + range.ToString());
                range = maxRange;
                specificMembers = ParliamentarianManager.GetMembersForDateRange(range);
            }

            FindMetaMembers(names, namesExpanded);
            
            var relevantMembers = ParliamentarianManager.GetMembersForDateRange(range);

            Log.LogMessage("Database restricted to {0} members", fileName, ConsoleColor.Cyan, relevantMembers.Length);

            MemberInfo.MemberFile[] guids = namesExpanded.Select(mem => ParliamentarianManager.MatchMember(mem, date, range, false, specificMembers)).ToArray();

            var unlinked = namesExpanded
                .Select((item, index) => new { Index = index, Item = item })
                .Where(x => guids[x.Index] == null && x.Item != MiniMember.EMPTY).Select(x => x);

            var namedUnlinked = unlinked.Select(x => x.Item.ToString()).Where(mem => !mem.ToLower().StartsWith("an hon") && !mem.ToLower().StartsWith("some hon")).ToArray();//.Distinct().ToArray();
            int numUnlinked = namedUnlinked.Length;
            Log.LogMessage("Got " + numUnlinked + " unlinked members", fileName, ConsoleColor.Yellow);



            guids = guids.Select((memfile, index) =>
            {
                if (memfile == null && namesExpanded[index] != MiniMember.EMPTY)
                {
                    return ParliamentarianManager.MatchMember(namesExpanded[index], date, range, true, relevantMembers);
                }
                return memfile;
            }).ToArray();

            TOTAL_COUNT += namesExpanded.Select(x => x.ToString()).Where(mem => !mem.ToLower().StartsWith("an hon") && !mem.ToLower().StartsWith("some hon")).Count();//Select(x => x.ToString()).Distinct().Count();


            unlinked = namesExpanded
                .Select((item, index) => new { Index = index, Item = item })
                .Where(x => guids[x.Index] == null && x.Item != MiniMember.EMPTY).Select(x => x);
            namedUnlinked = unlinked.Select(x => x.Item.ToString()).Where(mem => !mem.ToLower().StartsWith("an hon") && !mem.ToLower().StartsWith("some hon")).ToArray();//.Distinct().ToArray();
            int numUnlinkedAfterOCRPass = namedUnlinked.Length;
            Log.LogMessage("After OCR correction " + numUnlinkedAfterOCRPass + " unlinked members", fileName, ConsoleColor.Yellow);

            FIXED_COUNT += (numUnlinked - numUnlinkedAfterOCRPass);
            UNLINKED_COUNT += namedUnlinked.Length;//unlinked.Select(x => x.Item.ToString()).Count();
            //MemberInfo.MemberFile[] ocrlinked = unlinked.Select(mem => ParliamentarianManager.MatchMember(mem, date, true)).ToArray();

            parsedNames = namesExpanded;
            return guids;
        }

        private string Filter(string name)
        {
            return Regex.Replace(name, @"([A-Z]{2,})\.", "$1");
        }

        private float MakeConfidenceScore(ParliamentarianManager.MatchBasis matchBasis)
        {
            //Scoring parameters
            //Name: (4 points)
            //First Name - 1 point, Last Name - 1 point, BioName - 1 point, all three - 1 bonus point
            //Const: (3 points)
            //Constituency - 3 points / membership - 2 points / flex membership - 1 point
            //OCR: (1 point)
            //No OCR Flexibility - 1 point
            //Total = 8 points.
            int score = 0;
            score += (matchBasis.HasFlag(ParliamentarianManager.MatchBasis.LastName) ? 1 : 0) +
                (matchBasis.HasFlag(ParliamentarianManager.MatchBasis.FirstName) ? 1 : 0) +
                (matchBasis.HasFlag(ParliamentarianManager.MatchBasis.BiographyName) ? 1 : 0) +
                ((matchBasis.HasFlag(ParliamentarianManager.MatchBasis.LastName) &&
                matchBasis.HasFlag(ParliamentarianManager.MatchBasis.FirstName) &&
                matchBasis.HasFlag(ParliamentarianManager.MatchBasis.BiographyName)) ? 1 : 0);
            score += (matchBasis.HasFlag(ParliamentarianManager.MatchBasis.Constituency) ? 3 : 
                (matchBasis.HasFlag(ParliamentarianManager.MatchBasis.Membership) ? 2 : 
                (matchBasis.HasFlag(ParliamentarianManager.MatchBasis.FlexibleMembership) ? 1 : 0)));
            score += (matchBasis.HasFlag(ParliamentarianManager.MatchBasis.OCRFlexibility) ? 0 : 1);

            return score / 8f;
        }

        private string MatchBasisToString(RegExTransformer.ParliamentarianManager.MatchBasis mb)
        {
            string ret = "";
            Type mbType = typeof(RegExTransformer.ParliamentarianManager.MatchBasis);
            foreach (var name in Enum.GetNames(mbType))
            {
                RegExTransformer.ParliamentarianManager.MatchBasis value =
                    (RegExTransformer.ParliamentarianManager.MatchBasis)Enum.Parse(mbType, name);
                if (value != ParliamentarianManager.MatchBasis.None && mb.HasFlag(value))
                {
                    ret += name + ", ";
                }
            }
            return ret.TrimEnd(' ', ',');
        }

        private void FindMetaMembers(string[] docMembersNames, MiniMember[] docMembersNamesExpanded)
        {
            for (int i = 0; i < docMembersNames.Length; i++)
            {
                //Speaker = Speaker
                //Deputy Speaker = Deputy Speaker
                //Chairman = Deputy Speaker and Chair of Committees
                //Deputy Chairman = Deputy Chair of Committees
                string speakerType = "";
                if (docMembersNames[i].ToLower().Contains("speaker"))
                {
                    speakerType = "Speaker";
                    if (docMembersNames[i].ToLower().Contains("deputy"))
                    {
                        speakerType = "Deputy " + speakerType;
                    }
                    else if (docMembersNames[i].ToLower().Contains("acting"))
                    {
                        speakerType = "Acting " + speakerType;
                    }
                }
                if (docMembersNames[i].ToLower().Contains("chair"))
                {
                    speakerType = "Chair(man)? of Committees of the Whole";
                    
                    if (docMembersNames[i].ToLower().Contains("acting"))
                    {
                        speakerType = "Acting " + speakerType;
                    }
                    else if (!docMembersNames[i].ToLower().Contains("deputy"))
                    {
                        speakerType = "Deputy Speaker and " + speakerType;
                    }
                    else
                    {
                        speakerType = "Deputy " + speakerType;
                    }
                }
                if (speakerType != "")
                {
                    
                    if (docMembersNamesExpanded[i] == MiniMember.EMPTY || docMembersNamesExpanded[i].ToString().ToLower().Contains("acting")) //Acting speakers usually get parsed
                    {
                        docMembersNamesExpanded[i] = new MiniMember() { Constituency = speakerType, Speaker = true };
                        if (docMembersNames[i].Contains("("))
                        {
                            string info = Regex.Match(docMembersNames[i], @"\((.+)\)").Groups[1].Value;
                            Match nameMatch = FALLBACK_NAME_PARSER.Match(info); //Want case-insensitivity here
                            if (nameMatch.Success)
                            {
                                string refPrefix = nameMatch.Groups["prefix"].Value.Trim();
                                string refInitials = nameMatch.Groups["initials"].Value.Trim();
                                string refFirstName = nameMatch.Groups["firstname"].Value.Trim();
                                string refLastName = nameMatch.Groups["lastname"].Value.Trim();
                                docMembersNamesExpanded[i] = new MiniMember()
                                {
                                    Constituency = speakerType,
                                    FirstName = refFirstName,
                                    LastName = refLastName,
                                    Initials = refInitials,
                                    Prefix = refPrefix,
                                    Speaker = false //Acting speakers do not have their role as acting speaker in their member file
                                };
                            }
                        }


                    }
                }
                if (Regex.IsMatch(docMembersNames[i], @"^[^(]+(\sof\s|\sto\s)"))
                {
                    string job = docMembersNames[i]
                        .TakeWhile(chr=>chr!='(')
                        .Select(chr=>chr.ToString())
                        .AggregateBy("").Trim(':');
                    var mp = docMembersNamesExpanded.Where(name =>
                        name.Constituency.StartsWith(job)
                        ).First();
                    docMembersNamesExpanded[i] = mp;
                    Log.LogMessage("Infered MP from job: " + docMembersNames[i] + "=>" + mp, fileName, ConsoleColor.Green);
                    
                }
            }
        }

        List<string> disambiguatedAlready = new List<string>();
        public MiniMember[] DisambiguateMembers(string[] docMembersNames, Match[] docMembersMatched)
        {
            RegexPattern interventionRegex = RegexManager.GetPattern(date, RegexFeature.Intervention);
            MiniMember[] docMembersNamesExpanded = new MiniMember[docMembersNames.Length];
            for (int reference = 0; reference < docMembersNames.Length; reference++)
            {
                if (Regex.IsMatch(docMembersNames[reference], interventionRegex.FullPattern, RegexOptions.Compiled))
                {
                    continue;
                }
                //if (docMembersNamesExpanded[reference] != null)
                //    continue;
                Match referenceMatch = docMembersMatched[reference];
                //if (!referenceMatch.Success)
                //{
                //    referenceMatch = FALLBACK_NAME_PARSER.Match(docMembersNames[reference]);
                //}
                //TODO: No need for ToLower
                string refPrefix = referenceMatch.Groups["prefix"].Value.Trim();
                string refInitials = referenceMatch.Groups["initials"].Value.Trim();
                string refFirstName = referenceMatch.Groups["firstname"].Value.Trim();
                string refLastName = referenceMatch.Groups["lastname"].Value.Trim();
                string refConstit = referenceMatch.Groups["constit"].Value.Trim();
                refConstit = Regex.Match(refConstit, @"\((.+)\)").Groups[1].Value;

                if (refLastName.AlphaEquals("Speaker"))
                {
                    continue;
                }
                for (int instance = 0; instance < docMembersNames.Length; instance++)
                {
                    if (Regex.IsMatch(docMembersNames[instance], interventionRegex.FullPattern, RegexOptions.Compiled))
                    {
                        continue;
                    }
                    //if (docMembersNamesExpanded[instance] != null)
                    //    continue;

                    Match instanceMatch = docMembersMatched[instance];
                    //if (!instanceMatch.Success)
                    //{
                    //    instanceMatch = FALLBACK_NAME_PARSER.Match(docMembersNames[instance]);
                    //}
                    string insPrefix = instanceMatch.Groups["prefix"].Value.Trim();
                    string insInitials = instanceMatch.Groups["initials"].Value.Trim();
                    string insFirstName = instanceMatch.Groups["firstname"].Value.Trim();
                    string insLastName = instanceMatch.Groups["lastname"].Value.Trim();
                    string insConstit = instanceMatch.Groups["constit"].Value.Trim();

                    if (insLastName.AlphaEquals("Speaker"))
                    {
                        continue;
                    }

                    insConstit = Regex.Match(insConstit, @"\((.+)\)").Groups[1].Value;

                    bool prefixMatch = insPrefix.AlphaEquals(refPrefix);
                    bool initialsMatch = insInitials.AlphaEquals(refInitials);
                    bool lastNameMatch = insLastName.AlphaEquals(refLastName);

                    //First names are exactly the same and not empty
                    bool _firstNameDirect = !String.IsNullOrEmpty(insFirstName) && insFirstName.AlphaEquals(refFirstName);
                    
                    //First name not empty, Initials not empty, firstName starts with first initial
                    bool _firstNameInitials = initialsMatch ||
                        (!String.IsNullOrEmpty(insFirstName) && !String.IsNullOrEmpty(refInitials) && insFirstName.StartsWith(refInitials.Substring(0,1)));
                    
                    //Someone does not have a first name or initials
                    bool _firstNameBlank = (String.IsNullOrEmpty(insInitials + "" + insFirstName)) ||
                        (String.IsNullOrEmpty(refInitials + "" + refFirstName));
                    
                    bool firstNameMatch = _firstNameBlank || _firstNameDirect || _firstNameInitials;
                    
                    //Ministers can have constituencies, so if one is a Minister we don't really get any info
                    bool consMatch = insConstit == refConstit || 
                        String.IsNullOrEmpty(insConstit) || String.IsNullOrEmpty(refConstit) ||
                        (Regex.IsMatch(insConstit, @"\sof\s|\sto\s") && !Regex.IsMatch(refConstit, @"\sof\s|\sto\s")); //TODO: Check for safety

                    if (lastNameMatch && firstNameMatch && consMatch)
                    {
                        
                        MiniMember expanded;
                        int repl;
                        string original;
                        if (!String.IsNullOrWhiteSpace(refInitials) ||
                            !String.IsNullOrWhiteSpace(refConstit) ||
                            refPrefix.Length > insPrefix.Length)
                        {

                            expanded = new MiniMember()
                            {
                                Prefix = refPrefix,
                                FirstName = refFirstName,
                                LastName = refLastName,
                                Initials = refInitials,
                                Constituency = refConstit
                            };
                            original = referenceMatch.Value;
                            repl = instance;
                        }
                        else
                        {
                            expanded = new MiniMember()
                            {
                                Prefix = insPrefix,
                                FirstName = insFirstName,
                                LastName = insLastName,
                                Initials = insInitials,
                                Constituency = insConstit
                            };
                            original = instanceMatch.Value;
                            repl = reference;
                        }
                        //expanded = Regex.Replace(expanded, @"\s{2,}", " ").Trim();
                       
                        //expanded = FixCase(expanded, original);
                        if (expanded.ToString().Contains("Acting Speaker"))
                        {
                            expanded = new MiniMember() { Constituency = "ASpeaker " + Regex.Match(expanded.ToString(), @"\((.+)\)").Groups[1].Value };
                        }
                        if (docMembersNamesExpanded[repl] == null ||
                                docMembersNamesExpanded[repl].Length < expanded.Length)
                        {
                            docMembersNamesExpanded[repl] = expanded;
                        }
                    }
                    else if (lastNameMatch && !disambiguatedAlready.Contains(referenceMatch.Value + instanceMatch.Value) && !disambiguatedAlready.Contains(instanceMatch.Value + referenceMatch.Value))
                    {
                        Log.LogMessage("Disambiguated similar: " + referenceMatch.Value + " and " + instanceMatch.Value, fileName);
                        disambiguatedAlready.Add(referenceMatch.Value + instanceMatch.Value);
                    }
                   

                }
            }
            return docMembersNamesExpanded;
        }


        private string FixCase(string expanded, string original)
        {
            string[] origWords = original.Split(" ():".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            string[] words = expanded.Split(" @()".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (var word in words)
            {
                string origWord = origWords.First(o => o.ToLower().AlphaEquals(word));
                string escWord = Regex.Replace(word, "([^a-zA-Z0-9])", @"\$1");
                expanded = Regex.Replace(expanded, @"(?<=\b)" + escWord + @"(?=\b|\s)", origWord.Trim(':'));
            }
            return expanded;
        }
    }
}
